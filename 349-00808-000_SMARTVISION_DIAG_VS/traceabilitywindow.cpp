#include "traceabilitywindow.h"
#include <QTableWidget>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QPushButton>
#include <QAbstractItemView>
#include <QDebug>
#include <QUuid>
#include <QTextCodec>
#include <QApplication>
#include <QClipboard>
TraceabilityWindow::TraceabilityWindow(QWidget *parent) :
    QWidget(parent)

{
    //Initialisation de la fenêtre
    //Window Initialisation
    resize(500,676);                                                                                //Gives the window a suitable size
    m_tracabilityTable = new QTableWidget(this);                                                            //Creates the tableWidget which will contain the traceability data
    QHeaderView *vertHeader=m_tracabilityTable->verticalHeader();                                          //Retrieve the rows headers
    vertHeader->hide();                                                                            //Hide them
    m_tracabilityTable->setColumnCount(3);                                                                  //There is currently 3 columns and 45 rows in the traceability table
    m_tracabilityTable->setRowCount(45);
    m_tracabilityTable->resize(302,625);                                                                    //Resizes the table to its optimal size

    m_tracabilityTable->setHorizontalHeaderItem(0, new QTableWidgetItem("DID"));                            //Names the columns
    m_tracabilityTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Name"));
    m_tracabilityTable->setHorizontalHeaderItem(2, new QTableWidgetItem("Value"));

                                                                   //Tentative de changement des tailles des colonnes. La case de nom doit être plus grande
                                                                    // que les autres car elles contient souvent beaucoup plus de texte. Ces lignes ne
                                                                  //fonctionnent pas avec l'interface responsive active.

    QPushButton *quitButton = new QPushButton(this);                                             // Creates the button to hide this window
    quitButton->setText("Close");                                                                // Changes the text of the button
    quitButton->move(100,630);                                                                   // Moves it under the table
    QPushButton *Copy_to_clipboard = new QPushButton(this);
    Copy_to_clipboard->setText("Copy_to_clipboard");
    QVBoxLayout *mainLayout = new QVBoxLayout;                                                      // Creates a vertical layout
    mainLayout->addWidget(m_tracabilityTable);                                                              // Adds the table to the layout
    mainLayout->addWidget(quitButton);                                                           // And the button
    mainLayout->addWidget(Copy_to_clipboard);
    this->setLayout(mainLayout);                                                                    // Sets the window layout to the created one
    m_tracabilityTable->setEditTriggers(QAbstractItemView::NoEditTriggers);                                 // The table can't be edited by the user
    m_tracabilityTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);                     // the width of the columns is adjusted to the size of the window
    vertHeader->setSectionResizeMode(QHeaderView::Stretch);
    connect(quitButton, &QPushButton::clicked,this,&TraceabilityWindow::ButtonQuit);             // Connects the button to the hide slot
    connect(Copy_to_clipboard,&QPushButton::clicked,this,&TraceabilityWindow::traceability);
    Copy_to_clipboard->setEnabled(false);
    //Fill the table with identifiers and names of rows

    m_tracabilityTable->setItem(0,0, new QTableWidgetItem("Smart Vision"));                                 //Title of the first category, the line is grayed out
    m_tracabilityTable->item(0,0)->setBackgroundColor(Qt::lightGray);
    m_tracabilityTable->setItem(0,1, new QTableWidgetItem(""));
    m_tracabilityTable->item(0,1)->setBackgroundColor(Qt::lightGray);
    m_tracabilityTable->setItem(0,2, new QTableWidgetItem(""));
    m_tracabilityTable->item(0,2)->setBackgroundColor(Qt::lightGray);


    //Identifiers and names of the first category
    m_tracabilityTable->setItem(1,0, new QTableWidgetItem("FE26"));
    m_tracabilityTable->setItem(1,1, new QTableWidgetItem("System Software Baseline ID"));
    m_tracabilityTable->setItem(2,0, new QTableWidgetItem("FE20"));
    m_tracabilityTable->setItem(2,1, new QTableWidgetItem("VS Part Number"));
    m_tracabilityTable->setItem(3,0, new QTableWidgetItem("FE22"));
    m_tracabilityTable->setItem(3,1, new QTableWidgetItem("VS Serial Number"));
    m_tracabilityTable->setItem(4,0, new QTableWidgetItem("FE21"));
    m_tracabilityTable->setItem(4,1, new QTableWidgetItem("VS Manufacturing Date"));
    m_tracabilityTable->setItem(5,0, new QTableWidgetItem("FE23"));
    m_tracabilityTable->setItem(5,1, new QTableWidgetItem("Screen Serial Number"));
    m_tracabilityTable->setItem(6,0, new QTableWidgetItem("FE24"));
    m_tracabilityTable->setItem(6,1, new QTableWidgetItem("Top Camera Serial Number"));
    m_tracabilityTable->setItem(7,0, new QTableWidgetItem("FE25"));
    m_tracabilityTable->setItem(7,1, new QTableWidgetItem("Bot Camera Serial Number"));

    m_tracabilityTable->setItem(8,0, new QTableWidgetItem("Config"));                                       //Title of the second category, the line is grayed out
    m_tracabilityTable->item(8,0)->setBackgroundColor(Qt::lightGray);
    m_tracabilityTable->setItem(8,1, new QTableWidgetItem(""));
    m_tracabilityTable->item(8,1)->setBackgroundColor(Qt::lightGray);
    m_tracabilityTable->setItem(8,2, new QTableWidgetItem(""));
    m_tracabilityTable->item(8,2)->setBackgroundColor(Qt::lightGray);

    //Identifiers and names of the second category
    m_tracabilityTable->setItem(9,0, new QTableWidgetItem("FE0E"));
    m_tracabilityTable->setItem(9,1, new QTableWidgetItem("Configuration File Version ID (SV_CONFIG)"));
    m_tracabilityTable->setItem(10,0, new QTableWidgetItem("FE0F"));
    m_tracabilityTable->setItem(10,1, new QTableWidgetItem("Driver Remote Configuration File Version MD5 (SV_CONFIG_DRIVER)"));
    m_tracabilityTable->setItem(11,0, new QTableWidgetItem("FE30"));
    m_tracabilityTable->setItem(11,1, new QTableWidgetItem("Driver Reference Configuration File Version MD5"));
    m_tracabilityTable->setItem(12,0, new QTableWidgetItem("FE14"));
    m_tracabilityTable->setItem(12,1, new QTableWidgetItem("Passenger Remote Configuration File Version MD5 (SV_CONFIG_PASSENGER)"));
    m_tracabilityTable->setItem(13,0, new QTableWidgetItem("FE34"));
    m_tracabilityTable->setItem(13,1, new QTableWidgetItem("Passenger Reference Configuration File Version MD5"));

    m_tracabilityTable->setItem(14,0, new QTableWidgetItem("MainBoard"));                                   //Title of the third category, the line is grayed out
    m_tracabilityTable->item(14,0)->setBackgroundColor(Qt::lightGray);
    m_tracabilityTable->setItem(14,1, new QTableWidgetItem(""));
    m_tracabilityTable->item(14,1)->setBackgroundColor(Qt::lightGray);
    m_tracabilityTable->setItem(14,2, new QTableWidgetItem(""));
    m_tracabilityTable->item(14,2)->setBackgroundColor(Qt::lightGray);

    //Identifiers and names of the third category
    m_tracabilityTable->setItem(15,0, new QTableWidgetItem("FE00"));
    m_tracabilityTable->setItem(15,1, new QTableWidgetItem("Hardware Number ID"));
    m_tracabilityTable->setItem(16,0, new QTableWidgetItem("FE01"));
    m_tracabilityTable->setItem(16,1, new QTableWidgetItem("Hardware Version"));
    m_tracabilityTable->setItem(17,0, new QTableWidgetItem("FE02"));
    m_tracabilityTable->setItem(17,1, new QTableWidgetItem("Serial Number"));
    m_tracabilityTable->setItem(18,0, new QTableWidgetItem("FE0C"));
    m_tracabilityTable->setItem(18,1, new QTableWidgetItem("Package Version ID (SV_PACKAGE)"));
    m_tracabilityTable->setItem(19,0, new QTableWidgetItem("FE0D"));
    m_tracabilityTable->setItem(19,1, new QTableWidgetItem("Package Version MD5 (SV_PACKAGE)"));
    m_tracabilityTable->setItem(20,0, new QTableWidgetItem("FE0A"));
    m_tracabilityTable->setItem(20,1, new QTableWidgetItem("Linux Distribution Version ID (SV_DISTRIB)"));
    m_tracabilityTable->setItem(21,0, new QTableWidgetItem("FE0B"));
    m_tracabilityTable->setItem(21,1, new QTableWidgetItem("Linux Distribution Version MD5 (SV_DISTRIB)"));
    m_tracabilityTable->setItem(22,0, new QTableWidgetItem("FE04"));
    m_tracabilityTable->setItem(22,1, new QTableWidgetItem("Application SmartVision Version ID (SV_FC_APP)"));
    m_tracabilityTable->setItem(23,0, new QTableWidgetItem("FE05"));
    m_tracabilityTable->setItem(23,1, new QTableWidgetItem("Application SmartVision Version MD5 (SV_FC_APP)"));
    m_tracabilityTable->setItem(24,0, new QTableWidgetItem("FE06"));
    m_tracabilityTable->setItem(24,1, new QTableWidgetItem("BitStream Zynq Version ID (SV_FC_FPGA)"));
    m_tracabilityTable->setItem(25,0, new QTableWidgetItem("FE07"));
    m_tracabilityTable->setItem(25,1, new QTableWidgetItem("BitStream Zynq Version MD5 (SV_FC_FPGA)"));
    m_tracabilityTable->setItem(26,0, new QTableWidgetItem("FE08"));
    m_tracabilityTable->setItem(26,1, new QTableWidgetItem("Bitstream Artix (Safety) Version ID (SV_SM_FPGA)"));
    m_tracabilityTable->setItem(27,0, new QTableWidgetItem("FE09"));
    m_tracabilityTable->setItem(27,1, new QTableWidgetItem("Bitstream Artix (Safety) Version MD5 (SV_SM_FPGA)"));
    m_tracabilityTable->setItem(28,0, new QTableWidgetItem("FE12"));
    m_tracabilityTable->setItem(28,1, new QTableWidgetItem("Ethernet MAC Address"));

    m_tracabilityTable->setItem(29,0, new QTableWidgetItem("Safety Controller"));                           //Title of the fourth category, the line is grayed out
    m_tracabilityTable->item(29,0)->setBackgroundColor(Qt::lightGray);
    m_tracabilityTable->setItem(29,1, new QTableWidgetItem(""));
    m_tracabilityTable->item(29,1)->setBackgroundColor(Qt::lightGray);
    m_tracabilityTable->setItem(29,2, new QTableWidgetItem(""));
    m_tracabilityTable->item(29,2)->setBackgroundColor(Qt::lightGray);

    //Identifiers and names of the fourth category
    m_tracabilityTable->setItem(30,0, new QTableWidgetItem("F197"));
    m_tracabilityTable->setItem(30,1, new QTableWidgetItem("System Name"));
    m_tracabilityTable->setItem(31,0, new QTableWidgetItem("F18A"));
    m_tracabilityTable->setItem(31,1, new QTableWidgetItem("Supplier Name"));
    m_tracabilityTable->setItem(32,0, new QTableWidgetItem("F18C"));
    m_tracabilityTable->setItem(32,1, new QTableWidgetItem("Serial Number"));
    m_tracabilityTable->setItem(33,0, new QTableWidgetItem("F18B"));
    m_tracabilityTable->setItem(33,1, new QTableWidgetItem("Manufacturing Date"));
    m_tracabilityTable->setItem(34,0, new QTableWidgetItem("F192"));
    m_tracabilityTable->setItem(34,1, new QTableWidgetItem("Hardware Number ID"));
    m_tracabilityTable->setItem(35,0, new QTableWidgetItem("F193"));
    m_tracabilityTable->setItem(35,1, new QTableWidgetItem("Hardware Number Version"));
    m_tracabilityTable->setItem(36,0, new QTableWidgetItem("F195"));
    m_tracabilityTable->setItem(36,1, new QTableWidgetItem("Application Software Number TAG"));
    m_tracabilityTable->setItem(37,0, new QTableWidgetItem("F126"));
    m_tracabilityTable->setItem(37,1, new QTableWidgetItem("Bootloader Software Number TAG"));

    m_tracabilityTable->setItem(38,0, new QTableWidgetItem("Protocols"));                                   //Title of the fifth category, the line is grayed out
    m_tracabilityTable->item(38,0)->setBackgroundColor(Qt::lightGray);
    m_tracabilityTable->setItem(38,1, new QTableWidgetItem(""));
    m_tracabilityTable->item(38,1)->setBackgroundColor(Qt::lightGray);
    m_tracabilityTable->setItem(38,2, new QTableWidgetItem(""));
    m_tracabilityTable->item(38,2)->setBackgroundColor(Qt::lightGray);

    //Identifiers and names of the fifth category
    m_tracabilityTable->setItem(39,0, new QTableWidgetItem("FE10"));
    m_tracabilityTable->setItem(39,1, new QTableWidgetItem("Modbus Version ID(Main Board)"));
    m_tracabilityTable->setItem(40,0, new QTableWidgetItem("FE33"));
    m_tracabilityTable->setItem(40,1, new QTableWidgetItem("Modbus Version ID(Safety Controller)"));
    m_tracabilityTable->setItem(41,0, new QTableWidgetItem("FE11"));
    m_tracabilityTable->setItem(41,1, new QTableWidgetItem("HSIS Version ID(Main Board)"));
    m_tracabilityTable->setItem(42,0, new QTableWidgetItem("FE31"));
    m_tracabilityTable->setItem(42,1, new QTableWidgetItem("HSIS Version ID(Safety Controller)"));
    m_tracabilityTable->setItem(43,0, new QTableWidgetItem("FE32"));
    m_tracabilityTable->setItem(43,1, new QTableWidgetItem("UDS Version ID(Safety Controller)"));
    m_tracabilityTable->setItem(44,0, new QTableWidgetItem("FE36"));
    m_tracabilityTable->setItem(44,1, new QTableWidgetItem("CAN DB Version (Private CAN)"));
    setWindowModality(Qt::ApplicationModal);
}
TraceabilityWindow::~TraceabilityWindow()
{
    //It mustn't be destroyed in order to re-use it whenever showDetails is called
}
void TraceabilityWindow::traceability()
{
    QString clipboardString;
        QModelIndexList selectedIndexes = m_tracabilityTable->selectionModel()->selectedIndexes();

        for (int i = 0; i < selectedIndexes.count(); ++i)
        {
            QModelIndex current = selectedIndexes[i];
            QString displayText = current.data(Qt::DisplayRole).toString();
            qDebug() << QApplication::clipboard()->supportsSelection();
            // If there exists another column beyond this one.
            if (i + 1 < selectedIndexes.count())
            {
                QModelIndex next = selectedIndexes[i+1];

                // If the column is on different row, the clipboard should take note.
                if (next.row() != current.row())
                {
                    displayText.append("\n");
                }
                else
                {
                    // Otherwise append a column separator.
                    displayText.append(" | ");
                }
            }
            clipboardString.append(displayText);
        }

        QApplication::clipboard()->setText(clipboardString);
        //QApplication::clipboard()->setText("test");
}
void TraceabilityWindow::ButtonQuit()
{
    this->close();                                                         //closes the window without destroying it
}

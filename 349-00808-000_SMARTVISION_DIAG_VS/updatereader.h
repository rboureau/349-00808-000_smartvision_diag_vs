#ifndef UPDATEREADER_H
#define UPDATEREADER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>
#include <QDebug>
class UpdateReader: public QObject
        {
            Q_OBJECT
        public:
    explicit UpdateReader(QObject *parent = nullptr);
signals:
    void emitString(QString, bool);
    //void emitString(QString);
public slots:
    void newConnection();
    void readPort();

private:
        QTcpServer *m_server;
        QTcpSocket *m_socket;
        //QTcpSocket* socks[2];
        QVector<QTcpSocket*> m_socks;
        int m_numberConnexions;
};

#endif // UPDATEREADER_H

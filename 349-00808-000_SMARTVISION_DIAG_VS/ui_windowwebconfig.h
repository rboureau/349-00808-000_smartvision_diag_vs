/********************************************************************************
** Form generated from reading UI file 'windowwebconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOWWEBCONFIG_H
#define UI_WINDOWWEBCONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>
#include "qwebengineview.h"

QT_BEGIN_NAMESPACE

class Ui_WindowWebConfig
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *tab_Reference;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox_ReferenceButtonsNextCancel;
    QGridLayout *gridLayout_2;
    QPushButton *pushButton_ReferenceCancel;
    QSpacerItem *horizontalSpacer_13;
    QPushButton *pushButton_ReferenceNext;
    QGroupBox *groupBox_ReferenceInput;
    QGridLayout *gridLayout_4;
    QLineEdit *lineEdit_ReferenceInput;
    QLabel *labelReferenceText;
    QLabel *label_ReferenceLeftNumber;
    QLabel *label_ReferenceRightNumber;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_ReferenceEmpty2;
    QWidget *tab_ConnectDriver;
    QGridLayout *gridLayout_6;
    QSpacerItem *verticalSpacer_3;
    QGroupBox *groupBox_ConnectDriverButtonsNextCancel;
    QGridLayout *gridLayout_5;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_ConnectDriverNext;
    QPushButton *pushButton_ConnectDriverCancel;
    QLabel *label_ConnectDriverText;
    QLabel *label_ConnectDriverEmpty1;
    QSpacerItem *horizontalSpacer_4;
    QWidget *tab_WebConfigDriver;
    QGridLayout *gridLayout_8;
    QLabel *label_WebConfigDriverEmpty1;
    QLabel *label_WebConfigDriverEmpty2;
    QGroupBox *groupBox_WebConfigDriverNavigator;
    QGridLayout *gridLayout_19;
    QWebEngineView *WebEngineViewDriver;
    QGroupBox *groupBox_WebConfigDriverButtonsNextCancel;
    QGridLayout *gridLayout_7;
    QPushButton *pushButton_WebConfigDriverNext;
    QPushButton *pushButton_WebConfigDriverCancel;
    QSpacerItem *horizontalSpacer_5;
    QWidget *tab_ConnectPassenger;
    QGridLayout *gridLayout_10;
    QLabel *label_ConnectPassengerText;
    QLabel *label_ConnectPassengerEmpty1;
    QSpacerItem *verticalSpacer_4;
    QGroupBox *groupBox_ConnectPassengerButtonsNextCancel;
    QGridLayout *gridLayout_9;
    QPushButton *pushButton_ConnectPassengerNext;
    QPushButton *pushButton_ConnectPassengerCancel;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *horizontalSpacer_7;
    QWidget *tab_WebConfigPassenger;
    QGridLayout *gridLayout_12;
    QGroupBox *groupBox_WebConfigPassengerNavigator;
    QGridLayout *gridLayout_20;
    QWebEngineView *WebEngineViewPassenger;
    QLabel *label_WebConfigPassengerEmpty1;
    QGroupBox *groupBox_WebConfigPassengerButtonsNextCancel;
    QGridLayout *gridLayout_11;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *pushButton_WebConfigPassengerNext;
    QPushButton *pushButton_WebConfigPassengerCancel;
    QWidget *tab_Comment;
    QGridLayout *gridLayout_15;
    QGroupBox *groupBox_CommentTextZone;
    QGridLayout *gridLayout_13;
    QLabel *label_CommentText;
    QTextEdit *textEdit_CommentInput;
    QGroupBox *groupBox_CommentButtonsNextCancel;
    QGridLayout *gridLayout_14;
    QPushButton *pushButton_CommentNext;
    QPushButton *pushButton_CommentCancel;
    QSpacerItem *horizontalSpacer_9;
    QWidget *tab_SendFile;
    QGridLayout *gridLayout_18;
    QLabel *label_SendFileEmpty1;
    QSpacerItem *verticalSpacer_5;
    QGroupBox *groupBox_SendFileButtonsSendFilesCancel;
    QGridLayout *gridLayout_16;
    QSpacerItem *horizontalSpacer_12;
    QPushButton *pushButton_SendFiles;
    QPushButton *pushButton_SendFileCancel;
    QGroupBox *groupBox_SendFileText;
    QGridLayout *gridLayout_17;
    QSpacerItem *horizontalSpacer_10;
    QLabel *label_SendFileText1;
    QLabel *label_SendFileText2;
    QSpacerItem *horizontalSpacer_11;
    QSpacerItem *verticalSpacer_6;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *WindowWebConfig)
    {
        if (WindowWebConfig->objectName().isEmpty())
            WindowWebConfig->setObjectName(QStringLiteral("WindowWebConfig"));
        WindowWebConfig->resize(800, 600);
        centralwidget = new QWidget(WindowWebConfig);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setEnabled(true);
        tabWidget->setTabBarAutoHide(false);
        tab_Reference = new QWidget();
        tab_Reference->setObjectName(QStringLiteral("tab_Reference"));
        tab_Reference->setEnabled(true);
        gridLayout_3 = new QGridLayout(tab_Reference);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        groupBox_ReferenceButtonsNextCancel = new QGroupBox(tab_Reference);
        groupBox_ReferenceButtonsNextCancel->setObjectName(QStringLiteral("groupBox_ReferenceButtonsNextCancel"));
        groupBox_ReferenceButtonsNextCancel->setStyleSheet(QStringLiteral("#groupBox_ReferenceButtonsNextCancel{border:null;}"));
        gridLayout_2 = new QGridLayout(groupBox_ReferenceButtonsNextCancel);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        pushButton_ReferenceCancel = new QPushButton(groupBox_ReferenceButtonsNextCancel);
        pushButton_ReferenceCancel->setObjectName(QStringLiteral("pushButton_ReferenceCancel"));

        gridLayout_2->addWidget(pushButton_ReferenceCancel, 0, 2, 1, 1);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_13, 0, 0, 1, 1);

        pushButton_ReferenceNext = new QPushButton(groupBox_ReferenceButtonsNextCancel);
        pushButton_ReferenceNext->setObjectName(QStringLiteral("pushButton_ReferenceNext"));

        gridLayout_2->addWidget(pushButton_ReferenceNext, 0, 1, 1, 1);


        gridLayout_3->addWidget(groupBox_ReferenceButtonsNextCancel, 3, 2, 1, 1);

        groupBox_ReferenceInput = new QGroupBox(tab_Reference);
        groupBox_ReferenceInput->setObjectName(QStringLiteral("groupBox_ReferenceInput"));
        groupBox_ReferenceInput->setStyleSheet(QStringLiteral("#groupBox_ReferenceInput {border:null;}"));
        gridLayout_4 = new QGridLayout(groupBox_ReferenceInput);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        lineEdit_ReferenceInput = new QLineEdit(groupBox_ReferenceInput);
        lineEdit_ReferenceInput->setObjectName(QStringLiteral("lineEdit_ReferenceInput"));

        gridLayout_4->addWidget(lineEdit_ReferenceInput, 1, 1, 1, 1);

        labelReferenceText = new QLabel(groupBox_ReferenceInput);
        labelReferenceText->setObjectName(QStringLiteral("labelReferenceText"));

        gridLayout_4->addWidget(labelReferenceText, 0, 1, 1, 2);

        label_ReferenceLeftNumber = new QLabel(groupBox_ReferenceInput);
        label_ReferenceLeftNumber->setObjectName(QStringLiteral("label_ReferenceLeftNumber"));

        gridLayout_4->addWidget(label_ReferenceLeftNumber, 1, 0, 1, 1);

        label_ReferenceRightNumber = new QLabel(groupBox_ReferenceInput);
        label_ReferenceRightNumber->setObjectName(QStringLiteral("label_ReferenceRightNumber"));

        gridLayout_4->addWidget(label_ReferenceRightNumber, 1, 2, 1, 1);


        gridLayout_3->addWidget(groupBox_ReferenceInput, 1, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer_2, 2, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer, 0, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 1, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_2, 1, 2, 1, 1);

        label_ReferenceEmpty2 = new QLabel(tab_Reference);
        label_ReferenceEmpty2->setObjectName(QStringLiteral("label_ReferenceEmpty2"));

        gridLayout_3->addWidget(label_ReferenceEmpty2, 3, 0, 1, 2);

        tabWidget->addTab(tab_Reference, QString());
        tab_ConnectDriver = new QWidget();
        tab_ConnectDriver->setObjectName(QStringLiteral("tab_ConnectDriver"));
        tab_ConnectDriver->setEnabled(true);
        gridLayout_6 = new QGridLayout(tab_ConnectDriver);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_6->addItem(verticalSpacer_3, 0, 2, 1, 1);

        groupBox_ConnectDriverButtonsNextCancel = new QGroupBox(tab_ConnectDriver);
        groupBox_ConnectDriverButtonsNextCancel->setObjectName(QStringLiteral("groupBox_ConnectDriverButtonsNextCancel"));
        groupBox_ConnectDriverButtonsNextCancel->setStyleSheet(QStringLiteral("#groupBox_ConnectDriverButtonsNextCancel {border:null;}"));
        gridLayout_5 = new QGridLayout(groupBox_ConnectDriverButtonsNextCancel);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        horizontalSpacer_3 = new QSpacerItem(80, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer_3, 0, 0, 1, 1);

        pushButton_ConnectDriverNext = new QPushButton(groupBox_ConnectDriverButtonsNextCancel);
        pushButton_ConnectDriverNext->setObjectName(QStringLiteral("pushButton_ConnectDriverNext"));

        gridLayout_5->addWidget(pushButton_ConnectDriverNext, 0, 1, 1, 1);

        pushButton_ConnectDriverCancel = new QPushButton(groupBox_ConnectDriverButtonsNextCancel);
        pushButton_ConnectDriverCancel->setObjectName(QStringLiteral("pushButton_ConnectDriverCancel"));

        gridLayout_5->addWidget(pushButton_ConnectDriverCancel, 0, 2, 1, 1);


        gridLayout_6->addWidget(groupBox_ConnectDriverButtonsNextCancel, 1, 2, 1, 1);

        label_ConnectDriverText = new QLabel(tab_ConnectDriver);
        label_ConnectDriverText->setObjectName(QStringLiteral("label_ConnectDriverText"));

        gridLayout_6->addWidget(label_ConnectDriverText, 0, 1, 1, 1);

        label_ConnectDriverEmpty1 = new QLabel(tab_ConnectDriver);
        label_ConnectDriverEmpty1->setObjectName(QStringLiteral("label_ConnectDriverEmpty1"));

        gridLayout_6->addWidget(label_ConnectDriverEmpty1, 1, 1, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer_4, 0, 0, 1, 1);

        tabWidget->addTab(tab_ConnectDriver, QString());
        tab_WebConfigDriver = new QWidget();
        tab_WebConfigDriver->setObjectName(QStringLiteral("tab_WebConfigDriver"));
        gridLayout_8 = new QGridLayout(tab_WebConfigDriver);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        label_WebConfigDriverEmpty1 = new QLabel(tab_WebConfigDriver);
        label_WebConfigDriverEmpty1->setObjectName(QStringLiteral("label_WebConfigDriverEmpty1"));

        gridLayout_8->addWidget(label_WebConfigDriverEmpty1, 1, 0, 1, 1);

        label_WebConfigDriverEmpty2 = new QLabel(tab_WebConfigDriver);
        label_WebConfigDriverEmpty2->setObjectName(QStringLiteral("label_WebConfigDriverEmpty2"));

        gridLayout_8->addWidget(label_WebConfigDriverEmpty2, 1, 1, 1, 1);

        groupBox_WebConfigDriverNavigator = new QGroupBox(tab_WebConfigDriver);
        groupBox_WebConfigDriverNavigator->setObjectName(QStringLiteral("groupBox_WebConfigDriverNavigator"));
        gridLayout_19 = new QGridLayout(groupBox_WebConfigDriverNavigator);
        gridLayout_19->setObjectName(QStringLiteral("gridLayout_19"));
        WebEngineViewDriver = new QWebEngineView(groupBox_WebConfigDriverNavigator);
        WebEngineViewDriver->setObjectName(QStringLiteral("WebEngineViewDriver"));

        gridLayout_19->addWidget(WebEngineViewDriver, 0, 0, 1, 1);


        gridLayout_8->addWidget(groupBox_WebConfigDriverNavigator, 0, 0, 1, 3);

        groupBox_WebConfigDriverButtonsNextCancel = new QGroupBox(tab_WebConfigDriver);
        groupBox_WebConfigDriverButtonsNextCancel->setObjectName(QStringLiteral("groupBox_WebConfigDriverButtonsNextCancel"));
        groupBox_WebConfigDriverButtonsNextCancel->setMaximumSize(QSize(263, 41));
        groupBox_WebConfigDriverButtonsNextCancel->setStyleSheet(QStringLiteral("#groupBox_WebConfigDriverButtonsNextCancel{border:null;}"));
        gridLayout_7 = new QGridLayout(groupBox_WebConfigDriverButtonsNextCancel);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        pushButton_WebConfigDriverNext = new QPushButton(groupBox_WebConfigDriverButtonsNextCancel);
        pushButton_WebConfigDriverNext->setObjectName(QStringLiteral("pushButton_WebConfigDriverNext"));
        pushButton_WebConfigDriverNext->setEnabled(false);

        gridLayout_7->addWidget(pushButton_WebConfigDriverNext, 0, 1, 1, 1);

        pushButton_WebConfigDriverCancel = new QPushButton(groupBox_WebConfigDriverButtonsNextCancel);
        pushButton_WebConfigDriverCancel->setObjectName(QStringLiteral("pushButton_WebConfigDriverCancel"));

        gridLayout_7->addWidget(pushButton_WebConfigDriverCancel, 0, 2, 1, 1);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_7->addItem(horizontalSpacer_5, 0, 0, 1, 1);


        gridLayout_8->addWidget(groupBox_WebConfigDriverButtonsNextCancel, 1, 2, 1, 1);

        tabWidget->addTab(tab_WebConfigDriver, QString());
        tab_ConnectPassenger = new QWidget();
        tab_ConnectPassenger->setObjectName(QStringLiteral("tab_ConnectPassenger"));
        gridLayout_10 = new QGridLayout(tab_ConnectPassenger);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        label_ConnectPassengerText = new QLabel(tab_ConnectPassenger);
        label_ConnectPassengerText->setObjectName(QStringLiteral("label_ConnectPassengerText"));

        gridLayout_10->addWidget(label_ConnectPassengerText, 0, 1, 1, 1);

        label_ConnectPassengerEmpty1 = new QLabel(tab_ConnectPassenger);
        label_ConnectPassengerEmpty1->setObjectName(QStringLiteral("label_ConnectPassengerEmpty1"));

        gridLayout_10->addWidget(label_ConnectPassengerEmpty1, 1, 1, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_10->addItem(verticalSpacer_4, 0, 2, 1, 1);

        groupBox_ConnectPassengerButtonsNextCancel = new QGroupBox(tab_ConnectPassenger);
        groupBox_ConnectPassengerButtonsNextCancel->setObjectName(QStringLiteral("groupBox_ConnectPassengerButtonsNextCancel"));
        groupBox_ConnectPassengerButtonsNextCancel->setStyleSheet(QStringLiteral("#groupBox_ConnectPassengerButtonsNextCancel {border:null;}"));
        gridLayout_9 = new QGridLayout(groupBox_ConnectPassengerButtonsNextCancel);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        pushButton_ConnectPassengerNext = new QPushButton(groupBox_ConnectPassengerButtonsNextCancel);
        pushButton_ConnectPassengerNext->setObjectName(QStringLiteral("pushButton_ConnectPassengerNext"));

        gridLayout_9->addWidget(pushButton_ConnectPassengerNext, 0, 1, 1, 1);

        pushButton_ConnectPassengerCancel = new QPushButton(groupBox_ConnectPassengerButtonsNextCancel);
        pushButton_ConnectPassengerCancel->setObjectName(QStringLiteral("pushButton_ConnectPassengerCancel"));

        gridLayout_9->addWidget(pushButton_ConnectPassengerCancel, 0, 2, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_9->addItem(horizontalSpacer_6, 0, 0, 1, 1);


        gridLayout_10->addWidget(groupBox_ConnectPassengerButtonsNextCancel, 1, 2, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_10->addItem(horizontalSpacer_7, 0, 0, 1, 1);

        tabWidget->addTab(tab_ConnectPassenger, QString());
        tab_WebConfigPassenger = new QWidget();
        tab_WebConfigPassenger->setObjectName(QStringLiteral("tab_WebConfigPassenger"));
        gridLayout_12 = new QGridLayout(tab_WebConfigPassenger);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        groupBox_WebConfigPassengerNavigator = new QGroupBox(tab_WebConfigPassenger);
        groupBox_WebConfigPassengerNavigator->setObjectName(QStringLiteral("groupBox_WebConfigPassengerNavigator"));
        gridLayout_20 = new QGridLayout(groupBox_WebConfigPassengerNavigator);
        gridLayout_20->setObjectName(QStringLiteral("gridLayout_20"));
        WebEngineViewPassenger = new QWebEngineView(groupBox_WebConfigPassengerNavigator);
        WebEngineViewPassenger->setObjectName(QStringLiteral("WebEngineViewPassenger"));

        gridLayout_20->addWidget(WebEngineViewPassenger, 0, 0, 1, 1);


        gridLayout_12->addWidget(groupBox_WebConfigPassengerNavigator, 0, 0, 1, 2);

        label_WebConfigPassengerEmpty1 = new QLabel(tab_WebConfigPassenger);
        label_WebConfigPassengerEmpty1->setObjectName(QStringLiteral("label_WebConfigPassengerEmpty1"));

        gridLayout_12->addWidget(label_WebConfigPassengerEmpty1, 1, 0, 1, 1);

        groupBox_WebConfigPassengerButtonsNextCancel = new QGroupBox(tab_WebConfigPassenger);
        groupBox_WebConfigPassengerButtonsNextCancel->setObjectName(QStringLiteral("groupBox_WebConfigPassengerButtonsNextCancel"));
        groupBox_WebConfigPassengerButtonsNextCancel->setMaximumSize(QSize(263, 41));
        groupBox_WebConfigPassengerButtonsNextCancel->setStyleSheet(QStringLiteral("#groupBox_WebConfigPassengerButtonsNextCancel {border:null;}"));
        gridLayout_11 = new QGridLayout(groupBox_WebConfigPassengerButtonsNextCancel);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        horizontalSpacer_8 = new QSpacerItem(80, 18, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_11->addItem(horizontalSpacer_8, 0, 0, 1, 1);

        pushButton_WebConfigPassengerNext = new QPushButton(groupBox_WebConfigPassengerButtonsNextCancel);
        pushButton_WebConfigPassengerNext->setObjectName(QStringLiteral("pushButton_WebConfigPassengerNext"));
        pushButton_WebConfigPassengerNext->setEnabled(false);

        gridLayout_11->addWidget(pushButton_WebConfigPassengerNext, 0, 1, 1, 1);

        pushButton_WebConfigPassengerCancel = new QPushButton(groupBox_WebConfigPassengerButtonsNextCancel);
        pushButton_WebConfigPassengerCancel->setObjectName(QStringLiteral("pushButton_WebConfigPassengerCancel"));

        gridLayout_11->addWidget(pushButton_WebConfigPassengerCancel, 0, 2, 1, 1);


        gridLayout_12->addWidget(groupBox_WebConfigPassengerButtonsNextCancel, 1, 1, 1, 1);

        tabWidget->addTab(tab_WebConfigPassenger, QString());
        tab_Comment = new QWidget();
        tab_Comment->setObjectName(QStringLiteral("tab_Comment"));
        gridLayout_15 = new QGridLayout(tab_Comment);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        groupBox_CommentTextZone = new QGroupBox(tab_Comment);
        groupBox_CommentTextZone->setObjectName(QStringLiteral("groupBox_CommentTextZone"));
        groupBox_CommentTextZone->setStyleSheet(QStringLiteral("#groupBox_CommentTextZone {border:null;}"));
        gridLayout_13 = new QGridLayout(groupBox_CommentTextZone);
        gridLayout_13->setObjectName(QStringLiteral("gridLayout_13"));
        label_CommentText = new QLabel(groupBox_CommentTextZone);
        label_CommentText->setObjectName(QStringLiteral("label_CommentText"));

        gridLayout_13->addWidget(label_CommentText, 0, 0, 1, 1);

        textEdit_CommentInput = new QTextEdit(groupBox_CommentTextZone);
        textEdit_CommentInput->setObjectName(QStringLiteral("textEdit_CommentInput"));
        QFont font;
        font.setPointSize(12);
        textEdit_CommentInput->setFont(font);

        gridLayout_13->addWidget(textEdit_CommentInput, 1, 0, 1, 1);


        gridLayout_15->addWidget(groupBox_CommentTextZone, 0, 0, 1, 2);

        groupBox_CommentButtonsNextCancel = new QGroupBox(tab_Comment);
        groupBox_CommentButtonsNextCancel->setObjectName(QStringLiteral("groupBox_CommentButtonsNextCancel"));
        groupBox_CommentButtonsNextCancel->setStyleSheet(QStringLiteral("#groupBox_CommentButtonsNextCancel {border:null;}"));
        gridLayout_14 = new QGridLayout(groupBox_CommentButtonsNextCancel);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        pushButton_CommentNext = new QPushButton(groupBox_CommentButtonsNextCancel);
        pushButton_CommentNext->setObjectName(QStringLiteral("pushButton_CommentNext"));
        pushButton_CommentNext->setEnabled(true);

        gridLayout_14->addWidget(pushButton_CommentNext, 0, 1, 1, 1);

        pushButton_CommentCancel = new QPushButton(groupBox_CommentButtonsNextCancel);
        pushButton_CommentCancel->setObjectName(QStringLiteral("pushButton_CommentCancel"));

        gridLayout_14->addWidget(pushButton_CommentCancel, 0, 2, 1, 1);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_14->addItem(horizontalSpacer_9, 0, 0, 1, 1);


        gridLayout_15->addWidget(groupBox_CommentButtonsNextCancel, 1, 1, 1, 1);

        tabWidget->addTab(tab_Comment, QString());
        tab_SendFile = new QWidget();
        tab_SendFile->setObjectName(QStringLiteral("tab_SendFile"));
        gridLayout_18 = new QGridLayout(tab_SendFile);
        gridLayout_18->setObjectName(QStringLiteral("gridLayout_18"));
        label_SendFileEmpty1 = new QLabel(tab_SendFile);
        label_SendFileEmpty1->setObjectName(QStringLiteral("label_SendFileEmpty1"));

        gridLayout_18->addWidget(label_SendFileEmpty1, 3, 0, 1, 1);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_18->addItem(verticalSpacer_5, 2, 1, 1, 1);

        groupBox_SendFileButtonsSendFilesCancel = new QGroupBox(tab_SendFile);
        groupBox_SendFileButtonsSendFilesCancel->setObjectName(QStringLiteral("groupBox_SendFileButtonsSendFilesCancel"));
        groupBox_SendFileButtonsSendFilesCancel->setStyleSheet(QStringLiteral("#groupBox_SendFileButtonsSendFilesCancel {border:null;}"));
        gridLayout_16 = new QGridLayout(groupBox_SendFileButtonsSendFilesCancel);
        gridLayout_16->setObjectName(QStringLiteral("gridLayout_16"));
        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_16->addItem(horizontalSpacer_12, 0, 0, 1, 1);

        pushButton_SendFiles = new QPushButton(groupBox_SendFileButtonsSendFilesCancel);
        pushButton_SendFiles->setObjectName(QStringLiteral("pushButton_SendFiles"));

        gridLayout_16->addWidget(pushButton_SendFiles, 0, 2, 1, 1);

        pushButton_SendFileCancel = new QPushButton(groupBox_SendFileButtonsSendFilesCancel);
        pushButton_SendFileCancel->setObjectName(QStringLiteral("pushButton_SendFileCancel"));

        gridLayout_16->addWidget(pushButton_SendFileCancel, 0, 3, 1, 1);


        gridLayout_18->addWidget(groupBox_SendFileButtonsSendFilesCancel, 3, 1, 1, 1);

        groupBox_SendFileText = new QGroupBox(tab_SendFile);
        groupBox_SendFileText->setObjectName(QStringLiteral("groupBox_SendFileText"));
        groupBox_SendFileText->setStyleSheet(QStringLiteral("#groupBox_SendFileText {border:null;}"));
        gridLayout_17 = new QGridLayout(groupBox_SendFileText);
        gridLayout_17->setObjectName(QStringLiteral("gridLayout_17"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_17->addItem(horizontalSpacer_10, 0, 0, 1, 1);

        label_SendFileText1 = new QLabel(groupBox_SendFileText);
        label_SendFileText1->setObjectName(QStringLiteral("label_SendFileText1"));

        gridLayout_17->addWidget(label_SendFileText1, 0, 1, 1, 1);

        label_SendFileText2 = new QLabel(groupBox_SendFileText);
        label_SendFileText2->setObjectName(QStringLiteral("label_SendFileText2"));

        gridLayout_17->addWidget(label_SendFileText2, 1, 1, 1, 1);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_17->addItem(horizontalSpacer_11, 1, 2, 1, 1);


        gridLayout_18->addWidget(groupBox_SendFileText, 1, 0, 1, 2);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_18->addItem(verticalSpacer_6, 0, 0, 1, 1);

        tabWidget->addTab(tab_SendFile, QString());

        gridLayout->addWidget(tabWidget, 1, 0, 1, 1);

        WindowWebConfig->setCentralWidget(centralwidget);
        menubar = new QMenuBar(WindowWebConfig);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 21));
        WindowWebConfig->setMenuBar(menubar);
        statusbar = new QStatusBar(WindowWebConfig);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        WindowWebConfig->setStatusBar(statusbar);

        retranslateUi(WindowWebConfig);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(WindowWebConfig);
    } // setupUi

    void retranslateUi(QMainWindow *WindowWebConfig)
    {
        WindowWebConfig->setWindowTitle(QApplication::translate("WindowWebConfig", "Config Creation", Q_NULLPTR));
        groupBox_ReferenceButtonsNextCancel->setTitle(QString());
        pushButton_ReferenceCancel->setText(QApplication::translate("WindowWebConfig", "Cancel", Q_NULLPTR));
        pushButton_ReferenceNext->setText(QApplication::translate("WindowWebConfig", "Next", Q_NULLPTR));
        groupBox_ReferenceInput->setTitle(QString());
        labelReferenceText->setText(QApplication::translate("WindowWebConfig", "Enter the reference number :", Q_NULLPTR));
        label_ReferenceLeftNumber->setText(QApplication::translate("WindowWebConfig", "057-", Q_NULLPTR));
        label_ReferenceRightNumber->setText(QApplication::translate("WindowWebConfig", "-527", Q_NULLPTR));
        label_ReferenceEmpty2->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_Reference), QApplication::translate("WindowWebConfig", "Reference", Q_NULLPTR));
        groupBox_ConnectDriverButtonsNextCancel->setTitle(QString());
        pushButton_ConnectDriverNext->setText(QApplication::translate("WindowWebConfig", "Next", Q_NULLPTR));
        pushButton_ConnectDriverCancel->setText(QApplication::translate("WindowWebConfig", "Cancel", Q_NULLPTR));
        label_ConnectDriverText->setText(QApplication::translate("WindowWebConfig", "Please Connect to the Driver-side SmartVision", Q_NULLPTR));
        label_ConnectDriverEmpty1->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_ConnectDriver), QApplication::translate("WindowWebConfig", "Connect Driver", Q_NULLPTR));
        label_WebConfigDriverEmpty1->setText(QString());
        label_WebConfigDriverEmpty2->setText(QString());
        groupBox_WebConfigDriverNavigator->setTitle(QString());
        groupBox_WebConfigDriverButtonsNextCancel->setTitle(QString());
        pushButton_WebConfigDriverNext->setText(QApplication::translate("WindowWebConfig", "Next", Q_NULLPTR));
        pushButton_WebConfigDriverCancel->setText(QApplication::translate("WindowWebConfig", "Cancel", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_WebConfigDriver), QApplication::translate("WindowWebConfig", "Web Config Driver", Q_NULLPTR));
        label_ConnectPassengerText->setText(QApplication::translate("WindowWebConfig", "Please Connect to the Passenger-side SmartVision", Q_NULLPTR));
        label_ConnectPassengerEmpty1->setText(QString());
        groupBox_ConnectPassengerButtonsNextCancel->setTitle(QString());
        pushButton_ConnectPassengerNext->setText(QApplication::translate("WindowWebConfig", "Next", Q_NULLPTR));
        pushButton_ConnectPassengerCancel->setText(QApplication::translate("WindowWebConfig", "Cancel", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_ConnectPassenger), QApplication::translate("WindowWebConfig", "Connect Passenger", Q_NULLPTR));
        groupBox_WebConfigPassengerNavigator->setTitle(QString());
        label_WebConfigPassengerEmpty1->setText(QString());
        groupBox_WebConfigPassengerButtonsNextCancel->setTitle(QString());
        pushButton_WebConfigPassengerNext->setText(QApplication::translate("WindowWebConfig", "Next", Q_NULLPTR));
        pushButton_WebConfigPassengerCancel->setText(QApplication::translate("WindowWebConfig", "Cancel", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_WebConfigPassenger), QApplication::translate("WindowWebConfig", "Web Config Passenger", Q_NULLPTR));
        groupBox_CommentTextZone->setTitle(QString());
        label_CommentText->setText(QApplication::translate("WindowWebConfig", "Please enter a comment about why you did this configuration", Q_NULLPTR));
        textEdit_CommentInput->setHtml(QApplication::translate("WindowWebConfig", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", Q_NULLPTR));
        groupBox_CommentButtonsNextCancel->setTitle(QString());
        pushButton_CommentNext->setText(QApplication::translate("WindowWebConfig", "Next", Q_NULLPTR));
        pushButton_CommentCancel->setText(QApplication::translate("WindowWebConfig", "Cancel", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_Comment), QApplication::translate("WindowWebConfig", "Comment", Q_NULLPTR));
        label_SendFileEmpty1->setText(QString());
        groupBox_SendFileButtonsSendFilesCancel->setTitle(QString());
        pushButton_SendFiles->setText(QApplication::translate("WindowWebConfig", "Send Files", Q_NULLPTR));
        pushButton_SendFileCancel->setText(QApplication::translate("WindowWebConfig", "Cancel", Q_NULLPTR));
        groupBox_SendFileText->setTitle(QString());
        label_SendFileText1->setText(QApplication::translate("WindowWebConfig", "The Configuration is completed.", Q_NULLPTR));
        label_SendFileText2->setText(QApplication::translate("WindowWebConfig", "Please make sure everything is fine before sending the files", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_SendFile), QApplication::translate("WindowWebConfig", "Send File", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class WindowWebConfig: public Ui_WindowWebConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOWWEBCONFIG_H

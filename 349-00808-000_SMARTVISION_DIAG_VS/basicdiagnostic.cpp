#include "basicdiagnostic.h"
#include "ui_basicdiagnostic.h"

BasicDiagnostic::BasicDiagnostic(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::BasicDiagnostic)
{
    ui->setupUi(this);
    details = new Fenetretracability(nullptr);  //Creates the detailed traceability to be able to manage its data at any moment
    details->hide();                            //Hides this window temporarily
    //connects HMI elements to the corresponding slots
    connect(ui->push_B_traceability_detail,&QPushButton::clicked,this,&BasicDiagnostic::ButtonDetails);
    connect(ui->tool_B_firmware_upgrade_recherche, &QToolButton::clicked,this,&BasicDiagnostic::ButtonResearchVbfTar);
    connect(ui->push_B_Traceability_update, &QPushButton::clicked, this, &BasicDiagnostic::updateTraca);
    connect(ui->radio_B_Driver, &QRadioButton::clicked,this,&BasicDiagnostic::changeSide);
    connect(ui->radio_B_Passenger, &QRadioButton::clicked,this,&BasicDiagnostic::changeSide);
    connect(ui->push_B_firmware_upgrade_SC,&QPushButton::clicked, this, &BasicDiagnostic::ButtonUpdateSafetyController);
    connect(ui->line_E_firmware_upgrade_chemin,&QLineEdit::textChanged,this, &BasicDiagnostic::setEnableButtonUpdateSC);
    connect(ui->pushButton,&QPushButton::clicked,this,&BasicDiagnostic::emitEcuReset);

}

BasicDiagnostic::~BasicDiagnostic()
{
    delete ui;
}
void BasicDiagnostic::ButtonDetails()
{

    details->show();                    //this window is already created, we just have to make it visible.

}
void BasicDiagnostic::updateTraca()
{
    emit emitUpdateRequest();           //All the diagnosis functions including traceability are in the SmartVision object in MainWindow. Signals will be emitted to use these.
}
void BasicDiagnostic::ButtonResearchVbfTar()
{
    QString fileFirmwareUpgrade = QFileDialog::getOpenFileName(this, "Open File", QString(), "Firmware filter (*.vbf *.tar)");          //Retrieves the path to the selected file. VBF and TAR files only

    ui->line_E_firmware_upgrade_chemin->clear();
    ui->line_E_firmware_upgrade_chemin->setText(fileFirmwareUpgrade);                                                               //Show the path in the corresponding lineEdit
}

QString BasicDiagnostic::getMode()
{
    //Method to retrieve the corresponding IP address of the selected radioButton
    if(ui->radio_B_Driver->isChecked()){
        return "192.168.69.1";
    }else if(ui->radio_B_Passenger->isChecked()) {
        return "192.168.69.2";
    }
    return "192.168.69.1";
}
void BasicDiagnostic::changeSide(){
    //This function is called when the user selects the other radioButton
    // it checks which radioButton is active, and signals the change to the SmartVision object which can change the IP address for the diagnosis.
    if(ui->radio_B_Driver->isChecked()){
            emit emitChangeSide(true);

    }else if(ui->radio_B_Passenger->isChecked()){
            emit emitChangeSide(false);
    }
}
void BasicDiagnostic::ButtonUpdateSafetyController(){
    //Sends the path of the Safety Controller update file to the SmartVision object to perform the update
    emit emitCheminUpdateSC(ui->line_E_firmware_upgrade_chemin->text());
}
void BasicDiagnostic::changeProgressBar(int value){
    //When the Safety Controller Update progression signal is received, the progressionBar is updated to the new value
    ui->progressBar->setValue(value);
}
void BasicDiagnostic::setEnableButtonUpdateSC(){
    //Cette méthode teste la ligne pour le chemin du fichier de mise à jour Safety Controller à chaque changement du texte de la barre afin d'activer le bouton de mise à jour lorsque le chemin est correct
    //this function tests the path in the lineEdit for the Safety Controller Update. if the path is correct, the button to start the update is enabled.
    QString cheminFichierMAJ= ui->line_E_firmware_upgrade_chemin->text();
    //The file must exist and be a VBF or TAR File
    if(QFile(cheminFichierMAJ).exists() && (cheminFichierMAJ.endsWith(".vbf") || cheminFichierMAJ.endsWith(".tar"))){
        ui->push_B_firmware_upgrade_SC->setEnabled(true);
    }else{
        ui->push_B_firmware_upgrade_SC->setEnabled(false);
    }
}
void BasicDiagnostic::changeIHMTracabilityValues(QHash<QString,QString> hashvalues){
        //Refresh the HMI after the traceability changed
        //essential traceability data
        ui->label_SystemSoftwareBaselineIDOutput->clear();
        ui->label_SystemSoftwareBaselineIDOutput->setText(hashvalues.value("FE26"));
        ui->label_ConfigurationFileVersionIDOutput->clear();
        ui->label_ConfigurationFileVersionIDOutput->setText(hashvalues.value("FE0E"));
        ui->label_SerialNumberOutput->clear();
        ui->label_SerialNumberOutput->setText(hashvalues.value("F18C"));
        ui->label_ApplicationSoftwareNumberTAGOutput->clear();
        ui->label_ApplicationSoftwareNumberTAGOutput->setText(hashvalues.value("F195"));
        ui->label_BootloaderSoftwareNumberTAGOutput->clear();
        ui->label_BootloaderSoftwareNumberTAGOutput->setText(hashvalues.value("F126"));
        ui->label_PackageVersionIDOutput->clear();
        ui->label_PackageVersionIDOutput->setText(hashvalues.value("FE0C"));
        //detailed traceability data
        for(int i=0;i<details->visibility->rowCount();i++){
            if(details->visibility->item(i,0)->backgroundColor()!=Qt::lightGray){
                details->visibility->setItem(i,2,new QTableWidgetItem(hashvalues.value(details->visibility->item(i,0)->text())));
            }
        }
}

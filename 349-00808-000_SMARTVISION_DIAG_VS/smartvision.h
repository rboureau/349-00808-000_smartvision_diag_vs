#ifndef SMARTVISION_H
#define SMARTVISION_H

#include <QObject>
//#include "dll_visionsystems_sv.h"
#include <QDebug>
#include <QTableWidget>
#include <stdio.h>
#include <iostream>
#include <windows.h>
#include <stdlib.h>
#include <QUuid>
#include "cp_xcom.h"
#include "sp_chacom.h"
#include "dll_visionsystem_sv.h"
#include "QApplication"
#include "QWidget"
#include <QtConcurrent/QtConcurrent>
#include <QObject>
using namespace std;
using namespace QtMetaTypePrivate;
using namespace mscorlib;

using namespace SP_Chacom;
using namespace DLL_VisionSystem_SV;
using namespace CP_Xcom;
class SmartVision: public QObject
{
    Q_OBJECT
public:
    explicit SmartVision(QString port);
    QString getTracabilityElement(QString key);
    QHash<QString,QString> getTracability();
    void updateTracability();
    void isConnected();
signals:
    void signalProgressD(int);
    void signalProgressUM(int);
    void signalTracability(QHash<QString,QString>);
    void signalTracabilityElement(QString);
    void signalUpdateTracabilityInformationBefore(QStringList*);
    void signalUpdateTracabilityInformationAfter(QStringList*);
    void signalConnected();
    void signalNotConnected();
    void signalPingDriverSuccessfull();
    void signalPingPassengerSuccessfull();
    void signalPingDriverNotSuccessfull();
    void signalPingPassengerNotSuccessfull();
    void signalSCUpdateDriverConnected();
    void signalSVUpdateDriverConnected();
    void signalSCUpdatePassengerConnected();
    void signalSVUpdatePassengerConnected();
    void signalEnableSCUpdateButton();
    void signalNewSide(bool);
    void signalBothSides();
    void signalTracabilityDriverConnected();
    void signalTracabilityPassengerConnected();
    void signalECUResetDriverConnected();
    void signalECUResetPassengerConnected();
    void signalSCUpdateDiagDriverConnected();
    void signalSCUpdateDiagPassengerConnected();
private:
    QHash<QString,QString> m_HashSmartVision;
    QHash<QString,QString> m_HashID;
    PP_VisionSystem_SV m_Ppvs;
    QString m_sPath;
    void progressChangeD();
    void progressChangeUM();
    void updateSC();
    QStringList* tracabilityUpdateData();
    QProcess m_ping;
    int m_iDriverPingCount;
    int m_iPassengerPingCount;
    QString m_sAction;
    QTimer* m_timer;
    bool m_bDriver;
    bool m_bPassenger;
    bool m_bCurrentSide;
public slots:
    void multiThreadsD(QString value);
    void multiThreadsUM(QString value);
    void tracabilityUpdateBefore();
    void tracabilityUpdateAfter();
    void hardReset();
    void changeSide(bool driver);
    void pingEmitter(QString action,bool mode);
    void pongCatcherDriver();
    void pongCatcherPassenger();
    void launchActionDriver();
    void launchActionPassenger();
    void pingManager(QString action, QString sides);
    void parallelUpdateLaunch();
    void updateSCDriver();
    void updateSCPassenger();
};



#endif // SMARTVISION_H

#ifndef UPDATEMANAGER_H
#define UPDATEMANAGER_H

#include <QMainWindow>
#include <QProgressBar>
#include <QNetworkAccessManager>
#include "filetransfer.h"
#include "updatereader.h"
#include <QMessageBox>
#include <QTimer>
#include <QElapsedTimer>
#include <QFileDialog>
#include <QSettings>
#include "smartvision.h"
#include <QInputDialog>
#include <QSerialPortInfo>
namespace Ui {
class UpdateManager;
}

class UpdateManager : public QMainWindow
{
    Q_OBJECT

public:
    explicit UpdateManager(QWidget *parent = nullptr);
    ~UpdateManager();
signals:
    void signalRequestTracabilityBefore();
    void signalRequestTracabilityAfter();
    void signalHardReset();
    void signalLaunchUpdateSC(QString);
    void signalPingRequest(QString action,QString sides);
    void signalRefreshSide();
    void signalDisableMainWindowButtons();
    void signalEnableMainWindowButtons();
private:
    Ui::UpdateManager *ui;

    void ButtonResearchZipFile();
    void SendZip(QString mode);
    void ButtonSendZip();
    void updateLogDisplay(QString s, bool mode);
    //void affichageLogMaJ(QString s);
    void errorColorBarFtp();
    void errorColorBarLog();
    void finishedColorBarFtp();
    void StartValueBarFtp();
    //void getElementsJournalTraceability(QString portName);
    void getLogTraceabilityElements();
    QString m_sSerialNumber;
    QString m_sSoftBeforeUpdate;
    QString m_sSoftAfterUpdate;
    UpdateReader m_reader;
    QTimer *m_loop;
    QElapsedTimer m_elapsedTime;
    QString m_sPortName;
    bool m_bIsDriverAvailable;
    bool m_bIsPassengerAvailable;
    FileTransfer *m_trans;
    QString m_sFileName;
    QTimer* m_timerLaunchSCUpdate;
    QTimer* m_timerTracabilityAfterSCUpdate;
public slots:
    void changeprogressBarUpdate();
    void checkTimeUpdate();
    void textChange(QString texteBarre);
    void getTracaDataBefore(QStringList* before);
    void getTracaDataAfter(QStringList* after);
    void ProgressUpdateSC(int value);
    void tracaAfterSCUpdate(int value);
    void updateSVDriver();
    void updateSVPassenger();
    void tracabilityRequestAfterLauncher();
};

#endif // UPDATEMANAGER_H

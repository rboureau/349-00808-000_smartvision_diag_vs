#include "updatemanager.h"
#include "ui_updatemanager.h"

UpdateManager::UpdateManager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UpdateManager)
{
    ui->setupUi(this);
    //Connects the signals to the corresponding slots
    connect(ui->tool_B_openWindowSelectZIP, &QToolButton::clicked,this,&UpdateManager::ButtonResearchZipFile);
    connect(ui->pushButton_Send, &QPushButton::clicked, this, &UpdateManager::ButtonSendZip);
    connect(&m_reader,&UpdateReader::emitString,this,&UpdateManager::updateLogDisplay);
    connect(ui->text_E_log,&QTextEdit::textChanged,this,&UpdateManager::changeprogressBarUpdate);
    connect(ui->text_E_log_2,&QTextEdit::textChanged,this,&UpdateManager::changeprogressBarUpdate);
    connect(ui->line_E_Chemin_Fichier_ZIP,&QLineEdit::textChanged,this,&UpdateManager::textChange);
    connect(ui->progressBar,&QProgressBar::valueChanged,this,&UpdateManager::tracaAfterSCUpdate);
    setWindowModality(Qt::ApplicationModal);
    ui->progress_B_log_2->setStyleSheet("QProgressBar {text-align: center;}");
    ui->progress_B_transfer_2->setStyleSheet("QProgressBar {text-align: center;}");
    ui->progress_B_log->setStyleSheet("QProgressBar {text-align: center;}");
    ui->progress_B_transfer->setStyleSheet("QProgressBar {text-align: center;}");
    m_bIsDriverAvailable=false;
    m_bIsPassengerAvailable=false;
}

UpdateManager::~UpdateManager()
{
    delete ui;
}
void UpdateManager::ButtonResearchZipFile()
{
    //This function retrieves the path to the directory containing Update files.
    QString sDirectoryFirmwareUpgrade =QFileDialog::getExistingDirectory(this); //Opens a dialog to select the directory


    ui->line_E_Chemin_Fichier_ZIP->clear();
    ui->line_E_Chemin_Fichier_ZIP->setText(sDirectoryFirmwareUpgrade); //Put the path in the lineEdit
}
void UpdateManager::SendZip(QString mode)
{
    //Reset the progressBars and Logs in case the user already made an update.
    ui->progress_B_transfer->setValue(0);
    ui->text_E_log->clear();
    ui->progress_B_transfer_2->setValue(0);
    ui->text_E_log_2->clear();
    ui->progressBar->setValue(0);
    //Retrieves the path to the zip file from the path of the directory selected previously
    QString sPathOfDir = ui->line_E_Chemin_Fichier_ZIP->text() +"/LivraisonClient/smartvision.package.zip";
    m_trans = new FileTransfer(this,sPathOfDir,mode);       //Transfer the zip file by ftp to the mode IP address  using the TransfertFichiers class.

    //Connects the transfer state signals to the corresponding slots
    connect(m_trans, &FileTransfer::signalError,this,&UpdateManager::errorColorBarFtp);
    connect(m_trans, &FileTransfer::signalAbort,this,&UpdateManager::errorColorBarFtp);
    connect(m_trans, &FileTransfer::signalFinished,this,&UpdateManager::finishedColorBarFtp);
    qDebug() << "file sent";
    //ui->progress_B_log->setStyleSheet("QProgressBar::chunk {text-align: center;}");
}
void UpdateManager::ButtonSendZip()
{
    if(!QFile(ui->line_E_Chemin_Fichier_ZIP->text() +"/LivraisonClient/smartvision.package.zip").exists()){
        QMessageBox::critical(this,"No update file in the directory","The directory you selected to launch this update contains no update file smartvision.package.zip.");
        return;
    }
    QDirIterator it(ui->line_E_Chemin_Fichier_ZIP->text() +"/LivraisonClient");
    bool bAlreadyVBFFile=false;
    while(it.hasNext()){
        QString sFileName =it.next();
        qDebug() << sFileName;
        if(sFileName.endsWith(".vbf") || sFileName.endsWith(".tar")){
            if(!bAlreadyVBFFile){
                bAlreadyVBFFile=true;
                m_sFileName=sFileName;
            }else{
                QMessageBox::critical(this,"Multiple VBF files in the selected directory","The directory you selected to launch this update contains multiple VBF files, the update won't be performed");
                return;
            }

        }

    }
    if(!bAlreadyVBFFile){
        QMessageBox::critical(this,"No VBF files in the selected directory","The directory you selected to launch this update contains no VBF files. The update couldn't be performed");
        return;
    }



    // Let the user select if he wants to register some traceability data in a dedicated log
    QMessageBox::StandardButton answer=QMessageBox::question(this,"Register Traceability","Do you want to register some traceability information ?");
    //Initialise the traceability data storage variable to their value if the user refuses.
    m_sSerialNumber="XXXXXXX";
    m_sSoftBeforeUpdate="XXXXXXXXXX_XX";
    m_sSoftAfterUpdate="XXXXXXXXXX_XX";
    if(answer==QMessageBox::Yes){
        getLogTraceabilityElements();
    }else if(answer==QMessageBox::No){
        m_sSerialNumber="XXXXXXX";
        m_sSoftBeforeUpdate="XXXXXXXXXX_XX";
    }else{
        qDebug() << "problem";
        ButtonSendZip(); //if the user didn't select yes or no, the process is restarted.
        return;
    }
    //emit signalRefreshSide();
    ui->pushButton_Send->setEnabled(false);
    emit signalDisableMainWindowButtons();
    // Sends the zip file to both sides
    emit signalPingRequest("SVUpdate","both");
    //Creates a loop to control unusually long instructions
    m_loop=new QTimer(this);
    connect(m_loop, &QTimer::timeout,this,&UpdateManager::checkTimeUpdate);
    m_loop->start(1000);
    m_elapsedTime.start();

}
void UpdateManager::updateLogDisplay(QString s, bool mode)
{
    //Manage the messages received in UpdateReader
    if(s.contains("ERROR")){
        QMessageBox::critical(this,"Update Error","An error has been detected");
        errorColorBarLog();

    }
        //using mode to detect the side (Driver/Passenger), it adds the message to the corresponding log view.
            if(mode){
                ui->text_E_log_2->append(s);
            }else{
                ui->text_E_log->append(s);
            }
    m_elapsedTime.restart();   //Restart the count of seconds since last instruction
    //if the update is finished
    if(s.contains("DEBUG: Boot check flag programmed to no (no-boot-check)")){
        //Destroys the timers to stop looking for errors
        m_elapsedTime.invalidate();
        m_elapsedTime.~QElapsedTimer();
        m_loop->stop();
        m_loop->deleteLater();
        qDebug() << "here";
        //Informs the user that the first part is finished
        QApplication::alert(this);
        QMessageBox::StandardButton answer=QMessageBox::question(this, tr("Update Part 1 finished"), tr("The update of the main card is finished. Do you want to proceed to the update of the safety controller ?") );
        if(answer==QMessageBox::No){
            ui->pushButton_Send->setEnabled(true);
            emit signalEnableMainWindowButtons();
            return;
        }else{


        //Now starts the update of the Safety Controller
        //Asks for a reboot of the SmartVision
        emit signalHardReset();
        //Wait 5 seconds for the SmartVision to be ready

        m_timerLaunchSCUpdate= new QTimer(this);
        connect(m_timerLaunchSCUpdate,&QTimer::timeout, [this](){emit signalLaunchUpdateSC(m_sFileName);});
        m_timerLaunchSCUpdate->start(5000);
        //emit signalLaunchUpdateSC(m_sFileName);

        }


    }
}

void UpdateManager::errorColorBarFtp()
{
    //Change the progressBar color to red
    ui->progress_B_transfer->setStyleSheet("QProgressBar::chunk {background-color: #CC0505; text-align: center;}");
    ui->progress_B_transfer->setMaximum(1);
    ui->progress_B_transfer->setValue(1);
    ui->progress_B_transfer_2->setStyleSheet("QProgressBar::chunk {background-color: #CC0505; text-align: center;}");
    ui->progress_B_transfer_2->setMaximum(1);
    ui->progress_B_transfer_2->setValue(1);
}

void UpdateManager::errorColorBarLog()
{
    //Change the progressBar color to red


    if(!ui->text_E_log->toPlainText().isEmpty()){
        ui->progress_B_log->setStyleSheet("QProgressBar::chunk {background-color: #CC0505; text-align: center;}");
    }
    if(!ui->text_E_log_2->toPlainText().isEmpty()){
        ui->progress_B_log_2->setStyleSheet("QProgressBar::chunk {background-color: #CC0505; text-align: center;}");
    }


    //Retrieves the path to the local directory containing the update log
    QSettings settings("Vision Systems", "SmartVision Diag VS");
    QString sFilename=settings.value("cheminRepertoire").toString()+"/JournalMaJ.txt";


    QFile file( sFilename );
    if ( file.open(QIODevice::ReadWrite | QIODevice::Text) )
    {
        //Retrieves the path of the directory selected by the user
        QString sChemin= ui->line_E_Chemin_Fichier_ZIP->text();
        //Retrieves the current date
        QDateTime date=QDateTime::currentDateTime();
        QString sDateString=date.toString();
        file.readAll();
        QTextStream stream( &file );
        stream << sDateString <<"\t"<< sChemin << "\n" << ui->text_E_log->toPlainText() << ui->text_E_log_2->toPlainText()<< endl;

    }
}
void UpdateManager::finishedColorBarFtp()
{
    //Change the progressBar color to green and set the progress to max to indicate the completion of the ftp transfer
    if(m_bIsDriverAvailable){
        ui->progress_B_transfer_2->setMaximum(2);
        ui->progress_B_transfer_2->setValue(2);
    }else if(m_bIsPassengerAvailable){
        ui->progress_B_transfer->setMaximum(2);
        ui->progress_B_transfer->setValue(2);
    }
    m_trans->deleteLater();



}
void UpdateManager::StartValueBarFtp()
{


}
void UpdateManager::changeprogressBarUpdate()
{
    //Count the number of lines in the log to indicate the progress of the update
    ui->progress_B_log->setValue(ui->text_E_log->document()->blockCount());
    ui->progress_B_log_2->setValue(ui->text_E_log_2->document()->blockCount());


}
void UpdateManager::checkTimeUpdate()
{
    if(m_elapsedTime.elapsed()>180000){
        //If no messages were received in the last 3 minutes, there is an error
        QApplication::alert(this);
        QMessageBox::critical(this,"Update Error","There has been 3 minutes since the last instruction. There is probably an error");
        errorColorBarLog(); //Change the color of the progressBar
        //Stop the timer loop
        m_elapsedTime.invalidate();
        m_elapsedTime.~QElapsedTimer();
        m_loop->stop();
        m_loop->deleteLater();
        ui->pushButton_Send->setEnabled(true);
        emit signalEnableMainWindowButtons();

    }if(m_elapsedTime.elapsed()>30000 && ui->progress_B_transfer->value()!=ui->progress_B_transfer->maximum() && ui->progress_B_transfer_2->value()!=ui->progress_B_transfer_2->maximum()){
        //If the transfer didn't happened in the first 30 seconds, we signal it to the user
        QApplication::alert(this);
        QMessageBox::critical(this,"Connexion Error","Failed to connect to the SmartVision. Please check the ethernet connection.");
        errorColorBarFtp(); //Change the color of the progressBar
        //Stop the timer loop
        m_elapsedTime.invalidate();
        m_elapsedTime.~QElapsedTimer();
        m_loop->stop();
        m_loop->deleteLater();
        ui->pushButton_Send->setEnabled(true);
        emit signalEnableMainWindowButtons();
    }
}

void UpdateManager::getLogTraceabilityElements(){
    //Requests the traceability information before the update
    emit signalRequestTracabilityBefore();
}

void UpdateManager::textChange(QString texteBarre)
{
    //The button to send zip files is enabled only if there is some text in the lineEdit
    if(texteBarre!=""){
        ui->pushButton_Send->setEnabled(true);
    }else {
        ui->pushButton_Send->setEnabled(false);

    }
}
void UpdateManager::getTracaDataBefore(QStringList* before){
    qDebug() << "reception traca avant";
    //The first element in the Stringlist is the Serial Number, the second and last is the Software Version
    m_sSerialNumber=before->first();
    m_sSoftBeforeUpdate=before->last();
}
void UpdateManager::getTracaDataAfter(QStringList* after){
    qDebug() << "reception traca après";
    m_sSerialNumber=after->first();
    m_sSoftAfterUpdate=after->last();
    //Retrieves the path to the local directory containing the update log
    QSettings settings("Vision Systems", "SmartVision Diag VS");
    //Retrieves the path of the directory selected by the user
    QString sChemin= ui->line_E_Chemin_Fichier_ZIP->text();
    //Retrieves the current date
    QDateTime date=QDateTime::currentDateTime();
    QString sDateString=date.toString();
    qDebug() << sDateString;
    qDebug() << sChemin;
    QString sFilename=settings.value("cheminRepertoire").toString()+"/JournalMaJ.txt";


    QFile file( sFilename );
    if ( file.open(QIODevice::ReadWrite | QIODevice::Text) )
    {
        file.readAll();
        QTextStream stream( &file );
        stream <<  sDateString << "\t"<< m_sSerialNumber << "\t" << m_sSoftBeforeUpdate<< "\t" << m_sSoftAfterUpdate<< "\t" << sChemin << "\n" << endl;
        //Put it all in the Update log
    }
    //Informs the user that the update is finished
    QApplication::alert(this);
    QMessageBox::information(this, tr("Update SmartVision"), tr("The update is finished") );
    ui->pushButton_Send->setEnabled(true);
    emit signalEnableMainWindowButtons();
}
void UpdateManager::ProgressUpdateSC(int value){
    qDebug() << "Passe";
    //Change the progress bar to indicate the current progress of the Safety Controller Update.
    ui->progressBar->setValue(value);
    m_timerLaunchSCUpdate->stop();

}
void UpdateManager::tracaAfterSCUpdate(int value){
    // When the Safety Controller Update is finished
    if(value==100){

       m_timerTracabilityAfterSCUpdate= new QTimer();
    connect(m_timerTracabilityAfterSCUpdate,&QTimer::timeout,this,&UpdateManager::tracabilityRequestAfterLauncher);
    m_timerTracabilityAfterSCUpdate->start(10000);

    }
}
void UpdateManager::updateSVDriver(){
    SendZip("192.168.69.1");
    //Moves the percentage to the center of the progressBar
    ui->progress_B_log_2->setStyleSheet("QProgressBar {background-color: #05CC05; text-align: center;}");
    ui->progress_B_transfer_2->setStyleSheet("QProgressBar {background-color: #05CC05;text-align: center;}");
    ui->progress_B_transfer_2->setMaximum(0);
    ui->progress_B_transfer_2->setValue(0);
    m_bIsDriverAvailable=true;
}
void UpdateManager::updateSVPassenger(){
    SendZip("192.168.69.2");
    //Moves the percentage to the center of the progressBar
    ui->progress_B_log->setStyleSheet("QProgressBar {background-color: #05CC05; text-align: center;}");
    ui->progress_B_transfer->setStyleSheet("QProgressBar {background-color: #05CC05; text-align: center;}");
    ui->progress_B_transfer->setMaximum(0);
    ui->progress_B_transfer->setValue(0);
    m_bIsPassengerAvailable=true;
}
void UpdateManager::tracabilityRequestAfterLauncher(){
    disconnect(m_timerTracabilityAfterSCUpdate,&QTimer::timeout,this,&UpdateManager::tracabilityRequestAfterLauncher);
    m_timerTracabilityAfterSCUpdate->stop();
    if(m_sSerialNumber=="XXXXXXX"){
        //if the user refused to register the traceability information
        m_sSoftAfterUpdate="XXXXXXXXXX_XX";

    }else{
        emit signalRequestTracabilityAfter();
        //elsewise, it requests the traceability data after the Safety Controller Update
    }
}

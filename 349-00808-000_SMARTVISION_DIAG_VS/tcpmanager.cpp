#include "tcpmanager.h"
#include "ui_tcpmanager.h"

TcpManager::TcpManager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TcpManager)
{
    ui->setupUi(this);
    connect(ui->tool_B_openWindowSelectZIP, &QToolButton::clicked,this,&TcpManager::ButtonResearchZipFile);
    connect(ui->push_B_Send_ZIP, &QPushButton::clicked, this, &TcpManager::ButtonSendZip);
    connect(&reader,&PacketSender::emitString,this,&TcpManager::affichageLogMaJ);
    connect(ui->text_E_log,&QTextEdit::textChanged,this,&TcpManager::changeprogressionBarUpdate);
    ui->progress_B_log->setStyleSheet("QProgressBar {text-align: center;}");
    ui->progress_B_transfer->setStyleSheet("QProgressBar {text-align: center;}");

}

TcpManager::~TcpManager()
{
    delete ui;
}

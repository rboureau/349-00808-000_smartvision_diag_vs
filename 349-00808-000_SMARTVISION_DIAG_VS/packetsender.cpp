﻿#include "packetsender.h"
#include <QHostAddress>
#include <QTextCodec>
PacketSender::PacketSender(QObject *parent) : QObject(parent)
{
    server = new QTcpServer(this);
    nombreConnexion=0;

        // When SmartVision tries to connect, the signal newConnection is emitted
        connect(server, &QTcpServer::newConnection,
                this, &PacketSender::newConnection);

        if(!server->listen(QHostAddress::Any, 51000))                       //listens to the port 51000 which is used by the SmartVision during update
        {
            qDebug() << "Server could not start";
            qDebug() << server->errorString();                              //shows the error in the command prompt
        }
        else
        {

            qDebug() << "Server started!";
        }
    }

void PacketSender::newConnection()
{
        // retrieve the socket that enables communication
        QTcpSocket* sock = server->nextPendingConnection();
        socks.append(sock); //Adds it in the vector containing every connection

        connect(sock, &QTcpSocket::readyRead,this,&PacketSender::readPort);   //The socket has to be ready to start reading the message
        //socket->write("Hello client\r\n");
        //nombreConnexion++;



        //socket->close();
}

void PacketSender::readPort()
{
    qDebug() << socks.size();
    if(!socks.isEmpty()) //Check that the vector isn't empty to avoid fatal error in the for loop
    {
        for (int i=0;i<socks.length();i++){
            QByteArray infobytearray=socks.at(i)->readAll(); //Retrieves all information from the socket
            QString DataAsString = QString::fromStdString(infobytearray.toStdString()); //Convert it to string
            bool mode=true;
            qDebug() << socks.at(i)->peerAddress().toString();
            //Test to find out which side sent the message
            if(socks.at(i)->peerAddress().toString()=="::ffff:192.168.69.1"){
                    mode=true;
                }else if(socks.at(i)->peerAddress().toString()=="::ffff:192.168.69.2") {
                    mode=false;
                }
            if(DataAsString!=""){
                //Sends the message if it isn't empty, with the mode designating which side has sent it.
                emit emitString(DataAsString, mode);
            }

        }
    }
}

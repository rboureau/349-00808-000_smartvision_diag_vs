#ifndef FTPMANAGER_H
#define FTPMANAGER_H

#include <QMainWindow>
#include <QProgressBar>
#include <QNetworkAccessManager>
#include "transfertfichiers.h"
#include "packetsender.h"
#include <QMessageBox>
#include <QTimer>
#include <QElapsedTimer>
#include <QFileDialog>
#include <QSettings>
#include "smartvision.h"
#include <QInputDialog>
#include <QSerialPortInfo>
namespace Ui {
class FtpManager;
}

class FtpManager : public QMainWindow
{
    Q_OBJECT

public:
    explicit FtpManager(QWidget *parent = nullptr);
    ~FtpManager();
signals:
    void emitRequestTracabilityBefore();
    void emitRequestTracabilityAfter();
    void emitHardReset();
    void emitLaunchUpdateSC(QString);
private:
    Ui::FtpManager *ui;


    void ButtonResearchZipFile();
    void SendZip(QString mode);
    void ButtonSendZip();
    void affichageLogMaJ(QString s, bool mode);
    //void affichageLogMaJ(QString s);
    void erreurColorBarFtp();
    void erreurColorBarLog();
    void finishedColorBarFtp();
    void StartValueBarFtp();
    //void getElementsJournalTraceability(QString portName);
    void getElementsJournalTraceability();
    QString numSerie;
    QString SoftAvantMaJ;
    QString SoftApresMaj;
    PacketSender reader;
    QTimer *boucle;
    QElapsedTimer tempsEcoule;
    QString portname;
    SmartVision *sv;
public slots:
    void changeprogressionBarUpdate();
    void checkTimeMaj();
    void changementTexte(QString texteBarre);
    void RecuperationInfoTracaAvant(QStringList* before);
    void RecuperationInfoTracaApres(QStringList* after);
    void ProgressionUpdateSC(int value);
    void LancementTracaApresSCUpdate(int value);
};

#endif // FTPMANAGER_H

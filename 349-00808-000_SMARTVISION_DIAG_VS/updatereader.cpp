#include "updatereader.h"
#include <QHostAddress>
#include <QTextCodec>
UpdateReader::UpdateReader(QObject *parent) : QObject(parent)
{
    m_server = new QTcpServer(this);
    m_numberConnexions=0;

        // When SmartVision tries to connect, the signal newConnection is emitted
        connect(m_server, &QTcpServer::newConnection,
                this, &UpdateReader::newConnection);

        if(!m_server->listen(QHostAddress::Any, 51000))                       //listens to the port 51000 which is used by the SmartVision during update
        {
            qDebug() << "Server could not start";
            qDebug() << m_server->errorString();                              //shows the error in the command prompt
        }
        else
        {

            qDebug() << "Server started!";
        }
}
void UpdateReader::newConnection()
{
        // retrieve the socket that enables communication
        QTcpSocket* sock = m_server->nextPendingConnection();
        m_socks.append(sock); //Adds it in the vector containing every connection

        connect(sock, &QTcpSocket::readyRead,this,&UpdateReader::readPort);   //The socket has to be ready to start reading the message
        //socket->write("Hello client\r\n");
        //nombreConnexion++;



        //socket->close();
}

void UpdateReader::readPort()
{
    if(!m_socks.isEmpty()) //Check that the vector isn't empty to avoid fatal error in the for loop
    {
        for (int i=0;i<m_socks.length();i++){
            QByteArray infobytearray=m_socks.at(i)->readAll(); //Retrieves all information from the socket
            QString sDataAsString = QString::fromStdString(infobytearray.toStdString()); //Convert it to string
            bool mode=true;
            //qDebug() << m_socks.at(i)->peerAddress().toString();
            //Test to find out which side sent the message
            if(m_socks.at(i)->peerAddress().toString()=="::ffff:192.168.69.1"){
                    mode=true;
                }else if(m_socks.at(i)->peerAddress().toString()=="::ffff:192.168.69.2") {
                    mode=false;
                }
            if(sDataAsString!=""){
                //Sends the message if it isn't empty, with the mode designating which side has sent it.
                emit emitString(sDataAsString, mode);
            }

        }
    }
}

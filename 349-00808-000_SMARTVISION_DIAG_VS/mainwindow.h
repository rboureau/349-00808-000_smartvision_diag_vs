#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>



#include "windowwebconfig.h"
#include "diagnosis.h"
#include "updatemanager.h"
#include <QLabel>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
signals:
    void signalLocal(bool);
private:
    void ButtonConnect();
    Ui::MainWindow *ui;
    UpdateManager m_um;
    Diagnosis m_diag;
    QList<QSerialPortInfo> m_portsCOMList;
    QString m_sPortSelected;
    SmartVision *m_svObject;
    bool m_bLocalOrInternet;
    bool m_bConnectionStatus;

public slots:
    void ButtonRefresh();
    void ButtonConfiguration();
    void openUpdateManager();
    void openDiagnostic();

    void DeleteLocalLog(int id, bool error);
    void ConnectAvailable(QString value);
    void Connected();
    void UnConnected();
    void disableButtons();
    void enableButtons();
};
#endif // MAINWINDOW_H

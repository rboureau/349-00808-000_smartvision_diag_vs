#ifndef DIAGNOSIS_H
#define DIAGNOSIS_H

#include <QMainWindow>
#include "traceabilitywindow.h"
#include "smartvision.h"
#include <QtSerialPort/QtSerialPort>
#include "QtSerialPort/qserialportinfo.h"
#include "QtSerialPort/QtSerialPortDepends"
#include <QTableWidget>
#include <QToolButton>
#include <QLayout>
#include <QFileDialog>
namespace Ui {
class Diagnosis;
}

class Diagnosis : public QMainWindow
{
    Q_OBJECT

public:
    explicit Diagnosis(QWidget *parent = nullptr);
    ~Diagnosis();
signals:
    void signalSCUpdatePath(QString);
    void signalUpdateSCRequest();
    void signalChangeSide(bool driver);
    void signalEcuReset();
    void signalCurrentSide(bool driver);
    void signalPingRequest(QString action,QString sides);
private:
    Ui::Diagnosis *ui;

    void ButtonDetails();
    void ButtonResearchVbfTar();
    void ButtonUpdateSafetyController();
    void ButtonECUReset();
    void refreshTraca();
    //void DemandResetECU();
    QTimer timer;
    TraceabilityWindow *m_details;
    QString getMode();

public slots:
    void changeSide();
    void changeProgressBarSC(int iNewProgression);
    void setEnableButtonUpdateSC();
    void changeHMITracabilityValues(QHash<QString,QString> hashTracabilityValues);
    void sendSide();
    void enableSCUpdateButton();
    void changeRadioButton(bool);
    void changeRadioButtonEnabled();
    void launchTracabilityRefresh();
    void launchECUReset();
    void launchSCUpdate();

};

#endif // DIAGNOSIS_H

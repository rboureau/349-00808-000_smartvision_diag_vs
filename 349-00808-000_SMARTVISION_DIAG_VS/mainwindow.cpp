#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QWebSocket>

//using namespace DLL_VisionSystems_SV;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->statusBar->addPermanentWidget(new QLabel(QCoreApplication::applicationVersion()));                          //Adds the version number in the bottom right corner
    connect(ui->pushButton_SendConfigByFTP,&QPushButton::clicked,this, &MainWindow::ButtonConfiguration);
    connect(ui->pushButton_FTP_Manager,&QPushButton::clicked,this,&MainWindow::openUpdateManager);                     // Connects each clicked signal to the corresponding slot
    connect(ui->pushButton_Basic_Diagnostic,&QPushButton::clicked,this,&MainWindow::openDiagnostic);
    connect(ui->push_B_Connection_Final, &QPushButton::clicked, this, &MainWindow::ButtonConnect);
    connect(ui->push_B_Connection_USB_Refresh,&QPushButton::clicked,this,&MainWindow::ButtonRefresh);
    connect(ui->combo_B_Connection_USB,&QComboBox::currentTextChanged,this, &MainWindow::ConnectAvailable);            //When the text of the ComboBox changes, we perform a test
    connect(&m_um,&UpdateManager::signalDisableMainWindowButtons,this,&MainWindow::disableButtons);                    //During update, the other mainwindow buttons are disabled
    connect(&m_um,&UpdateManager::signalEnableMainWindowButtons,this,&MainWindow::enableButtons);

    m_bConnectionStatus=false;

    //The following part puts the update log on the dedicated ftp server. The log contains the date and hour in its name.
    QMessageBox::StandardButton answer=QMessageBox::question(this,"Local Network","Are you on the Vision Systems Local Network ?");
    QSettings settings("Vision Systems", "SmartVision Diag VS");
    QDateTime dateTime=QDateTime::currentDateTime();
    qDebug() << dateTime.toString("ddMMyyyyhhmm");
    QFtp *logSender = new QFtp();                      //Creates a QFtp object
    if(answer==QMessageBox::Yes){
        logSender->connectToHost("ftp-vs");             //Attempt of connection
        m_bLocalOrInternet=true;
        emit signalLocal(true);
    }else if(answer==QMessageBox::No){
        logSender->connectToHost("ftp.vision-systems.fr");             //Attempt of connection
        m_bLocalOrInternet=false;
        emit signalLocal(false);
    }else{
        return;
    }
    logSender->login("rd","n9k5X&");                       //Enters login and password required
    QFile* logFile = new QFile(settings.value("cheminRepertoire").toString()+"/JournalMaJ.txt");
    logFile->open(QIODevice::ReadWrite);
    qDebug() << logFile->size();
    if(logFile->size()!=0){
        logSender->put(logFile->readAll(),"/Production_SmartVision/Journaux MAJ/JournalMaJ"+dateTime.toString("ddMMyyyyhhmm")+".csv"); //read all the text in the update log and puts it in a csv file on the ftp server. The date and time are in the name.
        connect(logSender,&QFtp::commandFinished,this,&MainWindow::DeleteLocalLog); //When the command is finished, the update log is erased
    }
    logFile->close(); //close the Update log
    ButtonRefresh(); //Initial check for COM ports
}

MainWindow::~MainWindow()
{
    delete ui;
}


bool QWebEnginePage::certificateError(const QWebEngineCertificateError &certificateError)
{
    return true;                                                                                                    //To avoid problems of certification
                                                                                                                    //CIO, the partner who has developped the webpages, uses auto-signed certificates
}

void MainWindow::openDiagnostic()
{
    m_diag.show();                                                                                                                    //Shows the basic diagnostic window when it's hidden
}
void MainWindow::openUpdateManager()
{
    m_um.show();                                                                                                     //Shows the ftp manager window when it's hidden
}

void MainWindow::ButtonConfiguration(){
    //Creates a new WindowWebConfig at each click on the button, because it's a process that we need to start to the beginning with default parameters
    WindowWebConfig *webSites = new WindowWebConfig();
    connect(this, &MainWindow::signalLocal,webSites,&WindowWebConfig::changeNetwork);
    emit signalLocal(m_bLocalOrInternet);       //Change the ftp adress according to the question asked at the beginning
    webSites->show();
}

void MainWindow::ButtonConnect()
{

        m_sPortSelected=ui->combo_B_Connection_USB->currentText();                                                                     //Retrieves the portname selected by the user

        m_svObject = new SmartVision(m_sPortSelected);                                                                                   //Starts connexion with this port
        //Connects the signals and slots of svobject
        connect(m_svObject,&SmartVision::signalConnected,this,&MainWindow::Connected);
        connect(m_svObject,&SmartVision::signalNotConnected,this,&MainWindow::UnConnected);
        connect(m_svObject,&SmartVision::signalProgressD,&m_diag,&Diagnosis::changeProgressBarSC);
        connect(m_svObject,&SmartVision::signalProgressUM,&m_um,&UpdateManager::ProgressUpdateSC);
        connect(m_svObject,&SmartVision::signalTracability,&m_diag,&Diagnosis::changeHMITracabilityValues);
        connect(&m_diag,&Diagnosis::signalUpdateSCRequest,m_svObject,&SmartVision::updateTracability);
        connect(&m_diag,&Diagnosis::signalSCUpdatePath,m_svObject,&SmartVision::multiThreadsD);
        connect(&m_um,&UpdateManager::signalLaunchUpdateSC,m_svObject,&SmartVision::multiThreadsUM);
        connect(m_svObject,&SmartVision::signalUpdateTracabilityInformationBefore,&m_um,&UpdateManager::getTracaDataBefore);
        connect(m_svObject,&SmartVision::signalUpdateTracabilityInformationAfter,&m_um,&UpdateManager::getTracaDataAfter);
        connect(&m_um,&UpdateManager::signalRequestTracabilityBefore,m_svObject,&SmartVision::tracabilityUpdateBefore);
        connect(&m_um,&UpdateManager::signalRequestTracabilityAfter,m_svObject,&SmartVision::tracabilityUpdateAfter);
        connect(&m_diag,&Diagnosis::signalChangeSide,m_svObject,&SmartVision::changeSide);
        connect(&m_diag,&Diagnosis::signalEcuReset,m_svObject,&SmartVision::hardReset);
        m_svObject->isConnected(); //Runs the connection test to enable FTP manager and Basic Diagnostic
        connect(&m_um,&UpdateManager::signalPingRequest,m_svObject,&SmartVision::pingManager);
        connect(m_svObject,&SmartVision::signalSVUpdateDriverConnected,&m_um,&UpdateManager::updateSVDriver);
        connect(m_svObject,&SmartVision::signalSVUpdatePassengerConnected,&m_um,&UpdateManager::updateSVPassenger);
        connect(&m_um,&UpdateManager::signalRefreshSide,&m_diag,&Diagnosis::sendSide);
        connect(&m_diag,&Diagnosis::signalCurrentSide,m_svObject,&SmartVision::changeSide);
        connect(m_svObject,&SmartVision::signalEnableSCUpdateButton,&m_diag,&Diagnosis::enableSCUpdateButton);
        connect(&m_diag,&Diagnosis::signalPingRequest,m_svObject,&SmartVision::pingManager);
        connect(m_svObject,&SmartVision::signalBothSides,&m_diag,&Diagnosis::changeRadioButtonEnabled);
        connect(m_svObject,&SmartVision::signalNewSide,&m_diag,&Diagnosis::changeRadioButton);

}
void MainWindow::ButtonRefresh()
{
    ui->combo_B_Connection_USB->clear();                                                                                        //clears all the port previously discovered, in case they are not available anymore
    m_portsCOMList = QSerialPortInfo::availablePorts();                                                                               //gets all available ports
    for(int i=0;i<m_portsCOMList.length();i++)
    {
        if(m_portsCOMList.value(i).portName()!="COM1"){
            ui->combo_B_Connection_USB->addItem(m_portsCOMList.value(i).portName());
        }
                                                            //Adds them to the combo box list
    }

}
void MainWindow::DeleteLocalLog(int id,bool error){
    qDebug() << error;
    if(!error){
        //If the upload of the Update log was successful, we can erase it from the local repository
        QSettings settings("Vision Systems", "SmartVision Diag VS");
        qDebug() << "####" <<QFile("C:/Users/Public/SmartVision_Diag_VS_Files/JournalMaJ.txt").remove();

    }
}
void MainWindow::ConnectAvailable(QString sValue){
    if(sValue!=""){
        //if there are some connections available, it enables the connection button
        if(!m_bConnectionStatus){
        ui->push_B_Connection_Final->setEnabled(true);
        }
    }else{
        ui->push_B_Connection_Final->setEnabled(false);
    }
}
void MainWindow::Connected()
{

    //Once connected, disable the connection button (to avoid errors) and enable Basic Diagnostic and FTP Manager
    ui->pushButton_Basic_Diagnostic->setEnabled(true);
    ui->pushButton_FTP_Manager->setEnabled(true);
    ui->push_B_Connection_Final->setEnabled(false);
    m_bConnectionStatus=true;
}
void MainWindow::UnConnected(){
    QMessageBox::warning(this,"Not Connected","Connection to the SmartVision failed.");
    m_bConnectionStatus=false;
}
void MainWindow::disableButtons(){
    ui->pushButton_Basic_Diagnostic->setEnabled(false);
    ui->pushButton_SendConfigByFTP->setEnabled(false);
}
void MainWindow::enableButtons(){
    ui->pushButton_Basic_Diagnostic->setEnabled(true);
    ui->pushButton_SendConfigByFTP->setEnabled(true);
}

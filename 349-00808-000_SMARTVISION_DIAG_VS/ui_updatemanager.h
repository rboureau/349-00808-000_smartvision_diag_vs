/********************************************************************************
** Form generated from reading UI file 'updatemanager.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPDATEMANAGER_H
#define UI_UPDATEMANAGER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UpdateManager
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QGroupBox *groupBox_UpdateSafetyController;
    QVBoxLayout *verticalLayout_4;
    QProgressBar *progressBar;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_3;
    QProgressBar *progress_B_transfer_2;
    QTextEdit *text_E_log_2;
    QProgressBar *progress_B_log_2;
    QGroupBox *group_B_TCPTransfer;
    QHBoxLayout *horizontalLayout;
    QToolButton *tool_B_openWindowSelectZIP;
    QLineEdit *line_E_Chemin_Fichier_ZIP;
    QGroupBox *groupBox_Buttons;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton_Send;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QProgressBar *progress_B_transfer;
    QTextEdit *text_E_log;
    QProgressBar *progress_B_log;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *UpdateManager)
    {
        if (UpdateManager->objectName().isEmpty())
            UpdateManager->setObjectName(QStringLiteral("UpdateManager"));
        UpdateManager->resize(800, 600);
        centralwidget = new QWidget(UpdateManager);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox_UpdateSafetyController = new QGroupBox(centralwidget);
        groupBox_UpdateSafetyController->setObjectName(QStringLiteral("groupBox_UpdateSafetyController"));
        verticalLayout_4 = new QVBoxLayout(groupBox_UpdateSafetyController);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        progressBar = new QProgressBar(groupBox_UpdateSafetyController);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);

        verticalLayout_4->addWidget(progressBar);


        gridLayout->addWidget(groupBox_UpdateSafetyController, 2, 0, 1, 2);

        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_3 = new QVBoxLayout(groupBox_2);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        progress_B_transfer_2 = new QProgressBar(groupBox_2);
        progress_B_transfer_2->setObjectName(QStringLiteral("progress_B_transfer_2"));
        progress_B_transfer_2->setEnabled(true);
        progress_B_transfer_2->setMaximum(1);
        progress_B_transfer_2->setValue(0);
        progress_B_transfer_2->setTextVisible(true);

        verticalLayout_3->addWidget(progress_B_transfer_2);

        text_E_log_2 = new QTextEdit(groupBox_2);
        text_E_log_2->setObjectName(QStringLiteral("text_E_log_2"));
        text_E_log_2->setEnabled(true);
        text_E_log_2->setReadOnly(true);

        verticalLayout_3->addWidget(text_E_log_2);

        progress_B_log_2 = new QProgressBar(groupBox_2);
        progress_B_log_2->setObjectName(QStringLiteral("progress_B_log_2"));
        progress_B_log_2->setMaximum(104);
        progress_B_log_2->setValue(0);
        progress_B_log_2->setTextVisible(true);
        progress_B_log_2->setInvertedAppearance(false);
        progress_B_log_2->setTextDirection(QProgressBar::TopToBottom);

        verticalLayout_3->addWidget(progress_B_log_2);


        gridLayout->addWidget(groupBox_2, 1, 0, 1, 1);

        group_B_TCPTransfer = new QGroupBox(centralwidget);
        group_B_TCPTransfer->setObjectName(QStringLiteral("group_B_TCPTransfer"));
        group_B_TCPTransfer->setStyleSheet(QStringLiteral("#group_B_TCPTransfer {border:null;}"));
        horizontalLayout = new QHBoxLayout(group_B_TCPTransfer);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        tool_B_openWindowSelectZIP = new QToolButton(group_B_TCPTransfer);
        tool_B_openWindowSelectZIP->setObjectName(QStringLiteral("tool_B_openWindowSelectZIP"));

        horizontalLayout->addWidget(tool_B_openWindowSelectZIP);

        line_E_Chemin_Fichier_ZIP = new QLineEdit(group_B_TCPTransfer);
        line_E_Chemin_Fichier_ZIP->setObjectName(QStringLiteral("line_E_Chemin_Fichier_ZIP"));
        line_E_Chemin_Fichier_ZIP->setReadOnly(true);

        horizontalLayout->addWidget(line_E_Chemin_Fichier_ZIP);

        groupBox_Buttons = new QGroupBox(group_B_TCPTransfer);
        groupBox_Buttons->setObjectName(QStringLiteral("groupBox_Buttons"));
        groupBox_Buttons->setStyleSheet(QStringLiteral("#groupBox_Buttons {border:null;}"));
        verticalLayout_2 = new QVBoxLayout(groupBox_Buttons);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        pushButton_Send = new QPushButton(groupBox_Buttons);
        pushButton_Send->setObjectName(QStringLiteral("pushButton_Send"));
        pushButton_Send->setEnabled(false);

        verticalLayout_2->addWidget(pushButton_Send);


        horizontalLayout->addWidget(groupBox_Buttons);


        gridLayout->addWidget(group_B_TCPTransfer, 0, 0, 1, 2);

        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        progress_B_transfer = new QProgressBar(groupBox);
        progress_B_transfer->setObjectName(QStringLiteral("progress_B_transfer"));
        progress_B_transfer->setEnabled(true);
        progress_B_transfer->setMaximum(1);
        progress_B_transfer->setValue(0);
        progress_B_transfer->setTextVisible(true);

        verticalLayout->addWidget(progress_B_transfer);

        text_E_log = new QTextEdit(groupBox);
        text_E_log->setObjectName(QStringLiteral("text_E_log"));
        text_E_log->setEnabled(true);
        text_E_log->setReadOnly(true);

        verticalLayout->addWidget(text_E_log);

        progress_B_log = new QProgressBar(groupBox);
        progress_B_log->setObjectName(QStringLiteral("progress_B_log"));
        progress_B_log->setMaximum(104);
        progress_B_log->setValue(0);
        progress_B_log->setTextVisible(true);
        progress_B_log->setInvertedAppearance(false);
        progress_B_log->setTextDirection(QProgressBar::TopToBottom);

        verticalLayout->addWidget(progress_B_log);


        gridLayout->addWidget(groupBox, 1, 1, 1, 1);

        UpdateManager->setCentralWidget(centralwidget);
        menubar = new QMenuBar(UpdateManager);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 21));
        UpdateManager->setMenuBar(menubar);
        statusbar = new QStatusBar(UpdateManager);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        UpdateManager->setStatusBar(statusbar);

        retranslateUi(UpdateManager);

        QMetaObject::connectSlotsByName(UpdateManager);
    } // setupUi

    void retranslateUi(QMainWindow *UpdateManager)
    {
        UpdateManager->setWindowTitle(QApplication::translate("UpdateManager", "Update Manager", Q_NULLPTR));
        groupBox_UpdateSafetyController->setTitle(QApplication::translate("UpdateManager", "Update Safety Controller", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("UpdateManager", "Driver", Q_NULLPTR));
        progress_B_transfer_2->setFormat(QApplication::translate("UpdateManager", "%p%", Q_NULLPTR));
        group_B_TCPTransfer->setTitle(QString());
        tool_B_openWindowSelectZIP->setText(QApplication::translate("UpdateManager", "...", Q_NULLPTR));
        groupBox_Buttons->setTitle(QString());
        pushButton_Send->setText(QApplication::translate("UpdateManager", "Send", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("UpdateManager", "Passenger", Q_NULLPTR));
        progress_B_transfer->setFormat(QApplication::translate("UpdateManager", "%p%", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UpdateManager: public Ui_UpdateManager {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPDATEMANAGER_H

#include "filetransfer.h"
#include "QNetworkAccessManager"
#include <QUrl>
#include <QNetworkRequest>
#include <QFile>
#include <QMainWindow>
#include <QNetworkReply>
#include <QAuthenticator>
FileTransfer::FileTransfer(QWidget *parent, QString sFileName,QString sAddress)
{
    // NetworkAccessManager initialisation
    QNetworkAccessManager *manager = new QNetworkAccessManager;
    //The FTP Server will require identification, so we can already connect the IDs to the signal authenticationRequired
    connect(manager, &QNetworkAccessManager::authenticationRequired, this, &FileTransfer::auth);
//    // process to get something from the ftp server
//    QNetworkReply *replyrecup = manager->get(QNetworkRequest(QUrl("ftp://192.168.69.1/test.txt")));
//    //when the finished signal of the QNetworkReply is received, it means the transfer can begin

//    connect(replyrecup, SIGNAL(finished()), this, SLOT(get()));
//    // error management
//    connect(replyrecup, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(erreur(QNetworkReply::NetworkError)));


    QFile transferredFile(sFileName);                                                                                  //fichiertest is the file to transfer
    transferredFile.open(QIODevice::ReadWrite);                                                                         //Opens it in ReadWrite mode
    QString sAddressRequest="ftp://"+sAddress+"/smartvision.package.zip";                                             //Destination path of the file
    QNetworkReply *replyPut = manager->put(QNetworkRequest(QUrl(sAddressRequest)),transferredFile.readAll());            //The whole text contained in fichiertest is transferred to the destination path
    emit signalStart();                                                                                               //A signal is emitted at the beginning of the transfer
    // when the finished signal of the QNetworkReply is received, it means the transfer is finished
    connect(replyPut, &QNetworkReply::finished, this, &FileTransfer::put);
    // error management
    connect(replyPut, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
    connect(replyPut, &QNetworkReply::abort,this,&FileTransfer::aborted);
}
void    FileTransfer::get()
{
    qDebug() << "enregistrer";
    QNetworkReply *r = qobject_cast<QNetworkReply*>(sender()); //It contains the text of the file requested
    // Creates a file to write the text into
    QFile f("testEnregistrement.txt");
    //Opens it in write mode
    if ( f.open(QIODevice::WriteOnly) )
    {
            // Writes all in it
            f.write(r->readAll());
            // Closes file and suppress the reply
            f.close();
            r->deleteLater();
    }
    close();
}
void    FileTransfer::put()
{
    qDebug() << "envoyer";
    QNetworkReply *r = qobject_cast<QNetworkReply*>(sender()); // Contains the result of the transfer
    qDebug() << r->error();                                     // Displays the potential errors
    if(r->error()==QNetworkReply::NoError){                     //if no error has been detected
        emit signalFinished();
    }
    r->deleteLater();                                           // Closes file and suppress the reply
    close();
}

void    FileTransfer::auth(QNetworkReply*rep,QAuthenticator*auth)
{
    qDebug() << "auth";
    auth->setUser("sv-update");                                         //identifier
    auth->setPassword("lk5*Gh=4");                                      //password
}


void    FileTransfer::error(QNetworkReply::NetworkError error)
{
    qDebug() << "error" << error;                  //when an error occurs, it's automatically displayed and a signal is emitted
    emit signalError(error);
}
void    FileTransfer::aborted()
{
    emit signalAbort();                               //when the abortion signal is received, we send another signal that will be used by another class
}

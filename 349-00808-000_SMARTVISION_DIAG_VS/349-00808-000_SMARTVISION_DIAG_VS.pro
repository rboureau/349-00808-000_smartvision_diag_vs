#-------------------------------------------------
#
# Project created by QtCreator 2018-08-28T09:13:16
#
#-------------------------------------------------

QT       += core gui widgets \
        webenginewidgets \
       # core network sql svg webkit xml
        network \
        axcontainer \
        serialport \
    concurrent \
 websockets

TARGET = SmartVision_Diag_VS
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 \
          openssl-linked \
          c++11 console

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    sp_chacom.cpp \
    smartvision.cpp \
    cp_xcom.cpp \
    windowwebconfig.cpp \
    qftp.cpp \
    qurlinfo.cpp \
    dll_visionsystem_sv.cpp \
    diagnosis.cpp \
    updatemanager.cpp \
    traceabilitywindow.cpp \
    updatereader.cpp \
    filetransfer.cpp

HEADERS += \
        mainwindow.h \
    sp_chacom.h \
    smartvision.h \
    cp_xcom.h \
    windowwebconfig.h \
    qftp.h \
    qurlinfo.h \
    dll_visionsystem_sv.h \
    diagnosis.h \
    updatemanager.h \
    traceabilitywindow.h \
    updatereader.h \
    filetransfer.h

FORMS += \
        mainwindow.ui \
    windowwebconfig.ui \
    diagnosis.ui \
    updatemanager.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

RESOURCES += \
    ssl.qrc



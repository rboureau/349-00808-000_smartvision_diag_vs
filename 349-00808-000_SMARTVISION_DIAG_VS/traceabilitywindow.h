#ifndef TRACEABILITYWINDOW_H
#define TRACEABILITYWINDOW_H

#include <QTableWidget>
class TraceabilityWindow : public QWidget
{
    Q_OBJECT
public:
    explicit TraceabilityWindow(QWidget *parent = nullptr);
    ~TraceabilityWindow();
    QTableWidget* m_tracabilityTable;
    void traceability();
private:
    void ButtonQuit();
};

#endif // TRACEABILITYWINDOW_H

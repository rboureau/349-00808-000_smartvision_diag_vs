#include "diagnosis.h"
#include "ui_diagnosis.h"

Diagnosis::Diagnosis(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Diagnosis)
{
    ui->setupUi(this);
    m_details = new TraceabilityWindow(nullptr);  //Creates the detailed traceability to be able to manage its data at any moment
    m_details->hide();                            //Hides this window temporarily
    //connects HMI elements to the corresponding slots
    connect(ui->push_B_traceability_detail,&QPushButton::clicked,this,&Diagnosis::ButtonDetails);
    connect(ui->tool_B_firmware_upgrade_recherche, &QToolButton::clicked,this,&Diagnosis::ButtonResearchVbfTar);
    connect(ui->push_B_Traceability_update, &QPushButton::clicked, this, &Diagnosis::refreshTraca);
    connect(ui->radio_B_Driver, &QRadioButton::clicked,this,&Diagnosis::changeSide);
    connect(ui->radio_B_Passenger, &QRadioButton::clicked,this,&Diagnosis::changeSide);
    connect(ui->push_B_firmware_upgrade_SC,&QPushButton::clicked, this, &Diagnosis::ButtonUpdateSafetyController);
    connect(ui->line_E_firmware_upgrade_chemin,&QLineEdit::textChanged,this, &Diagnosis::setEnableButtonUpdateSC);
    connect(ui->pushButton,&QPushButton::clicked,this,&Diagnosis::ButtonECUReset);
    setWindowModality(Qt::ApplicationModal);  //Makes the window modal
}

Diagnosis::~Diagnosis()
{
    delete ui;
}
void Diagnosis::ButtonDetails()
{

    m_details->show();                    //this window is already created, we just have to make it visible.

}
void Diagnosis::refreshTraca()
{
    emit signalPingRequest("Tracability","both");           //The signal is sent to pingManager in the smartVision class
    connect(&timer,&QTimer::timeout,this,&Diagnosis::launchTracabilityRefresh);
    timer.start(5000);                                      //Waits for the side to be changed

}
void Diagnosis::ButtonResearchVbfTar()
{
    //Function that calls a vbf file selection dialog whenever the corresponding tool button is clicked.
    QString sFileFirmwareUpgrade = QFileDialog::getOpenFileName(this, "Open File", QString(), "Firmware filter (*.vbf *.tar)");          //Retrieves the path to the selected file. VBF and TAR files only

    ui->line_E_firmware_upgrade_chemin->clear();
    ui->line_E_firmware_upgrade_chemin->setText(sFileFirmwareUpgrade);                                                               //Show the path in the corresponding lineEdit
}

QString Diagnosis::getMode()
{
    //Method to retrieve the corresponding IP address of the selected radioButton
    if(ui->radio_B_Driver->isChecked()){
        return "192.168.69.1";
    }else if(ui->radio_B_Passenger->isChecked()) {
        return "192.168.69.2";
    }
    return "192.168.69.1";
}
void Diagnosis::changeSide(){
    //This function is called when the user selects the other radioButton
    // it checks which radioButton is active, and signals the change to the SmartVision object which can change the IP address for the diagnosis.
    if(ui->radio_B_Driver->isChecked()){
            emit signalChangeSide(true);

    }else if(ui->radio_B_Passenger->isChecked()){
            emit signalChangeSide(false);
    }
}
void Diagnosis::ButtonUpdateSafetyController(){

    emit signalPingRequest("SCUpdateDiag","both");   //The signal is sent to pingManager in the smartVision class
    connect(&timer,&QTimer::timeout,this,&Diagnosis::launchSCUpdate);
    timer.start(5000);                               //Waits for the side to be changed
}
void Diagnosis::changeProgressBarSC(int iNewProgression){
    //When the Safety Controller Update progression signal is received, the progressionBar is updated to the new value
    ui->progressBar->setValue(iNewProgression);
    if(iNewProgression!=100){
        ui->push_B_firmware_upgrade_SC->setEnabled(false);  //The push Button isn't available during the update
    }
}
void Diagnosis::setEnableButtonUpdateSC(){
    //this function tests the path in the lineEdit for the Safety Controller Update. if the path is correct, the button to start the update is enabled.
    QString sPathUpdateFile= ui->line_E_firmware_upgrade_chemin->text();
    //The file must exist and be a VBF or TAR File
    if(QFile(sPathUpdateFile).exists() && (sPathUpdateFile.endsWith(".vbf") || sPathUpdateFile.endsWith(".tar"))){
        ui->push_B_firmware_upgrade_SC->setEnabled(true);
    }else{
        ui->push_B_firmware_upgrade_SC->setEnabled(false);
    }
}
void Diagnosis::changeHMITracabilityValues(QHash<QString,QString> hashTracabilityValues){
        //Refresh the HMI after the traceability changed
        //essential traceability data
        ui->label_SystemSoftwareBaselineIDOutput->clear();
        ui->label_SystemSoftwareBaselineIDOutput->setText(hashTracabilityValues.value("FE26"));
        ui->label_ConfigurationFileVersionIDOutput->clear();
        ui->label_ConfigurationFileVersionIDOutput->setText(hashTracabilityValues.value("FE0E"));
        ui->label_SerialNumberOutput->clear();
        ui->label_SerialNumberOutput->setText(hashTracabilityValues.value("F18C"));
        ui->label_ApplicationSoftwareNumberTAGOutput->clear();
        ui->label_ApplicationSoftwareNumberTAGOutput->setText(hashTracabilityValues.value("F195"));
        ui->label_BootloaderSoftwareNumberTAGOutput->clear();
        ui->label_BootloaderSoftwareNumberTAGOutput->setText(hashTracabilityValues.value("F126"));
        ui->label_PackageVersionIDOutput->clear();
        ui->label_PackageVersionIDOutput->setText(hashTracabilityValues.value("FE0C"));
        //detailed traceability data
        for(int i=0;i<m_details->m_tracabilityTable->rowCount();i++){
            if(m_details->m_tracabilityTable->item(i,0)->backgroundColor()!=Qt::lightGray){
                m_details->m_tracabilityTable->setItem(i,2,new QTableWidgetItem(hashTracabilityValues.value(m_details->m_tracabilityTable->item(i,0)->text())));
            }
        }
}
void Diagnosis::sendSide(){
    emit signalCurrentSide(ui->radio_B_Driver->isChecked());    //Sends the side to the SmartVision class
}
void Diagnosis::enableSCUpdateButton(){
    ui->push_B_firmware_upgrade_SC->setEnabled(true);
}
void Diagnosis::ButtonECUReset(){
    emit signalPingRequest("ECUReset","both");                  //The signal is sent to pingManager in the smartVision class
    connect(&timer,&QTimer::timeout,this,&Diagnosis::launchECUReset);
    timer.start(5000);                                          //Waits for the side to be changed
}
void Diagnosis::changeRadioButton(bool driver){
    //Changes the checked radioButton according to the result of the ping. This function is used whenever there is only one side available.
    ui->group_B_Mode->setEnabled(true);
    if(driver){
        ui->radio_B_Driver->setChecked(true);
        ui->radio_B_Passenger->setChecked(false);
    }else{
        ui->radio_B_Driver->setChecked(false);
        ui->radio_B_Passenger->setChecked(true);
    }
    ui->group_B_Mode->setEnabled(false);    //Blocks the radioButtons group, the user can't change side if only one side is available.
}
void Diagnosis::changeRadioButtonEnabled(){
    ui->group_B_Mode->setEnabled(true);     //Both sides have been detected during the ping. The user is allowed to change the side for the diagnostic.
}
void Diagnosis::launchTracabilityRefresh(){
    timer.stop();
    disconnect(&timer,&QTimer::timeout,this,&Diagnosis::launchTracabilityRefresh);
    disconnect(&timer,&QTimer::timeout,this,&Diagnosis::launchSCUpdate);
    disconnect(&timer,&QTimer::timeout,this,&Diagnosis::launchECUReset);
    emit signalUpdateSCRequest();           //All the diagnosis functions including traceability are in the SmartVision object in MainWindow. Signals will be emitted to use these.
}
void Diagnosis::launchECUReset(){
    timer.stop();
    disconnect(&timer,&QTimer::timeout,this,&Diagnosis::launchTracabilityRefresh);
    disconnect(&timer,&QTimer::timeout,this,&Diagnosis::launchSCUpdate);
    disconnect(&timer,&QTimer::timeout,this,&Diagnosis::launchECUReset);
    emit signalEcuReset();
}
void Diagnosis::launchSCUpdate(){
    //Sends the path of the Safety Controller update file to the SmartVision object to perform the update
    timer.stop();
    disconnect(&timer,&QTimer::timeout,this,&Diagnosis::launchTracabilityRefresh);
    disconnect(&timer,&QTimer::timeout,this,&Diagnosis::launchSCUpdate);
    disconnect(&timer,&QTimer::timeout,this,&Diagnosis::launchECUReset);
    ui->push_B_firmware_upgrade_SC->setEnabled(false);
    emit signalSCUpdatePath(ui->line_E_firmware_upgrade_chemin->text());
}

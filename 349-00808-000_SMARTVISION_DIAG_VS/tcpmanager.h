#ifndef TCPMANAGER_H
#define TCPMANAGER_H

#include <QMainWindow>
#include <QProgressBar>
#include <QNetworkAccessManager>
#include "transfertfichiers.h"
#include "packetsender.h"
#include <QMessageBox>
#include <QTimer>
#include <QElapsedTimer>
namespace Ui {
class TcpManager;
}

class TcpManager : public QMainWindow
{
    Q_OBJECT

public:
    explicit TcpManager(QWidget *parent = nullptr);
    ~TcpManager();

private:
    Ui::TcpManager *ui;

    void ButtonResearchZipFile();
    void ButtonSendZip();


    void affichageLogMaJ(QString s);
    void erreurColorBarFtp();
    void erreurColorBarLog();
    void finishedColorBarFtp();
    void StartValueBarFtp();


    PacketSender reader;
    QTimer *boucle;
    QElapsedTimer tempsEcoule;


public slots:
    void changeprogressionBarUpdate();
    void checkTimeMaj();
};

#endif // TCPMANAGER_H

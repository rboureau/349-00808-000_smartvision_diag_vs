#include "smartvision.h"
#include "Windows.h"
#include <stdlib.h>
#include <QHash>
#include <QTextCodec>
#include "dll_visionsystem_sv.h"
#include "cp_xcom.h"
#include <QLatin1Char>
#include <QList>
using namespace CP_Xcom;
using namespace SP_Chacom;
using namespace DLL_VisionSystem_SV;
SmartVision::SmartVision(QString port)                                                       //The constructor contains a QString parameter which is the COM port selected by the user
{
        m_bCurrentSide=true;
        m_bDriver=false;
        m_bPassenger=false;
        connect(this,&SmartVision::signalPingDriverSuccessfull,this,&SmartVision::launchActionDriver);
        connect(this,&SmartVision::signalPingPassengerSuccessfull,this,&SmartVision::launchActionPassenger);
        m_iDriverPingCount=0;
        m_iPassengerPingCount=0;
        //This QHash contains the Traceability IDs as keys and the names as values
        m_HashID.insert("FE26","System Software Baseline ID");
        m_HashID.insert("FE20","VS Part Number");
        m_HashID.insert("FE22","VS Serial Number");
        m_HashID.insert("FE21","VS Manufacturing Date");
        m_HashID.insert("FE23","Screen Serial Number");
        m_HashID.insert("FE24","Top Camera Serial Number");
        m_HashID.insert("FE25","Bot Camera Serial Number");

        m_HashID.insert("FE0E","Configuration File Version ID (SV_CONFIG)");
        m_HashID.insert("FE0F","Driver Remote Configuration File Version MD5 (SV_CONFIG_DRIVER)");
        m_HashID.insert("FE30","Driver Reference Configuration File Version MD5");
        m_HashID.insert("FE14","Passenger Remote Configuration File Version MD5 (SV_CONFIG_PASSENGER");
        m_HashID.insert("FE34","Passenger Reference Configuration File Version MD5");

        m_HashID.insert("FE00","Hardware Number ID");
        m_HashID.insert("FE01","Hardware Version");
        m_HashID.insert("FE02","Serial Number");
        m_HashID.insert("FE0C","Package Version ID(SV_PACKAGE)");
        m_HashID.insert("FE0D","Package Version MD5(SV_PACKAGE)");
        m_HashID.insert("FE0A","Linux Distribution Version ID(SV_DISTRIB)");
        m_HashID.insert("FE0B","Linux Distribution Version MD5(SV_DISTRIB)");
        m_HashID.insert("FE04","Application SmartVision Version ID(SV_FC_APP)");
        m_HashID.insert("FE05","Application SmartVision Version MD5(SV_FC_APP)");
        m_HashID.insert("FE06","Bitstream Zynq Version ID(SV_FC_FPGA)");
        m_HashID.insert("FE07","Bitstream Zynq Version MD5(SV_FC_FPGA)");
        m_HashID.insert("FE08","Bitstream Artix(Safety) Version ID(SV_SM_FPGA)");
        m_HashID.insert("FE09","Bitstream Artix(Safety) Version MD5(SV_SM_FPGA)");
        m_HashID.insert("FE12","Ethernet MAC address");

        m_HashID.insert("F197","System Name");
        m_HashID.insert("F18A","Supplier Name");
        m_HashID.insert("F18C","Serial Number");
        m_HashID.insert("F18B","Manufacturing Date");
        m_HashID.insert("F192","Hardware Number ID");
        m_HashID.insert("F193","Hardware Number Version");
        m_HashID.insert("F195","Application Software Number TAG");
        m_HashID.insert("F126","Bootloader Software Number TAG");

        m_HashID.insert("FE10","Modbus Version ID (Main Board)");
        m_HashID.insert("FE33","Modbus Version ID (Safety Controller)");
        m_HashID.insert("FE11","HSIS Version ID (Main Board)");
        m_HashID.insert("FE31","HSIS Version ID (Safety Controller)");
        m_HashID.insert("FE32","UDS Version ID (Safety Controller)");
        m_HashID.insert("FE36","CAN DB Version (Private CAN)");

        //Connection code given by SeaSideTech
        qDebug()<<"Demo Chadoc3 Smart Vision\n";

        if( SUCCEEDED( CoInitializeEx( 0, COINIT_APARTMENTTHREADED ) ) )
        {

            qDebug() << "CoInit succes\r\n";

            if (m_Ppvs.isNull())
            {
                qDebug() << "error";
            }
            else
            {
              if (m_Ppvs.PP_bCheckProjectNotReadyForUse() == true)
              {
                  qDebug() << "Not Ready\r\n";
              }
              else
              {
                  qDebug() << "Ready\r\n";
              }

              if (m_Ppvs.SetPortSettings(port,"",true) == true)
              {
                  qDebug() << "Configuration OK\r\n";
                  if (m_Ppvs.ConnectToChadoc("", true) == true)
                  {
                      qDebug() << "Connexion Ok\r\n";
                  }
                  else
                  {
                      qDebug() << "Fail to connect\r\n";
                  }

                  m_Ppvs.connectToSmartVision("", true,false);

              }
              else
              {
                  qDebug() << "fail to configure\r\n";
              }

              if (m_Ppvs.PP_bCheckProjectNotReadyForUse() == true)
              {
                  qDebug() << "Not Ready\r\n";
              }
              else
              {
                  qDebug() << "Ready\r\n";
              }

              {
                  //uint reftime = 0;
                  if (m_Ppvs.GetCanUdsGateway() != 0)
                  {
                   if (m_Ppvs.GetCommunicationPoint() != 0)
                     {
                       qDebug() << "is connected ? " << m_Ppvs.GetCommunicationPoint()->XCOM_isConnected() << "\r\n";
                       if(m_Ppvs.GetCommunicationPoint()->XCOM_isConnected()){
                           emit signalConnected();                                //if the connection is valid, it emits this signal to enable buttons in MainWindow
                       }
                    }




                      //test en cours DTCs
//                      SP_UdsGateway_To_SV_DRV *target = new SP_UdsGateway_To_SV_DRV();
//                      QList<qulonglong>* aulDtcArray = new QList<qulonglong>;
//                      uint uDtcAvailabilityMask=0;
//                      target->UDS_SL_bReadDTCInformation_ReportDTCByStatusMask(12,aulDtcArray,uDtcAvailabilityMask);
//                      if(aulDtcArray->length()!=0)
//                      {
//                          qDebug() << aulDtcArray->at(1);
//                      }
                  }

              }
           }
           }
        if (m_Ppvs.GetCommunicationPoint() != 0)
        {
            qDebug() << "is connected ? " << m_Ppvs.GetCommunicationPoint()->XCOM_isConnected() << "\r\n";
        }
}
QString SmartVision::getTracabilityElement(QString key)
{
    emit signalTracabilityElement(m_HashSmartVision.value(key));
    return m_HashSmartVision.value(key);                                      // Returns the element and emits a signal with it
}
QHash<QString,QString> SmartVision::getTracability()
{
    emit signalTracability(m_HashSmartVision);
    return m_HashSmartVision;                                                 // Returns the full traceability and emits a signal with it

}
void SmartVision::updateTracability(){                                      // To update the traceability, the connection is needed
    m_HashSmartVision.clear();                                                // This QHash contains the IDs as keys  and the searched values as values
    for(QString sKey: m_HashID.keys()){
        qDebug() << sKey;
        QByteArray b;                                                       //b will contain the value returned by the smartVision
        bool bOk=false;
        int iValue = sKey.toInt(&bOk,16);                                      //the key is a QString representing the hexadecimal value of the ID
                                                                            //To use the SST function, it has to be converted into an int
        m_Ppvs.GetCanUdsGateway()->UDS_SL_bReadDataByIdentifier_ByteArray(iValue,b);           //SST function that returns the value in b
        QString sDataAsString;                                                               // The bytearray needs to be converted into a String to be used properly
        if(sKey=="FE12" && b!="")                                                                     //The MAC address has to be managed differently because it isn't yet in the good format
        {
            sDataAsString =QString::number(static_cast<quint8>(b.at(0)),16);                 //Transforms the bytearray into quint8, then it is interpreted has an hexadecimal and converted to QString
            for(int i=1;i<b.size();i++){                                                    //The first two numbers of the MAC address are in the initialization of the QString, and the rest is added in a loop
                quint8 u8Number = static_cast<quint8>(b.at(i));
                sDataAsString= sDataAsString +":"+ QString::number(u8Number,16);
            }
        }else if(b==""){
            sDataAsString="Not Found";
            return;
        }else{
            sDataAsString = QString::fromStdString(b.toStdString());                         //All the other values are in the good format, so it is converted directly
        }
        qDebug() << b;
        m_HashSmartVision.insert(sKey,sDataAsString);                                           //The QString is inserted in the QHash

    }
    emit signalTracability(m_HashSmartVision);                                                  //The whole traceability is sent to basic diagnostic to appear in the traceability section
    m_bDriver=false;
    m_bPassenger=false;
}

void SmartVision::updateSC()
{
    //This function has been developped by SeasSideTech and corresponds to the Safety Controller Update
    QByteArray b;
    QByteArray aucSecurityKey_Prog;
     QStringList firmwareName (m_sPath);

    aucSecurityKey_Prog.resize(8);
    aucSecurityKey_Prog[0] = (char)0x33;
    aucSecurityKey_Prog[1] = (char)0x45;
    aucSecurityKey_Prog[2] = (char)0x12;
    aucSecurityKey_Prog[3] = (char)0x19;
    aucSecurityKey_Prog[4] = (char)0xBC;
    aucSecurityKey_Prog[5] = (char)0x00;
    aucSecurityKey_Prog[6] = (char)0xAE;
    aucSecurityKey_Prog[7] = (char)0xAA;

    //ppVs.PP_ShowUdsScreen_To_VisionSystem_SV();
    m_Ppvs.GetCanUdsGateway()->UDS_SL_bReadDataByIdentifier_ByteArray(UDS_eRWDataById_Identfiers_systemSupplierECUSoftwareNumberDataIdentifier,b);
    cout << b.toStdString() << "\r\n";

    // First, open the programming session
    if (m_Ppvs.GetCanUdsGateway()->UDS_SL_bDiagnosticSessionControl((byte)UDS_SessionsIdentifiers_programmingSession, b) == false)
    {
        cout << "Cannot open the UDS programming session ";
    }
    else
    {
        // Need to wait that Safety Controller enters in programming session
        Sleep(uint(1000));

        // Then enter the security access key
        if (m_Ppvs.GetCanUdsGateway()->UDS_SL_bSecurityAccess((byte)UDS_SecuriryAccessTypesIdentifiers_Security_Level_1, aucSecurityKey_Prog) == false)
        {
            cout << "Security access has failed, UDS programming session Level 1";
        }
        else
        {
            cout << "Successfully entered in Programming Session, ready to update the firmware\r\n";



            Sleep(uint(1000));
            if (m_Ppvs.GetCanUdsGateway()->UDS_SL_bSoftwareDownload(firmwareName) == false)
            {
                cout << "Fail to start the firmware download";
            }
            else
            {
                cout << "Download in Done ";
                //stop=true;
            }
            //}
            // Reset Ecu
            {
                uint reftime = 0;
                m_Ppvs.GetCanUdsGateway()->UDS_SL_bECUReset(UDS_ResetTypes_hardReset, false, reftime);
            }
            //Shows the new Version of the Safety Controller software
            QByteArray c;
            QByteArray d;
            m_Ppvs.GetCanUdsGateway()->UDS_SL_bReadDataByIdentifier_ByteArray(61845,c);
            m_Ppvs.GetCanUdsGateway()->UDS_SL_bReadDataByIdentifier_ByteArray(61844,d);
            cout << c.toStdString();
            cout << d.toStdString();
            emit signalEnableSCUpdateButton();
            return;
        }
    }
}
void SmartVision::progressChangeD(){
    //Parallel thread for the progress of the Safety Controller update in Basic Diagnostic
    qDebug() << "thread progressioncganhmeebt:" << this->thread()->currentThreadId();
    while(m_Ppvs.GetCanUdsGateway()->BlockProgress()!=100){
        emit signalProgressD(m_Ppvs.GetCanUdsGateway()->BlockProgress());
        //While it's not finished, the value is continuously sent to Basic Diagnostic
    }
    //Once it's finished, we send it one last time.
    emit signalProgressD(m_Ppvs.GetCanUdsGateway()->BlockProgress());
    qDebug() << "fin process";
    return;
}
void SmartVision::progressChangeUM(){
    //Parallel thread for the progress of the Safety Controller update in FTP manager
    qDebug() << "thread progressioncganhmeebt:" << this->thread()->currentThreadId();
    while(m_Ppvs.GetCanUdsGateway()->BlockProgress()!=100){
        emit signalProgressUM(m_Ppvs.GetCanUdsGateway()->BlockProgress());
        //While it's not finished, the value is continuously sent to Ftp Manager
        qDebug() << "en cours";
    }
    //Once it's finished, we send it one last time.
    emit signalProgressUM(m_Ppvs.GetCanUdsGateway()->BlockProgress());
    qDebug() << "fin process";
    changeSide(m_bCurrentSide);
    return;
}
void SmartVision::multiThreadsD(QString sValue){
    m_sPath=sValue.replace("/","\\"); //The SeaSideTech function only accepts \\ as a separator in the path
    qDebug() << m_sPath;


    QtConcurrent::run(this, &SmartVision::updateSC);     //Starts the  SC update
    this->thread()->currentThread()->sleep(3);      //Wait a few seconds so that the progress value is redefined to 0
    QtConcurrent::run(this, &SmartVision::progressChangeD); //Start to track the progress
}
void SmartVision::multiThreadsUM(QString sValue){
    m_sPath=sValue.replace("/","\\"); //The SeaSideTech function only accepts \\ as a separator in the path
    qDebug() << m_sPath;


    if(m_bDriver){
        changeSide(true);
        updateSCDriver();
    }else if(m_bPassenger){
        changeSide(false);
        updateSCPassenger();
    }
}
void SmartVision::tracabilityUpdateBefore(){
    QStringList* list=tracabilityUpdateData(); //Collect the traceability elements
    emit signalUpdateTracabilityInformationBefore(list); //emit it to FTP Manager
    qDebug() << "envoi traca avant";
}
void SmartVision::tracabilityUpdateAfter(){
    QStringList* list=tracabilityUpdateData();  //Collect the traceability elements
    emit signalUpdateTracabilityInformationAfter(list); //emit it to FTP Manager
    qDebug() << "envoi traca après";
}
QStringList* SmartVision::tracabilityUpdateData(){
    updateTracability();                    //Refresh Traceability data
    QStringList* list =  new QStringList();
    list->append(m_HashSmartVision.value("FE02"));   //Get the element wanted for the update log and put it in a QStringList
    list->append(m_HashSmartVision.value("FE26"));
    return list;
}
void SmartVision::hardReset(){
    uint reftime = 0;
    m_Ppvs.GetCanUdsGateway()->UDS_SL_bECUReset(UDS_ResetTypes_hardReset, false, reftime);        //Resets the SmartVision
    m_bDriver=false;
    m_bPassenger=false;
}
void SmartVision::isConnected(){
    //tests the connection to the smartVision
    if (m_Ppvs.GetCommunicationPoint() != 0)
      {
        qDebug() << "is connected ? " << m_Ppvs.GetCommunicationPoint()->XCOM_isConnected() << "\r\n";
        if(m_Ppvs.GetCommunicationPoint()->XCOM_isConnected()){
            emit signalConnected();
        }else{
            emit signalNotConnected();
        }
     }
}
void SmartVision::changeSide(bool bDriver){
    //Connects to the side specified by the parameter
    if(bDriver){
        m_Ppvs.connectToSmartVision("", true,false);
        qDebug() << "driver";
    }else{
        m_Ppvs.connectToSmartVision("", true,true);
        qDebug() << "passenger";
    }
}
void SmartVision::pingEmitter(QString action,bool driver){
    //Attempt to ping the SmartVision

    QStringList arguments;
    if(driver){
        arguments << "192.168.69.1" <<"-n"<< "1" << "-w" << "500";
        disconnect(&m_ping,&QProcess::readyReadStandardOutput,this,&SmartVision::pongCatcherPassenger);
        disconnect(&m_ping,&QProcess::readyReadStandardOutput,this,&SmartVision::pongCatcherDriver);
        connect(&m_ping,&QProcess::readyReadStandardOutput,this,&SmartVision::pongCatcherDriver);
    }else{
        arguments << "192.168.69.2" <<"-n"<< "1" << "-w" << "500";
        disconnect(&m_ping,&QProcess::readyReadStandardOutput,this,&SmartVision::pongCatcherPassenger);
        disconnect(&m_ping,&QProcess::readyReadStandardOutput,this,&SmartVision::pongCatcherDriver);
        connect(&m_ping,&QProcess::readyReadStandardOutput,this,&SmartVision::pongCatcherPassenger);
    }

    m_ping.start("ping",arguments);

}
void SmartVision::pongCatcherDriver()
{

    QByteArray response = m_ping.readAll();
    QString sResponse=QString::fromStdString(response.toStdString());
    qDebug() << sResponse;
    if(sResponse.contains("octets=32 temps")){
        qDebug() << "réussi";       //Success indicator for the ping
        m_iDriverPingCount++;
    }else{
        qDebug() << "failed";
        emit signalPingDriverNotSuccessfull();
    }
    if(m_iDriverPingCount==1){
        emit signalPingDriverSuccessfull();
        m_bDriver=true;
        m_iDriverPingCount=0;
        disconnect(&m_ping,&QProcess::readyReadStandardOutput,this,&SmartVision::pongCatcherDriver);
    }
}
void SmartVision::pongCatcherPassenger()
{
    QByteArray response = m_ping.readAll();
    QString sResponse=QString::fromStdString(response.toStdString());
    qDebug() << sResponse;
    if(sResponse.contains("octets=32 temps")){
        qDebug() << "réussi";       //Success indicator for the ping
        m_iPassengerPingCount++;
    }else{
        qDebug() << "failed";
        emit signalPingPassengerNotSuccessfull();
    }
    if(m_iPassengerPingCount==1){
        emit signalPingPassengerSuccessfull();
        m_bPassenger=true;
        m_iPassengerPingCount=0;
        disconnect(&m_ping,&QProcess::readyReadStandardOutput,this,&SmartVision::pongCatcherPassenger);
    }
}
void SmartVision::launchActionDriver(){
    qDebug() << "driver";
    if(m_sAction=="SCUpdate"){
        emit signalSCUpdateDriverConnected();
    }else if(m_sAction=="SVUpdate"){
        emit signalSVUpdateDriverConnected();
    }else if(m_sAction=="Tracability"){
        //emit signalTracabilityDriverConnected();
        changeSide(true);
        emit signalNewSide(true);
    }else if(m_sAction=="ECUReset"){
        //emit signalECUResetDriverConnected();
        changeSide(true);
        emit signalNewSide(true);
    }else if(m_sAction=="SCUpdateDiag"){
        //emit signalSCUpdateDiagDriverConnected();
        changeSide(true);
        emit signalNewSide(true);
    }
//    else if(m_sAction=="Diagnostic"){
//        changeSide(true);
//        emit signalNewSide(true);
//  }
}
void SmartVision::launchActionPassenger(){
    qDebug() << "passenger";
    if(m_sAction=="SCUpdate"){
        emit signalSCUpdatePassengerConnected();
    }else if(m_sAction=="SVUpdate"){
        emit signalSVUpdatePassengerConnected();
    }else if(m_sAction=="Tracability"){
        //emit signalTracabilityPassengerConnected();
        if(!m_bDriver){
            changeSide(false);
            emit signalNewSide(false);
        }else{
            emit signalBothSides();
        }
    }else if(m_sAction=="ECUReset"){
        //emit signalECUResetPassengerConnected();
        if(!m_bDriver){
            changeSide(false);
            emit signalNewSide(false);
        }else{
            emit signalBothSides();
        }
    }else if(m_sAction=="SCUpdateDiag"){
        //emit signalSCUpdateDiagPassengerConnected();
        if(!m_bDriver){
            changeSide(false);
            emit signalNewSide(false);
        }else{
            emit signalBothSides();
        }
    }
    //    else if(m_sAction=="Diagnostic"){

    //    }
}
void SmartVision::pingManager(QString action, QString sides){
    m_sAction=action;
    if(sides=="both"){
        pingEmitter(action,true);
        m_timer=new QTimer();
        connect(m_timer,&QTimer::timeout,this,&SmartVision::parallelUpdateLaunch);
        m_timer->start(3000);
        //QtConcurrent::run(this, &SmartVision::pingEmitter,action,false);
    }else if(sides=="driver"){
        pingEmitter(action,true);
    }else if(sides=="passenger"){
        pingEmitter(action,false);
    }
}
void SmartVision::parallelUpdateLaunch(){
    qDebug() << "ici";
    m_timer->stop();
    //m_ping.terminate();
    pingEmitter(m_sAction,false);
}
void SmartVision::updateSCDriver(){
    QtConcurrent::run(this, &SmartVision::updateSC);     //Starts the SC update
    this->thread()->currentThread()->sleep(3);      //Wait a few seconds so that the progress value is redefined to 0
    QtConcurrent::run(this, &SmartVision::progressChangeUM); //Start to track the progress
    //this->thread()->currentThread()->sleep(30);
    if(m_bPassenger){
            changeSide(false);
            updateSCPassenger();
    }else{
        m_bDriver=false;
        m_bPassenger=false;
    }
}
void SmartVision::updateSCPassenger(){
    changeSide(false);
    QtConcurrent::run(this, &SmartVision::updateSC);     //Starts the SC update
    this->thread()->currentThread()->sleep(3);      //Wait a few seconds so that the progress value is redefined to 0
    QtConcurrent::run(this, &SmartVision::progressChangeUM); //Start to track the progress
    m_bDriver=false;
    m_bPassenger=false;

}


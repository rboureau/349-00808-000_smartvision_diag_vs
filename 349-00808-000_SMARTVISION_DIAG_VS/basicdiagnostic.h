#ifndef BASICDIAGNOSTIC_H
#define BASICDIAGNOSTIC_H

#include <QMainWindow>
#include "fenetretracability.h"
#include "smartvision.h"
#include <QtSerialPort/QtSerialPort>
#include "QtSerialPort/qserialportinfo.h"
#include "QtSerialPort/QtSerialPortDepends"
#include <QTableWidget>
#include <QToolButton>
#include <QLayout>
#include <QFileDialog>
#include <update.h>
namespace Ui {
class BasicDiagnostic;
}

class BasicDiagnostic : public QMainWindow
{
    Q_OBJECT

public:
    explicit BasicDiagnostic(QWidget *parent = nullptr);
    ~BasicDiagnostic();
signals:
    void emitCheminUpdateSC(QString);
    void emitUpdateRequest();
    void emitChangeSide(bool driver);
    void emitEcuReset();
private:
    Ui::BasicDiagnostic *ui;

    void ButtonDetails();
    void ButtonResearchVbfTar();
    void ButtonUpdateSafetyController();
    void updateTraca();
    //void DemandResetECU();


    Fenetretracability *details;
    SmartVision *svobject;
    QList<QSerialPortInfo> portscom;
    QString portselected;
    QString getMode();
public slots:
    void changeSide();
    void changeProgressBar(int value);
    void setEnableButtonUpdateSC();
    void changeIHMTracabilityValues(QHash<QString,QString> hashvalues);
};

#endif // BASICDIAGNOSTIC_H

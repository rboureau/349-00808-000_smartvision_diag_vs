#include "ftpmanager.h"
#include "ui_ftpmanager.h"

FtpManager::FtpManager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FtpManager)
{
    ui->setupUi(this);

    //Connects the signals to the corresponding slots
    connect(ui->tool_B_openWindowSelectZIP, &QToolButton::clicked,this,&FtpManager::ButtonResearchZipFile);
    connect(ui->pushButton_Send, &QPushButton::clicked, this, &FtpManager::ButtonSendZip);
    connect(&reader,&PacketSender::emitString,this,&FtpManager::affichageLogMaJ);
    connect(ui->text_E_log,&QTextEdit::textChanged,this,&FtpManager::changeprogressionBarUpdate);
    connect(ui->text_E_log_2,&QTextEdit::textChanged,this,&FtpManager::changeprogressionBarUpdate);
    connect(ui->line_E_Chemin_Fichier_ZIP,&QLineEdit::textChanged,this,&FtpManager::changementTexte);
    connect(ui->progressBar,&QProgressBar::valueChanged,this,&FtpManager::LancementTracaApresSCUpdate);
    //Moves the percentage to the center of the progressBar
    ui->progress_B_log->setStyleSheet("QProgressBar {text-align: center;}");
    ui->progress_B_transfer->setStyleSheet("QProgressBar {text-align: center;}");
    ui->progress_B_log_2->setStyleSheet("QProgressBar {text-align: center;}");
    ui->progress_B_transfer_2->setStyleSheet("QProgressBar {text-align: center;}");
}

FtpManager::~FtpManager()
{
    delete ui;
}
void FtpManager::ButtonResearchZipFile()
{
    //This function retrieves the path to the directory containing Update files.
    QString fileFirmwareUpgrade = QFileDialog::getExistingDirectory(this); //Opens a dialog to select the directory
    ui->line_E_Chemin_Fichier_ZIP->clear();
    ui->line_E_Chemin_Fichier_ZIP->setText(fileFirmwareUpgrade); //Put the path in the lineEdit
}
void FtpManager::SendZip(QString mode)
{
    //Reset the progressBars and Logs in case the user already made an update.
    ui->progress_B_transfer->setValue(0);
    ui->text_E_log->clear();
    ui->progress_B_transfer_2->setValue(0);
    ui->text_E_log_2->clear();

    //Retrieves the path to the zip file from the path of the directory selected previously
    QString pathOfDir = ui->line_E_Chemin_Fichier_ZIP->text() +"/Livraison Client/smartvision.package.zip";
    TransfertFichiers *trans = new TransfertFichiers(this,pathOfDir,mode);       //Transfer the zip file by ftp to the mode IP address  using the TransfertFichiers class.
    //Connects the transfer state signals to the corresponding slots
    connect(trans, &TransfertFichiers::emitStart,this,&FtpManager::StartValueBarFtp);
    connect(trans, &TransfertFichiers::emitError,this,&FtpManager::erreurColorBarFtp);
    connect(trans, &TransfertFichiers::emitAbort,this,&FtpManager::erreurColorBarFtp);
    connect(trans, &TransfertFichiers::emitFinished,this,&FtpManager::finishedColorBarFtp);
    qDebug() << "file sent";
    //ui->progress_B_log->setStyleSheet("QProgressBar::chunk {text-align: center;}");
}
void FtpManager::ButtonSendZip()
{
    if(!QFile(ui->line_E_Chemin_Fichier_ZIP->text() +"/Livraison Client/smartvision.package.zip").exists()){
        QMessageBox::critical(this,"No update file in the directory","The directory you selected to launch this update contains no update file smartvision.package.zip.");
        return;
    }
    QDirIterator it(ui->line_E_Chemin_Fichier_ZIP->text() +"/Livraison Client");
    bool alreadyVBFFile=false;
    while(it.hasNext()){
        QString nomfichier =it.next();
        qDebug() << nomfichier;
        if(nomfichier.endsWith(".vbf") || nomfichier.endsWith(".tar")){
            if(!alreadyVBFFile){
                alreadyVBFFile=true;
            }else{
                QMessageBox::critical(this,"Multiple VBF files in the selected directory","The directory you selected to launch this update contains multiple VBF files, the update won't be performed");
                return;
            }

        }

    }
    if(!alreadyVBFFile){
        QMessageBox::critical(this,"No VBF files in the selected directory","The directory you selected to launch this update contains no VBF files. The update couldn't be performed");
        return;
    }
    // Let the user select if he wants to register some traceability data in a dedicated log
    QMessageBox::StandardButton reponse=QMessageBox::question(this,"Register Traceability","Do you want to register some traceability information ?");
    //Initialise the traceability data storage variable to their value if the user refuses.
    numSerie="XXXXXXX";
    SoftAvantMaJ="XXXXXXXXXX_XX";
    SoftApresMaj="XXXXXXXXXX_XX";
    if(reponse==QMessageBox::Yes){
        getElementsJournalTraceability();
    }else if(reponse==QMessageBox::No){
        numSerie="XXXXXXX";
        SoftAvantMaJ="XXXXXXXXXX_XX";
    }else{
        qDebug() << "problem";
        ButtonSendZip(); //if the user didn't select yes or no, the process is restarted.
        return;
    }
    // Sends the zip file to both sides
    SendZip("192.168.69.1");
    SendZip("192.168.69.2");
    //Creates a loop to control unusually long instructions
    boucle=new QTimer(this);
    connect(boucle, &QTimer::timeout,this,&FtpManager::checkTimeMaj);
    boucle->start(1000);
    tempsEcoule.start();

}
void FtpManager::affichageLogMaJ(QString s, bool mode)
{
        //using mode to detect the side (Driver/Passenger), it adds the message to the corresponding log view.
            if(mode){
                ui->text_E_log_2->append(s);
            }else{
                ui->text_E_log->append(s);
            }
    tempsEcoule.restart();   //Restart the count of seconds since last instruction
    //if the update is finished
    if(s.contains("DEBUG: Boot check flag programmed to no (no-boot-check)")){
        //Destroys the timers to stop looking for errors
        tempsEcoule.invalidate();
        tempsEcoule.~QElapsedTimer();
        boucle->stop();
        boucle->deleteLater();
        qDebug() << "here";
        //Informs the user that the first part is finished
        QApplication::alert(this);
        QMessageBox::information(this, tr("Update Part 1 finished"), tr("The update of the main card is finished. The update of the Safety Controller card will be launched. Press Ok to continue") );


        //Now starts the update of the Safety Controller
        //Asks for a reboot of the SmartVision
        emit emitHardReset();
        //Wait 5 seconds for the SmartVision to be ready
        this->thread()->currentThread()->sleep(5);
        //Looks into the directory to find VBF files and sends its path to update the safety controller
        QDirIterator it(ui->line_E_Chemin_Fichier_ZIP->text() +"/Livraison Client");
        bool alreadyVBFFile=false;
        while(it.hasNext()){
            QString nomfichier =it.next();
            qDebug() << nomfichier;
            if(nomfichier.endsWith(".vbf") || nomfichier.endsWith(".tar")){
                if(!alreadyVBFFile){
                    qDebug() << nomfichier;
                    emit emitLaunchUpdateSC(nomfichier);
                    alreadyVBFFile=true;
                }else{
                    QMessageBox::warning(this,"Multiple VBF files in the selected directory","The directory you selected to launch this update contains multiple VBF files, the update is performed but it may not be with the good VBF file");
                }

            }

        }
        if(!alreadyVBFFile){
            QMessageBox::warning(this,"No VBF files in the selected directory","The directory you selected to launch this update contains no VBF files. The update couldn't be performed");
        }





    }
}

void FtpManager::erreurColorBarFtp()
{
    //Change the progressBar color to red
    ui->progress_B_transfer->setStyleSheet("QProgressBar::chunk {background-color: #CC0505; text-align: center;}");
    ui->progress_B_transfer->setMaximum(1);
    ui->progress_B_transfer->setValue(1);
    ui->progress_B_transfer_2->setStyleSheet("QProgressBar::chunk {background-color: #CC0505; text-align: center;}");
    ui->progress_B_transfer_2->setMaximum(1);
    ui->progress_B_transfer_2->setValue(1);
}

void FtpManager::erreurColorBarLog()
{
    //Change the progressBar color to red


    if(!ui->text_E_log->toPlainText().isEmpty()){
        ui->progress_B_log->setStyleSheet("QProgressBar::chunk {background-color: #CC0505; text-align: center;}");
    }
    if(!ui->text_E_log_2->toPlainText().isEmpty()){
        ui->progress_B_log_2->setStyleSheet("QProgressBar::chunk {background-color: #CC0505; text-align: center;}");
    }


    //Retrieves the path to the local directory containing the update log
    QSettings settings("Vision Systems", "SmartVision Diag VS");
    QString filename=settings.value("cheminRepertoire").toString()+"/JournalMaJ.txt";


    QFile file( filename );
    if ( file.open(QIODevice::ReadWrite | QIODevice::Text) )
    {
        //Retrieves the path of the directory selected by the user
        QString chemin= ui->line_E_Chemin_Fichier_ZIP->text();
        //Retrieves the current date
        QDateTime date=QDateTime::currentDateTime();
        QString dateString=date.toString();
        file.readAll();
        QTextStream stream( &file );
        stream << dateString <<"\t"<< chemin << "\n" << ui->text_E_log->toPlainText() << ui->text_E_log_2->toPlainText()<< endl;

    }
}
void FtpManager::finishedColorBarFtp()
{
    //Change the progressBar color to green and set the progress to max to indicate the completion of the ftp transfer
    ui->progress_B_transfer->setMaximum(2);
    ui->progress_B_transfer->setValue(2);
    ui->progress_B_transfer_2->setMaximum(2);
    ui->progress_B_transfer_2->setValue(2);
}
void FtpManager::StartValueBarFtp()
{
    ui->progress_B_transfer->setMaximum(0);
    ui->progress_B_transfer->setValue(0);
    ui->progress_B_transfer_2->setMaximum(0);
    ui->progress_B_transfer_2->setValue(0);
}
void FtpManager::changeprogressionBarUpdate()
{
    //Count the number of lines in the log to indicate the progress of the update
    ui->progress_B_log->setValue(ui->text_E_log->document()->blockCount());
    ui->progress_B_log_2->setValue(ui->text_E_log_2->document()->blockCount());


}
void FtpManager::checkTimeMaj()
{
    qDebug() << "en boucle";
    if(tempsEcoule.elapsed()>180000){
        //If no messages were received in the last 3 minutes, there is an error
        QApplication::alert(this);
        QMessageBox::critical(this,"Update Error","There has been 3 minutes since the last instruction. There is probably an error");
        erreurColorBarLog(); //Change the color of the progressBar
        //Stop the timer loop
        tempsEcoule.invalidate();
        tempsEcoule.~QElapsedTimer();
        boucle->stop();
        boucle->deleteLater();

    }if(tempsEcoule.elapsed()>30000 && ui->progress_B_transfer->value()!=ui->progress_B_transfer->maximum() && ui->progress_B_transfer_2->value()!=ui->progress_B_transfer_2->maximum()){
        //If the transfer didn't happened in the first 30 seconds, we signal it to the user
        QApplication::alert(this);
        QMessageBox::critical(this,"Connexion Error","Failed to connect to the ftp server. Please check the ethernet connection.");
        erreurColorBarFtp(); //Change the color of the progressBar
        //Stop the timer loop
        tempsEcoule.invalidate();
        tempsEcoule.~QElapsedTimer();
        boucle->stop();
        boucle->deleteLater();
    }
}

void FtpManager::getElementsJournalTraceability(){
    //Requests the traceability information before the update
    emit emitRequestTracabilityBefore();
}

void FtpManager::changementTexte(QString texteBarre)
{
    //The button to send zip files is enabled only if there is some text in the lineEdit
    if(texteBarre!=""){
        ui->pushButton_Send->setEnabled(true);
    }else {
        ui->pushButton_Send->setEnabled(false);

    }
}
void FtpManager::RecuperationInfoTracaAvant(QStringList* before){
    qDebug() << "reception traca avant";
    //The first element in the Stringlist is the Serial Number, the second and last is the Software Version
    numSerie=before->first();
    SoftAvantMaJ=before->last();
}
void FtpManager::RecuperationInfoTracaApres(QStringList* after){
    qDebug() << "reception traca après";
    numSerie=after->first();
    SoftApresMaj=after->last();
    //Retrieves the path to the local directory containing the update log
    QSettings settings("Vision Systems", "SmartVision Diag VS");
    //Retrieves the path of the directory selected by the user
    QString chemin= ui->line_E_Chemin_Fichier_ZIP->text();
    //Retrieves the current date
    QDateTime date=QDateTime::currentDateTime();
    QString dateString=date.toString();
    qDebug() << dateString;
    qDebug() << chemin;
    QString filename=settings.value("cheminRepertoire").toString()+"/JournalMaJ.txt";


    QFile file( filename );
    if ( file.open(QIODevice::ReadWrite | QIODevice::Text) )
    {
        file.readAll();
        QTextStream stream( &file );
        stream <<  dateString << "\t"<< numSerie << "\t" << SoftAvantMaJ<< "\t" << SoftApresMaj<< "\t" << chemin << "\n" << endl;
        //Put it all in the Update log
    }
    //Informs the user that the update is finished
    QMessageBox::information(this, tr("Update SmartVision"), tr("The update is finished") );
}
void FtpManager::ProgressionUpdateSC(int value){
    qDebug() << "Passe";
    //Change the progress bar to indicate the current progress of the Safety Controller Update.
    ui->progressBar->setValue(value);
}
void FtpManager::LancementTracaApresSCUpdate(int value){
    // When the Safety Controller Update is finished
    if(value==100){
    this->thread()->currentThread()->sleep(10);

    if(numSerie=="XXXXXXX"){
        //if the user refused to register the traceability information
        SoftApresMaj="XXXXXXXXXX_XX";

    }else{
        emit emitRequestTracabilityAfter();
        //elsewise, it requests the traceability data after the Safety Controller Update
    }
    }
}

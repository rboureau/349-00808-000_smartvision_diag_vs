#include "tcpsocket.h"
#include "QTextEdit"
tcpsocket::tcpsocket(QObject *parent) : QObject(parent)
{

}
void tcpsocket::doConnect()
{
    socket = new QTcpSocket(this);

    connect(socket, SIGNAL(connected()),this,SLOT(connected()));
    connect(socket, SIGNAL(disconnected()),this,SLOT(disconnected()));
    connect(socket, SIGNAL(bytesWritten(qint64)),this,SLOT(bytesWritten(qint64)));
    connect(socket, SIGNAL(readyRead()),this,SLOT(readyRead()));
    qDebug() << "connecting...";
    //this is not blocking call
    socket->connectToHost("192.168.69.1", 80);
    //we need to wait...
    if(!socket->waitForConnected(5000))
    {
        qDebug() << "Error: " << socket->errorString();
    }
}

void tcpsocket::connected()
{
     qDebug() << "connected...";

        // Hey server, tell me about you.
        socket->write("HEAD / HTTP/1.0\r\n\r\n\r\n\r\n");
}
void tcpsocket::disconnected()
{
    qDebug() << "disconnected...";
}

void tcpsocket::bytesWritten(qint64 bytes)
{
    qDebug() << QString::number(bytes) << " bytes written...";
}
void tcpsocket::readyRead()
{
    qDebug() <<"reading...";

    // read the data from the socket
   qDebug() << socket->readAll();
}

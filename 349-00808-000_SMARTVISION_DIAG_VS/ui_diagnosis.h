/********************************************************************************
** Form generated from reading UI file 'diagnosis.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIAGNOSIS_H
#define UI_DIAGNOSIS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Diagnosis
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QGroupBox *group_B_Mode;
    QHBoxLayout *horizontalLayout_2;
    QRadioButton *radio_B_Driver;
    QRadioButton *radio_B_Passenger;
    QGroupBox *group_B_traceability;
    QGridLayout *gridLayout_7;
    QGroupBox *group_B_Basic_Information;
    QGridLayout *gridLayout_6;
    QLabel *label_SystemSoftwareBaselineIDStatic;
    QLabel *label_SystemSoftwareBaselineIDOutput;
    QLabel *label_ConfigurationFileVersionIDStatic;
    QLabel *label_ConfigurationFileVersionIDOutput;
    QLabel *label_SerialNumberStatic;
    QLabel *label_SerialNumberOutput;
    QLabel *label_ApplicationSoftwareNumberTAGStatic;
    QLabel *label_ApplicationSoftwareNumberTAGOutput;
    QLabel *label_BootloaderSoftwareNumberTAGStatic;
    QLabel *label_BootloaderSoftwareNumberTAGOutput;
    QLabel *label_PackageVersionIDStatic;
    QLabel *label_PackageVersionIDOutput;
    QPushButton *push_B_traceability_detail;
    QPushButton *push_B_Traceability_update;
    QPushButton *pushButton;
    QGroupBox *group_B_Input_Output_Backlight_Order;
    QGridLayout *gridLayout_3;
    QGroupBox *group_B_Input_Output_Backlight_Order_Buttons;
    QVBoxLayout *verticalLayout_2;
    QRadioButton *radio_B_Input_Output_Backlight_Order_Buttons_Software_Current_Value;
    QRadioButton *radio_B_Input_Output_Backlight_Order_Buttons_Freeze;
    QRadioButton *radio_B_Input_Output_Backlight_Order_Buttons_Manual_Setting;
    QComboBox *comboBox;
    QGroupBox *group_B_Input_Output_Backlight_Order_Output;
    QLineEdit *line_E_Input_Output_Backlight_Order_Output_Display;
    QGroupBox *group_B_DTC_Information;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *group_B_DTC_Information_Button;
    QHBoxLayout *horizontalLayout;
    QPushButton *push_B_DTC_Information_Button_Read;
    QPushButton *push_B_DTC_Information_Button_Clear;
    QTextEdit *text_E_DTC_Information_Log;
    QGroupBox *group_B_firmware_upgrade;
    QGridLayout *gridLayout_2;
    QLineEdit *line_E_firmware_upgrade_chemin;
    QPushButton *push_B_firmware_upgrade_SC;
    QToolButton *tool_B_firmware_upgrade_recherche;
    QProgressBar *progressBar;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *Diagnosis)
    {
        if (Diagnosis->objectName().isEmpty())
            Diagnosis->setObjectName(QStringLiteral("Diagnosis"));
        Diagnosis->resize(796, 681);
        centralwidget = new QWidget(Diagnosis);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        group_B_Mode = new QGroupBox(centralwidget);
        group_B_Mode->setObjectName(QStringLiteral("group_B_Mode"));
        horizontalLayout_2 = new QHBoxLayout(group_B_Mode);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        radio_B_Driver = new QRadioButton(group_B_Mode);
        radio_B_Driver->setObjectName(QStringLiteral("radio_B_Driver"));
        QFont font;
        font.setPointSize(11);
        radio_B_Driver->setFont(font);
        radio_B_Driver->setChecked(true);

        horizontalLayout_2->addWidget(radio_B_Driver);

        radio_B_Passenger = new QRadioButton(group_B_Mode);
        radio_B_Passenger->setObjectName(QStringLiteral("radio_B_Passenger"));
        radio_B_Passenger->setFont(font);

        horizontalLayout_2->addWidget(radio_B_Passenger);


        gridLayout->addWidget(group_B_Mode, 0, 0, 1, 1);

        group_B_traceability = new QGroupBox(centralwidget);
        group_B_traceability->setObjectName(QStringLiteral("group_B_traceability"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(group_B_traceability->sizePolicy().hasHeightForWidth());
        group_B_traceability->setSizePolicy(sizePolicy);
        group_B_traceability->setStyleSheet(QStringLiteral(""));
        gridLayout_7 = new QGridLayout(group_B_traceability);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        group_B_Basic_Information = new QGroupBox(group_B_traceability);
        group_B_Basic_Information->setObjectName(QStringLiteral("group_B_Basic_Information"));
        group_B_Basic_Information->setMinimumSize(QSize(304, 177));
        group_B_Basic_Information->setStyleSheet(QStringLiteral("#group_B_Basic_Information {border:null;}"));
        gridLayout_6 = new QGridLayout(group_B_Basic_Information);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        label_SystemSoftwareBaselineIDStatic = new QLabel(group_B_Basic_Information);
        label_SystemSoftwareBaselineIDStatic->setObjectName(QStringLiteral("label_SystemSoftwareBaselineIDStatic"));

        gridLayout_6->addWidget(label_SystemSoftwareBaselineIDStatic, 0, 0, 1, 1);

        label_SystemSoftwareBaselineIDOutput = new QLabel(group_B_Basic_Information);
        label_SystemSoftwareBaselineIDOutput->setObjectName(QStringLiteral("label_SystemSoftwareBaselineIDOutput"));

        gridLayout_6->addWidget(label_SystemSoftwareBaselineIDOutput, 0, 1, 1, 1);

        label_ConfigurationFileVersionIDStatic = new QLabel(group_B_Basic_Information);
        label_ConfigurationFileVersionIDStatic->setObjectName(QStringLiteral("label_ConfigurationFileVersionIDStatic"));

        gridLayout_6->addWidget(label_ConfigurationFileVersionIDStatic, 1, 0, 1, 1);

        label_ConfigurationFileVersionIDOutput = new QLabel(group_B_Basic_Information);
        label_ConfigurationFileVersionIDOutput->setObjectName(QStringLiteral("label_ConfigurationFileVersionIDOutput"));

        gridLayout_6->addWidget(label_ConfigurationFileVersionIDOutput, 1, 1, 1, 1);

        label_SerialNumberStatic = new QLabel(group_B_Basic_Information);
        label_SerialNumberStatic->setObjectName(QStringLiteral("label_SerialNumberStatic"));

        gridLayout_6->addWidget(label_SerialNumberStatic, 2, 0, 1, 1);

        label_SerialNumberOutput = new QLabel(group_B_Basic_Information);
        label_SerialNumberOutput->setObjectName(QStringLiteral("label_SerialNumberOutput"));

        gridLayout_6->addWidget(label_SerialNumberOutput, 2, 1, 1, 1);

        label_ApplicationSoftwareNumberTAGStatic = new QLabel(group_B_Basic_Information);
        label_ApplicationSoftwareNumberTAGStatic->setObjectName(QStringLiteral("label_ApplicationSoftwareNumberTAGStatic"));

        gridLayout_6->addWidget(label_ApplicationSoftwareNumberTAGStatic, 3, 0, 1, 1);

        label_ApplicationSoftwareNumberTAGOutput = new QLabel(group_B_Basic_Information);
        label_ApplicationSoftwareNumberTAGOutput->setObjectName(QStringLiteral("label_ApplicationSoftwareNumberTAGOutput"));

        gridLayout_6->addWidget(label_ApplicationSoftwareNumberTAGOutput, 3, 1, 1, 1);

        label_BootloaderSoftwareNumberTAGStatic = new QLabel(group_B_Basic_Information);
        label_BootloaderSoftwareNumberTAGStatic->setObjectName(QStringLiteral("label_BootloaderSoftwareNumberTAGStatic"));

        gridLayout_6->addWidget(label_BootloaderSoftwareNumberTAGStatic, 4, 0, 1, 1);

        label_BootloaderSoftwareNumberTAGOutput = new QLabel(group_B_Basic_Information);
        label_BootloaderSoftwareNumberTAGOutput->setObjectName(QStringLiteral("label_BootloaderSoftwareNumberTAGOutput"));

        gridLayout_6->addWidget(label_BootloaderSoftwareNumberTAGOutput, 4, 1, 1, 1);

        label_PackageVersionIDStatic = new QLabel(group_B_Basic_Information);
        label_PackageVersionIDStatic->setObjectName(QStringLiteral("label_PackageVersionIDStatic"));

        gridLayout_6->addWidget(label_PackageVersionIDStatic, 5, 0, 1, 1);

        label_PackageVersionIDOutput = new QLabel(group_B_Basic_Information);
        label_PackageVersionIDOutput->setObjectName(QStringLiteral("label_PackageVersionIDOutput"));

        gridLayout_6->addWidget(label_PackageVersionIDOutput, 5, 1, 1, 1);


        gridLayout_7->addWidget(group_B_Basic_Information, 0, 0, 1, 2);

        push_B_traceability_detail = new QPushButton(group_B_traceability);
        push_B_traceability_detail->setObjectName(QStringLiteral("push_B_traceability_detail"));

        gridLayout_7->addWidget(push_B_traceability_detail, 1, 0, 1, 1);

        push_B_Traceability_update = new QPushButton(group_B_traceability);
        push_B_Traceability_update->setObjectName(QStringLiteral("push_B_Traceability_update"));

        gridLayout_7->addWidget(push_B_Traceability_update, 1, 1, 1, 1);


        gridLayout->addWidget(group_B_traceability, 0, 1, 3, 1);

        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout->addWidget(pushButton, 1, 0, 1, 1);

        group_B_Input_Output_Backlight_Order = new QGroupBox(centralwidget);
        group_B_Input_Output_Backlight_Order->setObjectName(QStringLiteral("group_B_Input_Output_Backlight_Order"));
        group_B_Input_Output_Backlight_Order->setEnabled(false);
        group_B_Input_Output_Backlight_Order->setStyleSheet(QStringLiteral(""));
        gridLayout_3 = new QGridLayout(group_B_Input_Output_Backlight_Order);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        group_B_Input_Output_Backlight_Order_Buttons = new QGroupBox(group_B_Input_Output_Backlight_Order);
        group_B_Input_Output_Backlight_Order_Buttons->setObjectName(QStringLiteral("group_B_Input_Output_Backlight_Order_Buttons"));
        group_B_Input_Output_Backlight_Order_Buttons->setStyleSheet(QStringLiteral("border:null"));
        verticalLayout_2 = new QVBoxLayout(group_B_Input_Output_Backlight_Order_Buttons);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        radio_B_Input_Output_Backlight_Order_Buttons_Software_Current_Value = new QRadioButton(group_B_Input_Output_Backlight_Order_Buttons);
        radio_B_Input_Output_Backlight_Order_Buttons_Software_Current_Value->setObjectName(QStringLiteral("radio_B_Input_Output_Backlight_Order_Buttons_Software_Current_Value"));
        radio_B_Input_Output_Backlight_Order_Buttons_Software_Current_Value->setLayoutDirection(Qt::RightToLeft);
        radio_B_Input_Output_Backlight_Order_Buttons_Software_Current_Value->setChecked(true);

        verticalLayout_2->addWidget(radio_B_Input_Output_Backlight_Order_Buttons_Software_Current_Value);

        radio_B_Input_Output_Backlight_Order_Buttons_Freeze = new QRadioButton(group_B_Input_Output_Backlight_Order_Buttons);
        radio_B_Input_Output_Backlight_Order_Buttons_Freeze->setObjectName(QStringLiteral("radio_B_Input_Output_Backlight_Order_Buttons_Freeze"));
        radio_B_Input_Output_Backlight_Order_Buttons_Freeze->setLayoutDirection(Qt::RightToLeft);

        verticalLayout_2->addWidget(radio_B_Input_Output_Backlight_Order_Buttons_Freeze);

        radio_B_Input_Output_Backlight_Order_Buttons_Manual_Setting = new QRadioButton(group_B_Input_Output_Backlight_Order_Buttons);
        radio_B_Input_Output_Backlight_Order_Buttons_Manual_Setting->setObjectName(QStringLiteral("radio_B_Input_Output_Backlight_Order_Buttons_Manual_Setting"));
        radio_B_Input_Output_Backlight_Order_Buttons_Manual_Setting->setLayoutDirection(Qt::RightToLeft);

        verticalLayout_2->addWidget(radio_B_Input_Output_Backlight_Order_Buttons_Manual_Setting);


        gridLayout_3->addWidget(group_B_Input_Output_Backlight_Order_Buttons, 0, 0, 2, 2);

        comboBox = new QComboBox(group_B_Input_Output_Backlight_Order);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        gridLayout_3->addWidget(comboBox, 2, 1, 1, 1);

        group_B_Input_Output_Backlight_Order_Output = new QGroupBox(group_B_Input_Output_Backlight_Order);
        group_B_Input_Output_Backlight_Order_Output->setObjectName(QStringLiteral("group_B_Input_Output_Backlight_Order_Output"));
        group_B_Input_Output_Backlight_Order_Output->setStyleSheet(QStringLiteral("border:null"));
        line_E_Input_Output_Backlight_Order_Output_Display = new QLineEdit(group_B_Input_Output_Backlight_Order_Output);
        line_E_Input_Output_Backlight_Order_Output_Display->setObjectName(QStringLiteral("line_E_Input_Output_Backlight_Order_Output_Display"));
        line_E_Input_Output_Backlight_Order_Output_Display->setEnabled(false);
        line_E_Input_Output_Backlight_Order_Output_Display->setGeometry(QRect(10, 23, 61, 21));
        line_E_Input_Output_Backlight_Order_Output_Display->setStyleSheet(QStringLiteral("border: 1px solid black"));
        line_E_Input_Output_Backlight_Order_Output_Display->setReadOnly(true);

        gridLayout_3->addWidget(group_B_Input_Output_Backlight_Order_Output, 0, 2, 2, 1);


        gridLayout->addWidget(group_B_Input_Output_Backlight_Order, 2, 0, 1, 1);

        group_B_DTC_Information = new QGroupBox(centralwidget);
        group_B_DTC_Information->setObjectName(QStringLiteral("group_B_DTC_Information"));
        group_B_DTC_Information->setEnabled(false);
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(group_B_DTC_Information->sizePolicy().hasHeightForWidth());
        group_B_DTC_Information->setSizePolicy(sizePolicy1);
        group_B_DTC_Information->setMinimumSize(QSize(0, 0));
        group_B_DTC_Information->setStyleSheet(QStringLiteral(""));
        verticalLayout_3 = new QVBoxLayout(group_B_DTC_Information);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        group_B_DTC_Information_Button = new QGroupBox(group_B_DTC_Information);
        group_B_DTC_Information_Button->setObjectName(QStringLiteral("group_B_DTC_Information_Button"));
        group_B_DTC_Information_Button->setStyleSheet(QStringLiteral("#group_B_DTC_Information_Button {border:null;}"));
        horizontalLayout = new QHBoxLayout(group_B_DTC_Information_Button);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        push_B_DTC_Information_Button_Read = new QPushButton(group_B_DTC_Information_Button);
        push_B_DTC_Information_Button_Read->setObjectName(QStringLiteral("push_B_DTC_Information_Button_Read"));
        push_B_DTC_Information_Button_Read->setEnabled(false);

        horizontalLayout->addWidget(push_B_DTC_Information_Button_Read);

        push_B_DTC_Information_Button_Clear = new QPushButton(group_B_DTC_Information_Button);
        push_B_DTC_Information_Button_Clear->setObjectName(QStringLiteral("push_B_DTC_Information_Button_Clear"));
        push_B_DTC_Information_Button_Clear->setEnabled(false);

        horizontalLayout->addWidget(push_B_DTC_Information_Button_Clear);


        verticalLayout_3->addWidget(group_B_DTC_Information_Button);

        text_E_DTC_Information_Log = new QTextEdit(group_B_DTC_Information);
        text_E_DTC_Information_Log->setObjectName(QStringLiteral("text_E_DTC_Information_Log"));
        text_E_DTC_Information_Log->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        text_E_DTC_Information_Log->setReadOnly(true);

        verticalLayout_3->addWidget(text_E_DTC_Information_Log);


        gridLayout->addWidget(group_B_DTC_Information, 4, 0, 1, 2);

        group_B_firmware_upgrade = new QGroupBox(centralwidget);
        group_B_firmware_upgrade->setObjectName(QStringLiteral("group_B_firmware_upgrade"));
        group_B_firmware_upgrade->setStyleSheet(QStringLiteral(""));
        gridLayout_2 = new QGridLayout(group_B_firmware_upgrade);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        line_E_firmware_upgrade_chemin = new QLineEdit(group_B_firmware_upgrade);
        line_E_firmware_upgrade_chemin->setObjectName(QStringLiteral("line_E_firmware_upgrade_chemin"));
        line_E_firmware_upgrade_chemin->setReadOnly(true);

        gridLayout_2->addWidget(line_E_firmware_upgrade_chemin, 0, 1, 1, 1);

        push_B_firmware_upgrade_SC = new QPushButton(group_B_firmware_upgrade);
        push_B_firmware_upgrade_SC->setObjectName(QStringLiteral("push_B_firmware_upgrade_SC"));
        push_B_firmware_upgrade_SC->setEnabled(false);

        gridLayout_2->addWidget(push_B_firmware_upgrade_SC, 0, 2, 1, 1);

        tool_B_firmware_upgrade_recherche = new QToolButton(group_B_firmware_upgrade);
        tool_B_firmware_upgrade_recherche->setObjectName(QStringLiteral("tool_B_firmware_upgrade_recherche"));

        gridLayout_2->addWidget(tool_B_firmware_upgrade_recherche, 0, 0, 1, 1);

        progressBar = new QProgressBar(group_B_firmware_upgrade);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);

        gridLayout_2->addWidget(progressBar, 1, 0, 1, 3);


        gridLayout->addWidget(group_B_firmware_upgrade, 3, 0, 1, 2);

        Diagnosis->setCentralWidget(centralwidget);
        menubar = new QMenuBar(Diagnosis);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 796, 21));
        Diagnosis->setMenuBar(menubar);
        statusbar = new QStatusBar(Diagnosis);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        Diagnosis->setStatusBar(statusbar);

        retranslateUi(Diagnosis);

        QMetaObject::connectSlotsByName(Diagnosis);
    } // setupUi

    void retranslateUi(QMainWindow *Diagnosis)
    {
        Diagnosis->setWindowTitle(QApplication::translate("Diagnosis", "Diagnostic", Q_NULLPTR));
        group_B_Mode->setTitle(QApplication::translate("Diagnosis", "Mode", Q_NULLPTR));
        radio_B_Driver->setText(QApplication::translate("Diagnosis", "Driver", Q_NULLPTR));
        radio_B_Passenger->setText(QApplication::translate("Diagnosis", "Passenger", Q_NULLPTR));
        group_B_traceability->setTitle(QApplication::translate("Diagnosis", "TraceAbility tools", Q_NULLPTR));
        group_B_Basic_Information->setTitle(QString());
        label_SystemSoftwareBaselineIDStatic->setText(QApplication::translate("Diagnosis", "System Software Baseline ID        :", Q_NULLPTR));
        label_SystemSoftwareBaselineIDOutput->setText(QApplication::translate("Diagnosis", "Not Found", Q_NULLPTR));
        label_ConfigurationFileVersionIDStatic->setText(QApplication::translate("Diagnosis", "Configuration File Version ID        :", Q_NULLPTR));
        label_ConfigurationFileVersionIDOutput->setText(QApplication::translate("Diagnosis", "Not Found", Q_NULLPTR));
        label_SerialNumberStatic->setText(QApplication::translate("Diagnosis", "Serial Number                                :", Q_NULLPTR));
        label_SerialNumberOutput->setText(QApplication::translate("Diagnosis", "Not Found", Q_NULLPTR));
        label_ApplicationSoftwareNumberTAGStatic->setText(QApplication::translate("Diagnosis", "Application Software Number TAG:", Q_NULLPTR));
        label_ApplicationSoftwareNumberTAGOutput->setText(QApplication::translate("Diagnosis", "Not Found", Q_NULLPTR));
        label_BootloaderSoftwareNumberTAGStatic->setText(QApplication::translate("Diagnosis", "Bootloader Software Number TAG:", Q_NULLPTR));
        label_BootloaderSoftwareNumberTAGOutput->setText(QApplication::translate("Diagnosis", "Not Found", Q_NULLPTR));
        label_PackageVersionIDStatic->setText(QApplication::translate("Diagnosis", "Package Version ID                       :", Q_NULLPTR));
        label_PackageVersionIDOutput->setText(QApplication::translate("Diagnosis", "Not Found", Q_NULLPTR));
        push_B_traceability_detail->setText(QApplication::translate("Diagnosis", "Show Details", Q_NULLPTR));
        push_B_Traceability_update->setText(QApplication::translate("Diagnosis", "Update", Q_NULLPTR));
        pushButton->setText(QApplication::translate("Diagnosis", "ECU Reset", Q_NULLPTR));
        group_B_Input_Output_Backlight_Order->setTitle(QApplication::translate("Diagnosis", "Backlight Order", Q_NULLPTR));
        group_B_Input_Output_Backlight_Order_Buttons->setTitle(QString());
        radio_B_Input_Output_Backlight_Order_Buttons_Software_Current_Value->setText(QApplication::translate("Diagnosis", "Software Current Value", Q_NULLPTR));
        radio_B_Input_Output_Backlight_Order_Buttons_Freeze->setText(QApplication::translate("Diagnosis", "Freeze", Q_NULLPTR));
        radio_B_Input_Output_Backlight_Order_Buttons_Manual_Setting->setText(QApplication::translate("Diagnosis", "Manual setting", Q_NULLPTR));
        group_B_Input_Output_Backlight_Order_Output->setTitle(QApplication::translate("Diagnosis", "Current Output", Q_NULLPTR));
        group_B_DTC_Information->setTitle(QApplication::translate("Diagnosis", "DTC Information", Q_NULLPTR));
        group_B_DTC_Information_Button->setTitle(QString());
        push_B_DTC_Information_Button_Read->setText(QApplication::translate("Diagnosis", "Read", Q_NULLPTR));
        push_B_DTC_Information_Button_Clear->setText(QApplication::translate("Diagnosis", "Clear", Q_NULLPTR));
        group_B_firmware_upgrade->setTitle(QApplication::translate("Diagnosis", "Firmware Upgrade", Q_NULLPTR));
        push_B_firmware_upgrade_SC->setText(QApplication::translate("Diagnosis", "Update SC", Q_NULLPTR));
        tool_B_firmware_upgrade_recherche->setText(QApplication::translate("Diagnosis", "...", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Diagnosis: public Ui_Diagnosis {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIAGNOSIS_H

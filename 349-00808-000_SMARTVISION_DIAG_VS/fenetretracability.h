#ifndef FENETRETRACABILITY_H
#define FENETRETRACABILITY_H

#include <QMainWindow>
#include <QTableWidget>

namespace Ui {
class Fenetretracability;
}

class Fenetretracability : public QWidget
{
    Q_OBJECT

public:
    explicit Fenetretracability(QWidget *parent = nullptr);
    ~Fenetretracability();
    QTableWidget* visibility;
    void traceability();

private:
    Ui::Fenetretracability *ui;
    void ButtonQuit();
};

#endif // FENETRETRACABILITY_H

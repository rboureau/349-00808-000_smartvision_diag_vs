#ifndef WINDOWWEBCONFIG_H
#define WINDOWWEBCONFIG_H

#include <QMainWindow>
#include <QWebEnginePage>
#include <QProgressBar>
#include <QWebEngineDownloadItem>
#include <QWebEngineProfile>
#include <QFileDialog>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QAuthenticator>
#include "qftp.h"
#include "qurlinfo.h"
#include <QMessageBox>
#include <QSettings>
namespace Ui {
class WindowWebConfig;
}

class WindowWebConfig : public QMainWindow
{
    Q_OBJECT

public:
    explicit WindowWebConfig(QWidget *parent = nullptr);
    ~WindowWebConfig();

private:
    Ui::WindowWebConfig *ui;
    QWebEnginePage *m_pageConfigDriver;
    QProgressBar *m_chargementPageWebDriver;
    QWebEnginePage *m_pageConfigPassenger;
    QProgressBar *m_chargementPageWebPassenger;
    QWebEngineProfile *m_driverProf;
    QWebEngineProfile *m_passengerProf;
    QString m_sXMLPath;
    QString m_sCommentPath;
    QString m_sDirectoryPath;
    QString m_sFTPReference;
    QString m_sOriginalNamePassenger;
    QString m_sOriginalNameDriver;
    QString m_sRelativePathXML;
    QString m_sRelativePathComment;
    QString m_sReferenceValue;
    QString m_sLocalOrInternet;
    void changeFTPAdress();
    //QString max;
    int m_iMax;
    int m_iRetenue;
    bool m_bEmptyFile;
    //QString nameNewFolder(QString indice);
    QString nameNewFolder(int indice);
    void testNames();
    int m_iNumberAttemptsSendFiles;
    bool m_bWarningNameTooLong;
    bool m_bWarningForbiddenCharacters;
    int m_iNumberSuccess;
public slots:
    void setTab();
    void webBarLoadDriver(int progress);
    void webBarLoadPassenger(int progress);
    void downloadRequestedDriver(QWebEngineDownloadItem* download);
    void downloadRequestedPassenger(QWebEngineDownloadItem* download);
    void ReferenceNextButton();
    void CommentNextButton();
    void SendFilesButton();
    void CancelButton();
    void RefreshWebPageDriver();
    void RefreshWebPagePassenger();
    void auth(QNetworkReply*rep,QAuthenticator*auth);
    void progress(qint64 progress, qint64 total);
    void error(QNetworkReply::NetworkError error);
    void sent();
    void FTPServerFileNamesTreatment(QUrlInfo filename);
    //void nombreVersionRécupéré(QFtp* ftp);
    void changeNetwork(bool local);

};

#endif // WINDOWWEBCONFIG_H

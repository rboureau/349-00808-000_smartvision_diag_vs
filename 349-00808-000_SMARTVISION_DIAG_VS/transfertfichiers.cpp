#include "transfertfichiers.h"
#include "QNetworkAccessManager"
#include <QUrl>
#include <QNetworkRequest>
#include <QFile>
#include <QMainWindow>
#include <QNetworkReply>
#include <QAuthenticator>
TransfertFichiers::TransfertFichiers(QWidget *parent, QString nomfichier,QString adresse)   //nomFichier is the name of the file to send, adresse is the destination IP address.
{

    // NetworkAccessManager initialisation
    QNetworkAccessManager *manager = new QNetworkAccessManager;
    //The FTP Server will require identification, so we can already connect the IDs to the signal authenticationRequired
    connect(manager, &QNetworkAccessManager::authenticationRequired, this, &TransfertFichiers::auth);
//    // process to get something from the ftp server
//    QNetworkReply *replyrecup = manager->get(QNetworkRequest(QUrl("ftp://192.168.69.1/test.txt")));
//    //when the finished signal of the QNetworkReply is received, it means the transfer can begin

//    connect(replyrecup, SIGNAL(finished()), this, SLOT(recuperer()));
//    // error management
//    connect(replyrecup, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(erreur(QNetworkReply::NetworkError)));


    QFile fichiertest(nomfichier);                                                                                  //fichiertest is the file to transfer
    fichiertest.open(QIODevice::ReadWrite);                                                                         //Opens it in ReadWrite mode
    QString adresseRequete="ftp://"+adresse+"/smartvision.package.zip";                                             //Destination path of the file
    QNetworkReply *replyenv = manager->put(QNetworkRequest(QUrl(adresseRequete)),fichiertest.readAll());            //The whole text contained in fichiertest is transferred to the destination path
    emit emitStart();                                                                                               //A signal is emitted at the beginning of the transfer
    // when the finished signal of the QNetworkReply is received, it means the transfer is finished
    connect(replyenv, &QNetworkReply::finished, this, &TransfertFichiers::envoyer);
    // error management
    connect(replyenv, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(erreur(QNetworkReply::NetworkError)));
    connect(replyenv, &QNetworkReply::abort,this,&TransfertFichiers::aborted);
}
void    TransfertFichiers::recuperer()
{
    qDebug() << "enregistrer";
    QNetworkReply *r = qobject_cast<QNetworkReply*>(sender()); //It contains the text of the file requested
    // Creates a file to write the text into
    QFile f("testEnregistrement.txt");
    //Opens it in write mode
    if ( f.open(QIODevice::WriteOnly) )
    {
            // Writes all in it
            f.write(r->readAll());
            // Closes file and suppress the reply
            f.close();
            r->deleteLater();
    }
    close();
}
void    TransfertFichiers::envoyer()
{
    qDebug() << "envoyer";
    QNetworkReply *r = qobject_cast<QNetworkReply*>(sender()); // Contains the result of the transfer
    qDebug() << r->error();                                     // Displays the potential errors
    if(r->error()==QNetworkReply::NoError){                     //if no error has been detected
        emit emitFinished();
    }
    r->deleteLater();                                           // Closes file and suppress the reply
    close();
}

void    TransfertFichiers::auth(QNetworkReply*rep,QAuthenticator*auth)
{
    qDebug() << "auth";
    auth->setUser("sv-update");                                         //identifier
    auth->setPassword("lk5*Gh=4");                                      //password
}


void    TransfertFichiers::erreur(QNetworkReply::NetworkError erreur)
{
    qDebug() << "error" << erreur;                  //when an error occurs, it's automatically displayed and a signal is emitted
    emit emitError(erreur);
}
void    TransfertFichiers::aborted()
{
    emit emitAbort();                               //when the abortion signal is received, we send another signal that will be used by another class
}

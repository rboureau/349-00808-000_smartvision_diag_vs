#ifndef TRANSFERTFICHIERS_H
#define TRANSFERTFICHIERS_H

#include <QMainWindow>
#include "QNetworkAccessManager"
#include <QUrl>
#include <QNetworkRequest>
#include <QFile>
#include <QNetworkReply>
class TransfertFichiers : public QMainWindow
{
    Q_OBJECT
public:
    explicit TransfertFichiers(QWidget *parent, QString nomfichier, QString adresse);

signals:
    void emitError(QNetworkReply::NetworkError);
    void emitFinished();
    void emitStart();
    void emitAbort();
public slots:
    void recuperer();
    void envoyer();
    void auth(QNetworkReply*rep,QAuthenticator*auth);
    void erreur(QNetworkReply::NetworkError erreur);
    void aborted();
};

#endif // TRANSFERTFICHIERS_H

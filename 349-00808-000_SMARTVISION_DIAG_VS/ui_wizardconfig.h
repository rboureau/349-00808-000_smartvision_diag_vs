/********************************************************************************
** Form generated from reading UI file 'wizardconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIZARDCONFIG_H
#define UI_WIZARDCONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWizard>
#include <QtWidgets/QWizardPage>

QT_BEGIN_NAMESPACE

class Ui_WizardConfig
{
public:
    QWizardPage *wizardRef;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLabel *label_3;
    QWizardPage *wizardPage;
    QLabel *label_4;
    QWizardPage *wizardPage_2;
    QGridLayout *gridLayout;
    QLabel *label_5;
    QGroupBox *groupBox_2;
    QWizardPage *wizardPage_3;
    QLabel *label_6;
    QWizardPage *wizardPage_4;
    QLabel *label_7;
    QGroupBox *groupBox_3;
    QWizardPage *wizardPage_5;
    QTextEdit *textEdit;
    QLabel *label_8;
    QWizardPage *wizardPage2;
    QLabel *label_9;
    QLabel *label_10;

    void setupUi(QWizard *WizardConfig)
    {
        if (WizardConfig->objectName().isEmpty())
            WizardConfig->setObjectName(QStringLiteral("WizardConfig"));
        WizardConfig->resize(845, 495);
        wizardRef = new QWizardPage();
        wizardRef->setObjectName(QStringLiteral("wizardRef"));
        label = new QLabel(wizardRef);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(90, 20, 151, 31));
        label_2 = new QLabel(wizardRef);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(90, 80, 31, 31));
        lineEdit = new QLineEdit(wizardRef);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(120, 80, 71, 31));
        label_3 = new QLabel(wizardRef);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(200, 80, 81, 31));
        WizardConfig->addPage(wizardRef);
        wizardPage = new QWizardPage();
        wizardPage->setObjectName(QStringLiteral("wizardPage"));
        label_4 = new QLabel(wizardPage);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(70, 50, 201, 41));
        WizardConfig->addPage(wizardPage);
        wizardPage_2 = new QWizardPage();
        wizardPage_2->setObjectName(QStringLiteral("wizardPage_2"));
        gridLayout = new QGridLayout(wizardPage_2);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_5 = new QLabel(wizardPage_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(wizardPage_2);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));

        gridLayout->addWidget(groupBox_2, 1, 0, 1, 1);

        WizardConfig->addPage(wizardPage_2);
        wizardPage_3 = new QWizardPage();
        wizardPage_3->setObjectName(QStringLiteral("wizardPage_3"));
        label_6 = new QLabel(wizardPage_3);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(9, 9, 238, 16));
        WizardConfig->addPage(wizardPage_3);
        wizardPage_4 = new QWizardPage();
        wizardPage_4->setObjectName(QStringLiteral("wizardPage_4"));
        label_7 = new QLabel(wizardPage_4);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(70, 10, 50, 13));
        groupBox_3 = new QGroupBox(wizardPage_4);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(110, 40, 471, 191));
        WizardConfig->addPage(wizardPage_4);
        wizardPage_5 = new QWizardPage();
        wizardPage_5->setObjectName(QStringLiteral("wizardPage_5"));
        textEdit = new QTextEdit(wizardPage_5);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(50, 20, 261, 91));
        label_8 = new QLabel(wizardPage_5);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(50, 0, 91, 21));
        WizardConfig->addPage(wizardPage_5);
        wizardPage2 = new QWizardPage();
        wizardPage2->setObjectName(QStringLiteral("wizardPage2"));
        label_9 = new QLabel(wizardPage2);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(50, 30, 161, 31));
        label_10 = new QLabel(wizardPage2);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(50, 60, 291, 31));
        WizardConfig->addPage(wizardPage2);

        retranslateUi(WizardConfig);

        QMetaObject::connectSlotsByName(WizardConfig);
    } // setupUi

    void retranslateUi(QWizard *WizardConfig)
    {
        WizardConfig->setWindowTitle(QApplication::translate("WizardConfig", "Wizard", Q_NULLPTR));
        label->setText(QApplication::translate("WizardConfig", "Enter the reference number :", Q_NULLPTR));
        label_2->setText(QApplication::translate("WizardConfig", "057-", Q_NULLPTR));
        label_3->setText(QApplication::translate("WizardConfig", "-527", Q_NULLPTR));
        label_4->setText(QApplication::translate("WizardConfig", "Please connect to driver-side SmartVision", Q_NULLPTR));
        label_5->setText(QApplication::translate("WizardConfig", "Driver", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("WizardConfig", "GroupBox", Q_NULLPTR));
        label_6->setText(QApplication::translate("WizardConfig", "Please connect to the passenger-side SmartVision", Q_NULLPTR));
        label_7->setText(QApplication::translate("WizardConfig", "Passenger", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("WizardConfig", "GroupBox", Q_NULLPTR));
        label_8->setText(QApplication::translate("WizardConfig", "Comment :", Q_NULLPTR));
        label_9->setText(QApplication::translate("WizardConfig", "Are you ready to send the files ?", Q_NULLPTR));
        label_10->setText(QApplication::translate("WizardConfig", "Please make sure everything is fine before sending the file", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class WizardConfig: public Ui_WizardConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIZARDCONFIG_H

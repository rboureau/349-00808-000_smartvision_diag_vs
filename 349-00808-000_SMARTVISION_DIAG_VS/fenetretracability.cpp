#include "fenetretracability.h"
#include <QWidget>
#include <QTableWidget>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QPushButton>
#include <QAbstractItemView>
#include <QCoreApplication>
#include <QDebug>
#include <QUuid>
#include <QTextCodec>

Fenetretracability::Fenetretracability(QWidget *parent) :
    QWidget(parent)

{

    //Initialisation de la fenêtre
    //Window Initialisation
    resize(500,676);                                                                                //Gives the window a suitable size
    visibility = new QTableWidget(this);                                                            //Creates the tableWidget which will contain the traceability data
    QHeaderView *vert_header=visibility->verticalHeader();                                          //Retrieve the rows headers
    vert_header->hide();                                                                            //Hide them
    visibility->setColumnCount(3);                                                                  //There is currently 3 columns and 45 rows in the traceability table
    visibility->setRowCount(45);
    visibility->resize(302,625);                                                                    //Resizes the table to its optimal size

    visibility->setHorizontalHeaderItem(0, new QTableWidgetItem("DID"));                            //Names the columns
    visibility->setHorizontalHeaderItem(1, new QTableWidgetItem("Name"));
    visibility->setHorizontalHeaderItem(2, new QTableWidgetItem("Value"));

                                                                   //Tentative de changement des tailles des colonnes. La case de nom doit être plus grande
                                                                    // que les autres car elles contient souvent beaucoup plus de texte. Ces lignes ne
                                                                  //fonctionnent pas avec l'interface responsive active.

    QPushButton *boutonquitter = new QPushButton(this);                                             // Creates the button to hide this window
    boutonquitter->setText("Close");                                                                // Changes the text of the button
    boutonquitter->move(100,630);                                                                   // Moves it under the table
    QVBoxLayout *mainLayout = new QVBoxLayout;                                                      // Creates a vertical layout
    mainLayout->addWidget(visibility);                                                              // Adds the table to the layout
    mainLayout->addWidget(boutonquitter);                                                           // And the button
    this->setLayout(mainLayout);                                                                    // Sets the window layout to the created one
    visibility->setEditTriggers(QAbstractItemView::NoEditTriggers);                                 // The table can't be edited by the user
    visibility->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);                     // the width of the columns is adjusted to the size of the window
    vert_header->setSectionResizeMode(QHeaderView::Stretch);
    connect(boutonquitter, &QPushButton::clicked,this,&Fenetretracability::ButtonQuit);             // Connects the button to the hide slot


    //Fill the table with identifiers and names of rows

    visibility->setItem(0,0, new QTableWidgetItem("Smart Vision"));                                 //Title of the first category, the line is grayed out
    visibility->item(0,0)->setBackgroundColor(Qt::lightGray);
    visibility->setItem(0,1, new QTableWidgetItem(""));
    visibility->item(0,1)->setBackgroundColor(Qt::lightGray);
    visibility->setItem(0,2, new QTableWidgetItem(""));
    visibility->item(0,2)->setBackgroundColor(Qt::lightGray);


    //Identifiers and names of the first category
    visibility->setItem(1,0, new QTableWidgetItem("FE26"));
    visibility->setItem(1,1, new QTableWidgetItem("System Software Baseline ID"));
    visibility->setItem(2,0, new QTableWidgetItem("FE20"));
    visibility->setItem(2,1, new QTableWidgetItem("VS Part Number"));
    visibility->setItem(3,0, new QTableWidgetItem("FE22"));
    visibility->setItem(3,1, new QTableWidgetItem("VS Serial Number"));
    visibility->setItem(4,0, new QTableWidgetItem("FE21"));
    visibility->setItem(4,1, new QTableWidgetItem("VS Manufacturing Date"));
    visibility->setItem(5,0, new QTableWidgetItem("FE23"));
    visibility->setItem(5,1, new QTableWidgetItem("Screen Serial Number"));
    visibility->setItem(6,0, new QTableWidgetItem("FE24"));
    visibility->setItem(6,1, new QTableWidgetItem("Top Camera Serial Number"));
    visibility->setItem(7,0, new QTableWidgetItem("FE25"));
    visibility->setItem(7,1, new QTableWidgetItem("Bot Camera Serial Number"));

    visibility->setItem(8,0, new QTableWidgetItem("Config"));                                       //Title of the second category, the line is grayed out
    visibility->item(8,0)->setBackgroundColor(Qt::lightGray);
    visibility->setItem(8,1, new QTableWidgetItem(""));
    visibility->item(8,1)->setBackgroundColor(Qt::lightGray);
    visibility->setItem(8,2, new QTableWidgetItem(""));
    visibility->item(8,2)->setBackgroundColor(Qt::lightGray);

    //Identifiers and names of the second category
    visibility->setItem(9,0, new QTableWidgetItem("FE0E"));
    visibility->setItem(9,1, new QTableWidgetItem("Configuration File Version ID (SV_CONFIG)"));
    visibility->setItem(10,0, new QTableWidgetItem("FE0F"));
    visibility->setItem(10,1, new QTableWidgetItem("Driver Remote Configuration File Version MD5 (SV_CONFIG_DRIVER)"));
    visibility->setItem(11,0, new QTableWidgetItem("FE30"));
    visibility->setItem(11,1, new QTableWidgetItem("Driver Reference Configuration File Version MD5"));
    visibility->setItem(12,0, new QTableWidgetItem("FE14"));
    visibility->setItem(12,1, new QTableWidgetItem("Passenger Remote Configuration File Version MD5 (SV_CONFIG_PASSENGER)"));
    visibility->setItem(13,0, new QTableWidgetItem("FE34"));
    visibility->setItem(13,1, new QTableWidgetItem("Passenger Reference Configuration File Version MD5"));

    visibility->setItem(14,0, new QTableWidgetItem("MainBoard"));                                   //Title of the third category, the line is grayed out
    visibility->item(14,0)->setBackgroundColor(Qt::lightGray);
    visibility->setItem(14,1, new QTableWidgetItem(""));
    visibility->item(14,1)->setBackgroundColor(Qt::lightGray);
    visibility->setItem(14,2, new QTableWidgetItem(""));
    visibility->item(14,2)->setBackgroundColor(Qt::lightGray);

    //Identifiers and names of the third category
    visibility->setItem(15,0, new QTableWidgetItem("FE00"));
    visibility->setItem(15,1, new QTableWidgetItem("Hardware Number ID"));
    visibility->setItem(16,0, new QTableWidgetItem("FE01"));
    visibility->setItem(16,1, new QTableWidgetItem("Hardware Version"));
    visibility->setItem(17,0, new QTableWidgetItem("FE02"));
    visibility->setItem(17,1, new QTableWidgetItem("Serial Number"));
    visibility->setItem(18,0, new QTableWidgetItem("FE0C"));
    visibility->setItem(18,1, new QTableWidgetItem("Package Version ID (SV_PACKAGE)"));
    visibility->setItem(19,0, new QTableWidgetItem("FE0D"));
    visibility->setItem(19,1, new QTableWidgetItem("Package Version MD5 (SV_PACKAGE)"));
    visibility->setItem(20,0, new QTableWidgetItem("FE0A"));
    visibility->setItem(20,1, new QTableWidgetItem("Linux Distribution Version ID (SV_DISTRIB)"));
    visibility->setItem(21,0, new QTableWidgetItem("FE0B"));
    visibility->setItem(21,1, new QTableWidgetItem("Linux Distribution Version MD5 (SV_DISTRIB)"));
    visibility->setItem(22,0, new QTableWidgetItem("FE04"));
    visibility->setItem(22,1, new QTableWidgetItem("Application SmartVision Version ID (SV_FC_APP)"));
    visibility->setItem(23,0, new QTableWidgetItem("FE05"));
    visibility->setItem(23,1, new QTableWidgetItem("Application SmartVision Version MD5 (SV_FC_APP)"));
    visibility->setItem(24,0, new QTableWidgetItem("FE06"));
    visibility->setItem(24,1, new QTableWidgetItem("BitStream Zynq Version ID (SV_FC_FPGA)"));
    visibility->setItem(25,0, new QTableWidgetItem("FE07"));
    visibility->setItem(25,1, new QTableWidgetItem("BitStream Zynq Version MD5 (SV_FC_FPGA)"));
    visibility->setItem(26,0, new QTableWidgetItem("FE08"));
    visibility->setItem(26,1, new QTableWidgetItem("Bitstream Artix (Safety) Version ID (SV_SM_FPGA)"));
    visibility->setItem(27,0, new QTableWidgetItem("FE09"));
    visibility->setItem(27,1, new QTableWidgetItem("Bitstream Artix (Safety) Version MD5 (SV_SM_FPGA)"));
    visibility->setItem(28,0, new QTableWidgetItem("FE12"));
    visibility->setItem(28,1, new QTableWidgetItem("Ethernet MAC Address"));

    visibility->setItem(29,0, new QTableWidgetItem("Safety Controller"));                           //Title of the fourth category, the line is grayed out
    visibility->item(29,0)->setBackgroundColor(Qt::lightGray);
    visibility->setItem(29,1, new QTableWidgetItem(""));
    visibility->item(29,1)->setBackgroundColor(Qt::lightGray);
    visibility->setItem(29,2, new QTableWidgetItem(""));
    visibility->item(29,2)->setBackgroundColor(Qt::lightGray);

    //Identifiers and names of the fourth category
    visibility->setItem(30,0, new QTableWidgetItem("F197"));
    visibility->setItem(30,1, new QTableWidgetItem("System Name"));
    visibility->setItem(31,0, new QTableWidgetItem("F18A"));
    visibility->setItem(31,1, new QTableWidgetItem("Supplier Name"));
    visibility->setItem(32,0, new QTableWidgetItem("F18C"));
    visibility->setItem(32,1, new QTableWidgetItem("Serial Number"));
    visibility->setItem(33,0, new QTableWidgetItem("F18B"));
    visibility->setItem(33,1, new QTableWidgetItem("Manufacturing Date"));
    visibility->setItem(34,0, new QTableWidgetItem("F192"));
    visibility->setItem(34,1, new QTableWidgetItem("Hardware Number ID"));
    visibility->setItem(35,0, new QTableWidgetItem("F193"));
    visibility->setItem(35,1, new QTableWidgetItem("Hardware Number Version"));
    visibility->setItem(36,0, new QTableWidgetItem("F195"));
    visibility->setItem(36,1, new QTableWidgetItem("Application Software Number TAG"));
    visibility->setItem(37,0, new QTableWidgetItem("F126"));
    visibility->setItem(37,1, new QTableWidgetItem("Bootloader Software Number TAG"));

    visibility->setItem(38,0, new QTableWidgetItem("Protocols"));                                   //Title of the fifth category, the line is grayed out
    visibility->item(38,0)->setBackgroundColor(Qt::lightGray);
    visibility->setItem(38,1, new QTableWidgetItem(""));
    visibility->item(38,1)->setBackgroundColor(Qt::lightGray);
    visibility->setItem(38,2, new QTableWidgetItem(""));
    visibility->item(38,2)->setBackgroundColor(Qt::lightGray);

    //Identifiers and names of the fifth category
    visibility->setItem(39,0, new QTableWidgetItem("FE10"));
    visibility->setItem(39,1, new QTableWidgetItem("Modbus Version ID(Main Board)"));
    visibility->setItem(40,0, new QTableWidgetItem("FE33"));
    visibility->setItem(40,1, new QTableWidgetItem("Modbus Version ID(Safety Controller)"));
    visibility->setItem(41,0, new QTableWidgetItem("FE11"));
    visibility->setItem(41,1, new QTableWidgetItem("HSIS Version ID(Main Board)"));
    visibility->setItem(42,0, new QTableWidgetItem("FE31"));
    visibility->setItem(42,1, new QTableWidgetItem("HSIS Version ID(Safety Controller)"));
    visibility->setItem(43,0, new QTableWidgetItem("FE32"));
    visibility->setItem(43,1, new QTableWidgetItem("UDS Version ID(Safety Controller)"));
    visibility->setItem(44,0, new QTableWidgetItem("FE36"));
    visibility->setItem(44,1, new QTableWidgetItem("CAN DB Version (Private CAN)"));
    traceability();




}

Fenetretracability::~Fenetretracability()
{
    //It mustn't be destroyed in order to re-use it whenever showDetails is called
}
void Fenetretracability::traceability()
{

}
void Fenetretracability::ButtonQuit()
{
    this->close();                                                         //closes the window without destroying it
}

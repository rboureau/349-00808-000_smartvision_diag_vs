import QtQuick 2.4

import QtQuick.Window 2.3
import QtWebEngine 1.7
import QtWebKit 3.0

Window {
    width: 1024
    height: 750
    visible: true
    WebEngineView {anchors.fill: parent
                    url: "http://www.qt.io"
    }
}

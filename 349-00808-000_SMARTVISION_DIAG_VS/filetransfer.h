#ifndef FILETRANSFER_H
#define FILETRANSFER_H

#include <QWidget>
#include "QNetworkAccessManager"
#include <QUrl>
#include <QNetworkRequest>
#include <QFile>
#include <QNetworkReply>
class FileTransfer : public QWidget
{
    Q_OBJECT
public:
    explicit FileTransfer(QWidget *parent, QString fileName, QString address);
signals:
    void signalError(QNetworkReply::NetworkError);
    void signalFinished();
    void signalStart();
    void signalAbort();
public slots:
    void get();
    void put();
    void auth(QNetworkReply*rep,QAuthenticator*auth);
    void error(QNetworkReply::NetworkError error);
    void aborted();
};

#endif // FILETRANSFER_H

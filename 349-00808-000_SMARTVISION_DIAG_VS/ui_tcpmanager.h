/********************************************************************************
** Form generated from reading UI file 'tcpmanager.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TCPMANAGER_H
#define UI_TCPMANAGER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TcpManager
{
public:
    QWidget *centralwidget;
    QGroupBox *group_B_TCPTransfer;
    QToolButton *tool_B_openWindowSelectZIP;
    QLineEdit *line_E_Chemin_Fichier_ZIP;
    QPushButton *push_B_Send_ZIP;
    QProgressBar *progress_B_transfer;
    QTextEdit *text_E_log;
    QProgressBar *progress_B_log;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *TcpManager)
    {
        if (TcpManager->objectName().isEmpty())
            TcpManager->setObjectName(QStringLiteral("TcpManager"));
        TcpManager->resize(1188, 696);
        centralwidget = new QWidget(TcpManager);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        group_B_TCPTransfer = new QGroupBox(centralwidget);
        group_B_TCPTransfer->setObjectName(QStringLiteral("group_B_TCPTransfer"));
        group_B_TCPTransfer->setGeometry(QRect(20, 0, 265, 43));
        group_B_TCPTransfer->setStyleSheet(QStringLiteral("#groupBox_2 {border:null;}"));
        tool_B_openWindowSelectZIP = new QToolButton(group_B_TCPTransfer);
        tool_B_openWindowSelectZIP->setObjectName(QStringLiteral("tool_B_openWindowSelectZIP"));
        tool_B_openWindowSelectZIP->setGeometry(QRect(10, 12, 25, 19));
        line_E_Chemin_Fichier_ZIP = new QLineEdit(group_B_TCPTransfer);
        line_E_Chemin_Fichier_ZIP->setObjectName(QStringLiteral("line_E_Chemin_Fichier_ZIP"));
        line_E_Chemin_Fichier_ZIP->setGeometry(QRect(41, 11, 133, 20));
        line_E_Chemin_Fichier_ZIP->setReadOnly(true);
        push_B_Send_ZIP = new QPushButton(group_B_TCPTransfer);
        push_B_Send_ZIP->setObjectName(QStringLiteral("push_B_Send_ZIP"));
        push_B_Send_ZIP->setGeometry(QRect(936, 10, 75, 23));
        progress_B_transfer = new QProgressBar(centralwidget);
        progress_B_transfer->setObjectName(QStringLiteral("progress_B_transfer"));
        progress_B_transfer->setEnabled(true);
        progress_B_transfer->setGeometry(QRect(30, 40, 108, 21));
        progress_B_transfer->setMaximum(1);
        progress_B_transfer->setValue(0);
        progress_B_transfer->setTextVisible(true);
        text_E_log = new QTextEdit(centralwidget);
        text_E_log->setObjectName(QStringLiteral("text_E_log"));
        text_E_log->setEnabled(true);
        text_E_log->setGeometry(QRect(30, 70, 256, 192));
        text_E_log->setReadOnly(true);
        progress_B_log = new QProgressBar(centralwidget);
        progress_B_log->setObjectName(QStringLiteral("progress_B_log"));
        progress_B_log->setGeometry(QRect(30, 620, 108, 21));
        progress_B_log->setMaximum(104);
        progress_B_log->setValue(0);
        progress_B_log->setTextVisible(true);
        progress_B_log->setInvertedAppearance(false);
        progress_B_log->setTextDirection(QProgressBar::TopToBottom);
        TcpManager->setCentralWidget(centralwidget);
        menubar = new QMenuBar(TcpManager);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1188, 21));
        TcpManager->setMenuBar(menubar);
        statusbar = new QStatusBar(TcpManager);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        TcpManager->setStatusBar(statusbar);

        retranslateUi(TcpManager);

        QMetaObject::connectSlotsByName(TcpManager);
    } // setupUi

    void retranslateUi(QMainWindow *TcpManager)
    {
        TcpManager->setWindowTitle(QApplication::translate("TcpManager", "MainWindow", Q_NULLPTR));
        group_B_TCPTransfer->setTitle(QString());
        tool_B_openWindowSelectZIP->setText(QApplication::translate("TcpManager", "...", Q_NULLPTR));
        push_B_Send_ZIP->setText(QApplication::translate("TcpManager", "Send ZIP", Q_NULLPTR));
        progress_B_transfer->setFormat(QApplication::translate("TcpManager", "%p%", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class TcpManager: public Ui_TcpManager {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TCPMANAGER_H

#include "update.h"
#include "dll_visionsystem_sv.h"

#include "QTimer"
#include <stdlib.h>
#include "cp_xcom.h"
#include <QThread>
using namespace CP_Xcom;
using namespace SP_Chacom;
using namespace DLL_VisionSystem_SV;
void update::maj()
{

    QByteArray b;
    QByteArray aucSecurityKey_Prog;
     QStringList firmwareName (chemin);

    aucSecurityKey_Prog.resize(8);
    aucSecurityKey_Prog[0] = (char)0x33;
    aucSecurityKey_Prog[1] = (char)0x45;
    aucSecurityKey_Prog[2] = (char)0x12;
    aucSecurityKey_Prog[3] = (char)0x19;
    aucSecurityKey_Prog[4] = (char)0xBC;
    aucSecurityKey_Prog[5] = (char)0x00;
    aucSecurityKey_Prog[6] = (char)0xAE;
    aucSecurityKey_Prog[7] = (char)0xAA;

    //ppVs.PP_ShowUdsScreen_To_VisionSystem_SV();
    ppVs->GetCanUdsGateway()->UDS_SL_bReadDataByIdentifier_ByteArray(UDS_eRWDataById_Identfiers_systemSupplierECUSoftwareNumberDataIdentifier,b);
    cout << b.toStdString() << "\r\n";

    // First, open the programming session
    if (ppVs->GetCanUdsGateway()->UDS_SL_bDiagnosticSessionControl((byte)UDS_SessionsIdentifiers_programmingSession, b) == false)
    {
        cout << "Cannot open the UDS programming session ";
    }
    else
    {
        // Need to wait that Safety Controller enters in programming session
        Sleep(uint(1000));

        // Then enter the security access key
        if (ppVs->GetCanUdsGateway()->UDS_SL_bSecurityAccess((byte)UDS_SecuriryAccessTypesIdentifiers_Security_Level_1, aucSecurityKey_Prog) == false)
        {
            cout << "Security access has failed, UDS programming session Level 1";
        }
        else
        {
            cout << "Successfully entered in Programming Session, ready to update the firmware\r\n";



            Sleep(uint(1000));
            if (ppVs->GetCanUdsGateway()->UDS_SL_bSoftwareDownload(firmwareName) == false)
            {
                cout << "Fail to start the firmware download";
            }
            else
            {
                cout << "Download in Done ";
                //stop=true;
            }
            //}
            // Reset Ecu
            {
                uint reftime = 0;
                ppVs->GetCanUdsGateway()->UDS_SL_bECUReset(UDS_ResetTypes_hardReset, false, reftime);
            }
            QByteArray c;
            QByteArray d;
            ppVs->GetCanUdsGateway()->UDS_SL_bReadDataByIdentifier_ByteArray(61845,c);
            ppVs->GetCanUdsGateway()->UDS_SL_bReadDataByIdentifier_ByteArray(61844,d);
            cout << c.toStdString();
            cout << d.toStdString();

            return;
        }
    }
}
void update::progressionChangement(){
    qDebug() << "thread progressioncganhmeebt:" << this->thread()->currentThreadId();
    while(ppVs->GetCanUdsGateway()->BlockProgress()!=100){
        emit emitProgression(ppVs->GetCanUdsGateway()->BlockProgress());
        qDebug() << "en cours";
    }

    emit emitProgression(ppVs->GetCanUdsGateway()->BlockProgress());
    qDebug() << "fin process";
    return;
}

update::update()
{
    cout<<"Demo Chadoc3 Smart Vision\n"<<endl;

    if( SUCCEEDED( CoInitializeEx( 0, COINIT_APARTMENTTHREADED ) ) )
    {

        ppVs= new PP_VisionSystem_SV();

        cout << "CoInit succes\r\n";

        if (ppVs->isNull())
        {
            cout << "error";
        }
        else
        {
            if (ppVs->SetPortSettings("COM4","C:/Users/rboureau/Desktop/log.txt",false) == true){

                    cout << "Configuration OK\r\n";
                    if (ppVs->ConnectToChadoc("", true) == true)
                    {
                        cout << "Connexion Ok\r\n";
                    }
                    else
                    {
                        cout << "Fail to connect\r\n";
                    }

                    // Connect to Smart Vision Driver
                    ppVs->connectToSmartVision("", true, false);

                    // Connect to Smart Vision Passenger
                    //ppVs.connectToSmartVision("", true, true);
                }
                else
                {
                    cout << "fail to configure\r\n";
                }

                if (ppVs->PP_bCheckProjectNotReadyForUse() == true)
                {
                    cout << "Not Ready\r\n";
                }
                else
                {
                    cout << "Ready\r\n";
                }

                // UART communication point
                {
                    if (ppVs->GetCommunicationPoint() != 0)
                    {
                        cout << "is connected ? " << ppVs->GetCommunicationPoint()->XCOM_isConnected() << "\r\n";

                        // To disconnect, use command below
                        //ppVs.GetCommunicationPoint()->XCOM_bDisconnect();
                        //cout << "is connected ? " << ppVs.GetCommunicationPoint()->XCOM_isConnected() << "\r\n";
                    }
                }

                // Communication with target over CAN
                {
                    if (ppVs->GetCanUdsGateway() != 0)
                    {
                        //qDebug() << "thread princpal:" << this->thread()->currentThreadId();
                      // QFuture<void> f1 = run(maj,ppVs);
                       //QFuture<void> f2 = QtConcurrent::run(progressionChangement());
                    }

                }
            }
        }
        else
        {
            cout << "CoInit failed";
        }

}
void update::threads(QString value){
    chemin=value.replace("/","\\");
    qDebug() << chemin;

    QtConcurrent::run(this, &update::maj);
    this->thread()->currentThread()->sleep(10);
    QtConcurrent::run(this, &update::progressionChangement);
}

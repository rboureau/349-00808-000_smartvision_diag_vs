/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_SendConfigByFTP;
    QPushButton *pushButton_FTP_Manager;
    QPushButton *pushButton_Basic_Diagnostic;
    QSpacerItem *horizontalSpacer;
    QGroupBox *group_B_connection;
    QGridLayout *gridLayout;
    QRadioButton *radio_B_Connection_TCP;
    QRadioButton *radio_B_Connection_USB;
    QLineEdit *line_E_Connection_TCP;
    QPushButton *push_B_Connection_Final;
    QComboBox *combo_B_Connection_USB;
    QPushButton *push_B_Connection_USB_Refresh;
    QSpacerItem *horizontalSpacer_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(682, 456);
        MainWindow->setStyleSheet(QStringLiteral(""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QStringLiteral(""));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pushButton_SendConfigByFTP = new QPushButton(groupBox);
        pushButton_SendConfigByFTP->setObjectName(QStringLiteral("pushButton_SendConfigByFTP"));
        pushButton_SendConfigByFTP->setMinimumSize(QSize(150, 150));

        horizontalLayout->addWidget(pushButton_SendConfigByFTP);

        pushButton_FTP_Manager = new QPushButton(groupBox);
        pushButton_FTP_Manager->setObjectName(QStringLiteral("pushButton_FTP_Manager"));
        pushButton_FTP_Manager->setEnabled(false);
        pushButton_FTP_Manager->setMinimumSize(QSize(150, 150));

        horizontalLayout->addWidget(pushButton_FTP_Manager);

        pushButton_Basic_Diagnostic = new QPushButton(groupBox);
        pushButton_Basic_Diagnostic->setObjectName(QStringLiteral("pushButton_Basic_Diagnostic"));
        pushButton_Basic_Diagnostic->setEnabled(false);
        pushButton_Basic_Diagnostic->setMinimumSize(QSize(150, 150));

        horizontalLayout->addWidget(pushButton_Basic_Diagnostic);


        gridLayout_2->addWidget(groupBox, 0, 0, 1, 3);

        horizontalSpacer = new QSpacerItem(198, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 1, 0, 1, 1);

        group_B_connection = new QGroupBox(centralWidget);
        group_B_connection->setObjectName(QStringLiteral("group_B_connection"));
        group_B_connection->setStyleSheet(QStringLiteral("#group_B_{background-color: #002D59;}"));
        gridLayout = new QGridLayout(group_B_connection);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        radio_B_Connection_TCP = new QRadioButton(group_B_connection);
        radio_B_Connection_TCP->setObjectName(QStringLiteral("radio_B_Connection_TCP"));
        radio_B_Connection_TCP->setEnabled(false);
        radio_B_Connection_TCP->setChecked(false);

        gridLayout->addWidget(radio_B_Connection_TCP, 1, 0, 1, 1);

        radio_B_Connection_USB = new QRadioButton(group_B_connection);
        radio_B_Connection_USB->setObjectName(QStringLiteral("radio_B_Connection_USB"));
        radio_B_Connection_USB->setChecked(true);

        gridLayout->addWidget(radio_B_Connection_USB, 0, 0, 1, 1);

        line_E_Connection_TCP = new QLineEdit(group_B_connection);
        line_E_Connection_TCP->setObjectName(QStringLiteral("line_E_Connection_TCP"));
        line_E_Connection_TCP->setEnabled(false);

        gridLayout->addWidget(line_E_Connection_TCP, 1, 1, 1, 2);

        push_B_Connection_Final = new QPushButton(group_B_connection);
        push_B_Connection_Final->setObjectName(QStringLiteral("push_B_Connection_Final"));
        push_B_Connection_Final->setEnabled(false);
        push_B_Connection_Final->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(push_B_Connection_Final, 2, 0, 1, 1);

        combo_B_Connection_USB = new QComboBox(group_B_connection);
        combo_B_Connection_USB->setObjectName(QStringLiteral("combo_B_Connection_USB"));

        gridLayout->addWidget(combo_B_Connection_USB, 0, 1, 1, 1);

        push_B_Connection_USB_Refresh = new QPushButton(group_B_connection);
        push_B_Connection_USB_Refresh->setObjectName(QStringLiteral("push_B_Connection_USB_Refresh"));

        gridLayout->addWidget(push_B_Connection_USB_Refresh, 0, 2, 1, 1);


        gridLayout_2->addWidget(group_B_connection, 1, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(197, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 1, 2, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 682, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "SmartVision Diag VS", Q_NULLPTR));
#ifndef QT_NO_STATUSTIP
        MainWindow->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        groupBox->setTitle(QString());
        pushButton_SendConfigByFTP->setText(QApplication::translate("MainWindow", "Configuration", Q_NULLPTR));
        pushButton_FTP_Manager->setText(QApplication::translate("MainWindow", "Update", Q_NULLPTR));
        pushButton_Basic_Diagnostic->setText(QApplication::translate("MainWindow", "Traceability", Q_NULLPTR));
        group_B_connection->setTitle(QApplication::translate("MainWindow", "Connection", Q_NULLPTR));
        radio_B_Connection_TCP->setText(QApplication::translate("MainWindow", "TCP link", Q_NULLPTR));
        radio_B_Connection_USB->setText(QApplication::translate("MainWindow", "USB Serial", Q_NULLPTR));
        line_E_Connection_TCP->setPlaceholderText(QApplication::translate("MainWindow", "Destination IP Adress", Q_NULLPTR));
        push_B_Connection_Final->setText(QApplication::translate("MainWindow", "Connect", Q_NULLPTR));
        push_B_Connection_USB_Refresh->setText(QApplication::translate("MainWindow", "Refresh", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

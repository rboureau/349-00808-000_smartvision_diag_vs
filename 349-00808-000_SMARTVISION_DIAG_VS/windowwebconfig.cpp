﻿#include "windowwebconfig.h"
#include "ui_windowwebconfig.h"

WindowWebConfig::WindowWebConfig(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WindowWebConfig)
{
    ui->setupUi(this);

    setWindowModality(Qt::ApplicationModal);
    //Connects the buttons to the corresponding slots
    connect(ui->pushButton_ReferenceCancel,&QPushButton::clicked,this,&WindowWebConfig::close);
    connect(ui->pushButton_CommentCancel,&QPushButton::clicked,this,&WindowWebConfig::CancelButton);
    connect(ui->pushButton_ConnectDriverCancel,&QPushButton::clicked,this,&WindowWebConfig::CancelButton);
    connect(ui->pushButton_ConnectPassengerCancel,&QPushButton::clicked,this,&WindowWebConfig::CancelButton);
    connect(ui->pushButton_SendFileCancel,&QPushButton::clicked,this,&WindowWebConfig::CancelButton);
    connect(ui->pushButton_WebConfigDriverCancel,&QPushButton::clicked,this,&WindowWebConfig::CancelButton);
    connect(ui->pushButton_WebConfigPassengerCancel,&QPushButton::clicked,this,&WindowWebConfig::CancelButton);
    connect(ui->pushButton_ReferenceNext,&QPushButton::clicked,this,&WindowWebConfig::ReferenceNextButton);
    connect(ui->pushButton_CommentNext,&QPushButton::clicked,this,&WindowWebConfig::CommentNextButton);
    connect(ui->pushButton_ConnectDriverNext,&QPushButton::clicked,this,&WindowWebConfig::RefreshWebPageDriver);
    connect(ui->pushButton_ConnectPassengerNext,&QPushButton::clicked,this,&WindowWebConfig::RefreshWebPagePassenger);
    connect(ui->pushButton_SendFiles,&QPushButton::clicked,this,&WindowWebConfig::SendFilesButton);
    connect(ui->pushButton_WebConfigDriverNext,&QPushButton::clicked,this,&WindowWebConfig::setTab);
    connect(ui->pushButton_WebConfigPassengerNext,&QPushButton::clicked,this,&WindowWebConfig::setTab);

    //Blocks all tabs except first one (index 0)
    ui->tabWidget->setTabEnabled(1,false);
    ui->tabWidget->setTabEnabled(2,false);
    ui->tabWidget->setTabEnabled(3,false);
    ui->tabWidget->setTabEnabled(4,false);
    ui->tabWidget->setTabEnabled(5,false);
    ui->tabWidget->setTabEnabled(6,false);

    //QWebEngineView *view = new QWebEngineView(ui->groupBox_WebConfigDriverNavigator);


    //Define the WebEngine profiles to manage the two downloads. NoPersistentCookies is a parameter to stock the cookies in the navigator memory. Cookies are lost when it closes.

    m_driverProf = new QWebEngineProfile();
    m_driverProf->setPersistentCookiesPolicy(QWebEngineProfile::NoPersistentCookies);

    m_passengerProf = new QWebEngineProfile();
    m_passengerProf->setPersistentCookiesPolicy(QWebEngineProfile::NoPersistentCookies);

    //Creates a progressBar to display the loading of the web page
    m_chargementPageWebDriver= new QProgressBar();
    ui->WebEngineViewDriver->setMinimumWidth(998);
    ui->WebEngineViewDriver->adjustSize();                      //Resizes the navigator to the optimal size
    m_pageConfigDriver = new QWebEnginePage(m_driverProf,this);     //Create a web page object with the driver profile
    QString sAddress= "https://192.168.69.1/login.php";          //driver side web page address
    qDebug() << sAddress;
    m_pageConfigDriver->setUrl(QUrl(sAddress));                    //Defines adresse as the pageConfigDriver address
    connect(m_pageConfigDriver,&QWebEnginePage::loadProgress,this,&WindowWebConfig::webBarLoadDriver); //Connects the progressionBar to the loading
    //QWebEngineProfile *driverprof = new QWebEngineProfile("Driver",this);
    connect(m_driverProf, &QWebEngineProfile::downloadRequested,
                    this, &WindowWebConfig::downloadRequestedDriver);  //Connects download request to the specific slot

    ui->groupBox_WebConfigDriverNavigator->layout()->addWidget(m_chargementPageWebDriver);    //insert the progressBar in the layout of the groupbox containing the navigator
    ui->WebEngineViewDriver->setPage(m_pageConfigDriver);                                     //Define the navigator's webpage
    ui->WebEngineViewDriver->show();                                                        //Show the navigator

    //Same
    m_chargementPageWebPassenger= new QProgressBar();
    ui->WebEngineViewPassenger->setMinimumWidth(998);
    ui->WebEngineViewPassenger->adjustSize();
    m_pageConfigPassenger = new QWebEnginePage(m_passengerProf,this);
    QString sAddressP= "https://192.168.69.2/login.php";
    qDebug() << sAddressP;
    m_pageConfigPassenger->setUrl(QUrl(sAddressP));
    connect(m_pageConfigPassenger,&QWebEnginePage::loadProgress,this,&WindowWebConfig::webBarLoadPassenger);
    //QWebEngineProfile *driverprof = new QWebEngineProfile("Driver",this);
    connect(m_passengerProf, &QWebEngineProfile::downloadRequested,
                    this, &WindowWebConfig::downloadRequestedPassenger);
    ui->groupBox_WebConfigPassengerNavigator->layout()->addWidget(m_chargementPageWebPassenger);
    ui->WebEngineViewPassenger->setPage(m_pageConfigPassenger);
    ui->WebEngineViewPassenger->show();
    //ui->groupBox_WebConfigDriverNavigator->layout()->addWidget(view);


    //This variable is the name of the FTP server, it' i's different when we are in a VS Network or on the internet
    m_sLocalOrInternet="ftp.vision-systems.fr";
    //Counts the number of transfer attempts if it fails
    m_iNumberAttemptsSendFiles=0;
    //booleans to detect a wrong name on the ftp server
    m_bWarningNameTooLong=false;
    m_bWarningForbiddenCharacters=false;


}



WindowWebConfig::~WindowWebConfig()
{
    delete ui;
}



void WindowWebConfig::setTab()
{
    // This function changes the active tab
    int iCurrentIndex=ui->tabWidget->currentIndex(); // get the active tab index

    if(iCurrentIndex<6){         //if it's not the last
        ui->tabWidget->setTabEnabled(iCurrentIndex+1,true);  //Enables the next
        ui->tabWidget->setCurrentIndex(iCurrentIndex+1); //Change active tab to the next
        ui->tabWidget->setTabEnabled(iCurrentIndex,false); //Disable the former active tab index



    }
}



void WindowWebConfig::downloadRequestedDriver(QWebEngineDownloadItem* download) {
              qDebug() << "Path: " << download->path();
              QString sOriginalName;
              // get the file name by taking the full path's last part
              QStringList listOriginalName = download->path().split('/', QString::SkipEmptyParts); // Split the full path with '/' as the separator.Skipping the empty strings
              sOriginalName=listOriginalName.last(); //Retrieve the name
              m_sOriginalNameDriver=sOriginalName;  //Stocks it as a class attribute
              qDebug() << "Original Name: " << sOriginalName; //Display it
              download->setPath(m_sXMLPath+"/"+sOriginalName); //Change the download path to have the document in the right directory
              qDebug() << "Path: " << download->path();   //Display the new path
            // If you want to modify something like the default path or the format

            // Check your url to accept/reject the download
            download->accept(); //Accepts Download
            ui->pushButton_WebConfigDriverNext->setEnabled(true); //Enables the next button
//        }
}



void WindowWebConfig::webBarLoadDriver(int progress)
{
    //change the progressBar to match the loading value
    m_chargementPageWebDriver->setValue(progress);
}


//same for passenger
void WindowWebConfig::downloadRequestedPassenger(QWebEngineDownloadItem* download) {

              qDebug() << "Path: " << download->path();
              QString sOriginalName;
              QStringList listOriginalName = download->path().split('/', QString::SkipEmptyParts);
              sOriginalName=listOriginalName.last();
              m_sOriginalNamePassenger=sOriginalName;
              qDebug() << "Original Name: " << sOriginalName;
              download->setPath(m_sXMLPath+"/"+sOriginalName);
              qDebug() << "Path: " << download->path();
            // If you want to modify something like the default path or the format
//            download->setSavePageFormat(...);

            // Check your url to accept/reject the download
            download->accept();
            ui->pushButton_WebConfigPassengerNext->setEnabled(true);
//        }
}



void WindowWebConfig::webBarLoadPassenger(int progress)
{
    m_chargementPageWebPassenger->setValue(progress);
}



void WindowWebConfig::ReferenceNextButton()
{
    QSettings settings("Vision Systems", "SmartVision Diag VS");
    QString sReference;
    sReference="057-" +ui->lineEdit_ReferenceInput->text()+"-527";  //Retrieves the reference of the SmartVision
    QString sNomDossier;
    sNomDossier=settings.value("cheminRepertoire").toString() +"/"+ sReference ; //Retrieves the local storage path, adding the reference number to create a file for this reference
    if(!QDir(settings.value("cheminRepertoire").toString()).exists())          //If local storage path doesn't exists
    {
        QDir().mkdir(settings.value("cheminRepertoire").toString());           //It's created
    }
    if(!QDir(sNomDossier).exists())              //if the reference directory doesn't exists, it' created
    {
        QDir().mkdir(sNomDossier);
    }
    int a=1;
    while(QDir(sNomDossier+"/"+sReference+"-"+QString::fromStdString(std::to_string(a))).exists()) //Defines the next number available
    {
        a++;
    }
    QString nameRepertoire=sNomDossier+"/"+sReference+"-"+QString::fromStdString(std::to_string(a)); //Creates a local directory with this number
    QDir().mkdir(nameRepertoire);
    if(!QDir(nameRepertoire+"/smartvision.package").exists()) //Creates a local directory named smartvision.package
    {
        QDir().mkdir(nameRepertoire+"/smartvision.package");
    }
    m_sReferenceValue=sReference;   //stores the reference as a class variable
    m_sRelativePathComment=sReference+"/"+sReference+"-"+QString::fromStdString(std::to_string(a)); // stores the relative path for the Readme as a class variable
    m_sRelativePathXML=sReference+"/"+sReference+"-"+QString::fromStdString(std::to_string(a))+"/smartvision.package"; // stores the relative path for the XML files as a class variable
    m_sDirectoryPath=sNomDossier;   //stores the local storage path as a class variable
    m_sCommentPath=sNomDossier+"/"+sReference+"-"+QString::fromStdString(std::to_string(a)); //stores the local storage path for the Readme as a class variable
    m_sXMLPath=sNomDossier+"/"+sReference+"-"+QString::fromStdString(std::to_string(a))+"/smartvision.package"; // stores the local storage path for the XML files as a class variable
    m_bEmptyFile=true;                                         //To check if the reference directory is empty
    //max="A";                                                //Initialisation of the variable indicating the last index for this reference on the ftp server
    m_iMax=0;
    QFtp *dircreator = new QFtp(this);                      //Creates a QFtp object that will check the directories on the FTP server
    dircreator->connectToHost(m_sLocalOrInternet);             //Connection
    dircreator->login("rd","n9k5X&");                       //Enters correct login and password
    connect(dircreator,&QFtp::listInfo,this, &WindowWebConfig::FTPServerFileNamesTreatment); //connect the signal returning the directory names on the ftp server to a special treatment slot
    dircreator->list("Production_SmartVision/"+m_sReferenceValue); //it takes a few seconds, that's why we do it now
    qDebug() <<m_iMax;
    qDebug() << dircreator->state();
    setTab();                                               //moves to the next tab
}



void WindowWebConfig::CommentNextButton()
{
    QFile readme(m_sCommentPath+"/README.txt"); //Creates a README.txt file
    readme.open(QIODevice::ReadWrite | QIODevice::Text); // Opens it
    QTextStream stream( &readme );                       //QTextStream makes writing easier
    stream << ui->textEdit_CommentInput->toPlainText() << endl; // Retrieves all available text in the dedicated zone
    readme.close();                      //Closes the readme file
    setTab();                            //moves to the next tab
}



void WindowWebConfig::SendFilesButton()
{
    m_iNumberSuccess=0;
    if(m_bWarningNameTooLong){
        QMessageBox::warning(this,"Error Config name","One of the names of config folder in the ftp deposit is wrong. Please check it on the ftp deposit.");
        //Warns the user of a too long name on the ftp server
    }
    if(m_bWarningForbiddenCharacters)
    {
        QMessageBox::warning(this,"Error Config name","One of the names of config folder in the ftp deposit contains forbidden characters. Please check it on the ftp deposit.");
        //Warns the user of forbidden characters in a name on the ftp server
    }
    QFtp *dircreator = new QFtp(this); //Another connection to the ftp server
    dircreator->connectToHost(m_sLocalOrInternet);
    dircreator->login("rd","n9k5X&");

    qDebug() << m_iMax;

   QString sEndOfName=nameNewFolder(m_iMax); //Determines the next index for this reference number directory
    qDebug()<<"#######" << sEndOfName;
    if(sEndOfName!="1000"){
    dircreator->mkdir("Production_SmartVision/"+m_sReferenceValue+"/"+m_sReferenceValue+"-"+sEndOfName+"/smartvision.package");//Creates the directories in the ftp server
    dircreator->mkdir("Production_SmartVision/"+m_sReferenceValue+"/"+m_sReferenceValue+"-"+sEndOfName+"/LivraisonClient");
    dircreator->mkdir("Production_SmartVision/"+m_sReferenceValue+"/"+m_sReferenceValue+"-"+sEndOfName);
    dircreator->close();




    //File Transfer
    QNetworkAccessManager *manager = new QNetworkAccessManager;
    // The FTP Server will require identification, so we can already connect the IDs to the signal authenticationRequired
    connect(manager, &QNetworkAccessManager::authenticationRequired, this, &WindowWebConfig::auth);



    //Driver File
    QFile driverxml(m_sXMLPath+"/"+m_sOriginalNameDriver);              //Opens the xml driver file
    driverxml.open(QIODevice::ReadWrite);
    QString sAdresseRequeteDriver="ftp://"+m_sLocalOrInternet+ "/Production_SmartVision/"+m_sReferenceValue+"/"+m_sReferenceValue+"-"+sEndOfName+"/smartvision.package"+"/"+m_sOriginalNameDriver; // Defines the destination adress
    qDebug() << m_iMax+1;
    QNetworkReply *replyenvdriver = manager->put(QNetworkRequest(QUrl(sAdresseRequeteDriver)),driverxml.readAll());  //Executes the request
    qDebug() << m_iMax+1;
    // error management
    connect(replyenvdriver, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
    connect(replyenvdriver, &QNetworkReply::finished, this, &WindowWebConfig::sent);
    //Same for passenger file
    QFile passengerxml(m_sXMLPath+"/"+m_sOriginalNamePassenger);
    passengerxml.open(QIODevice::ReadWrite);
    QString sAdresseRequetePassenger="ftp://"+m_sLocalOrInternet+ "/Production_SmartVision/"+m_sReferenceValue+"/"+m_sReferenceValue+"-"+sEndOfName+"/smartvision.package"+"/"+m_sOriginalNamePassenger;
    QNetworkReply *replyenvPassenger = manager->put(QNetworkRequest(QUrl(sAdresseRequetePassenger)),passengerxml.readAll());

    // error management
    connect(replyenvPassenger, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
    connect(replyenvPassenger, &QNetworkReply::finished, this, &WindowWebConfig::sent);
    //Same for comment file
    QFile commentairetxt(m_sCommentPath+"/README.txt");
    commentairetxt.open(QIODevice::ReadWrite);
    QString sAdresseRequeteCommentaire="ftp://"+m_sLocalOrInternet+ "/Production_SmartVision/"+m_sReferenceValue+"/"+m_sReferenceValue+"-"+sEndOfName+"/README.txt";
    QNetworkReply *replyenvCommentaire = manager->put(QNetworkRequest(QUrl(sAdresseRequeteCommentaire)),commentairetxt.readAll());

    // error management
    connect(replyenvCommentaire, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
    connect(replyenvCommentaire, &QNetworkReply::finished, this, &WindowWebConfig::sent);
    //
    }else{
        QMessageBox::critical(this,"FTP depository full","The depository for this reference is full. The config is saved in local storage but wasn't sent to the ftp server");
    }
    dircreator->close();
    close();
}



void    WindowWebConfig::auth(QNetworkReply*rep,QAuthenticator*auth)
{
    qDebug() << "auth";                                             //login and password see TransfertFichiers
    auth->setUser("rd");
    auth->setPassword("n9k5X&");
}



void    WindowWebConfig::progress(qint64 progress, qint64 total)
{
    qDebug() << "progression";
    qint64 pourcent = (progress / total) * 100;
    qDebug() << pourcent;
}



void    WindowWebConfig::error(QNetworkReply::NetworkError erreur)
{
    qDebug() << "error" << erreur;
    if(m_iNumberAttemptsSendFiles<3){
        //if there's a problem when the files are sent, another attempt is made. 3 Attempts max.
        QMessageBox::critical(this,"Error Transfer","The transfer wasn't completed. An other transfer will be made once you clicked the Ok Button");
        m_iNumberAttemptsSendFiles++;
        SendFilesButton();
    }


}



void WindowWebConfig::CancelButton(){
    //If files were created locally and the user cancels, they must be erased
    //path mustn't be empty to avoid deleting other files
    if(QFile(m_sXMLPath+"/"+m_sOriginalNameDriver).exists() && m_sXMLPath!="" && m_sOriginalNameDriver!=""){
        qDebug() << m_sXMLPath+"/"+m_sOriginalNameDriver;
        QFile(m_sXMLPath+"/"+m_sOriginalNameDriver).remove();
    }
    if(QFile(m_sXMLPath+"/"+m_sOriginalNamePassenger ).exists() && m_sXMLPath!="" && m_sOriginalNamePassenger!=""){
        qDebug() << m_sXMLPath+"/"+m_sOriginalNamePassenger;
        QFile(m_sXMLPath+"/"+m_sOriginalNamePassenger).remove();
    }
    if(QFile(m_sCommentPath+"README.txt" ).exists() && m_sCommentPath!=""){
        qDebug() << m_sCommentPath+"README.txt";
        QFile(m_sCommentPath+"README.txt").remove();
    }
    if(QDir(m_sDirectoryPath).exists() && m_sDirectoryPath!=""){
        qDebug() << m_sDirectoryPath;
        QDir(m_sDirectoryPath).removeRecursively();


    }
    close(); //Closes the Window
}



void WindowWebConfig::RefreshWebPageDriver(){
    ui->WebEngineViewDriver->reload();  //slot to reload the webpage
    setTab();                      //moves to the next tab
}



void WindowWebConfig::RefreshWebPagePassenger(){
    ui->WebEngineViewPassenger->reload(); //Same
    setTab();
}

void    WindowWebConfig::sent()
{
    //See TransfertFichiers
    qDebug() << "envoyé";
    QNetworkReply *r = qobject_cast<QNetworkReply*>(sender()); //Gets ftp server response
    // on l'ouvre
    qDebug() << r->error();
    if(r->error()==QNetworkReply::NoError){
        m_iNumberSuccess++;
        if(m_iNumberSuccess==3){
            QMessageBox::information(this, tr("Config Transfer"), tr("Transfer Successful !") );
        }
    }
    r->deleteLater();
    //close();
}

//Function with numbers
void WindowWebConfig::FTPServerFileNamesTreatment(QUrlInfo filename)
{
        //This function checks the names on the ftp server to find the next index
        //Forbidden characters are treated by versionNumber.toInt(). It returns 0 when the conversion encountered an error, so it doesn't have an impact on the max
        m_bEmptyFile=false;                                //if this slot is called, the reference directory on the ftp server isn't empty
        QString sFilenameString=filename.name();         //Retrieves the name of the file found
        qDebug() << sFilenameString;
        if(sFilenameString.contains("057-") && sFilenameString.contains("-527")) //Filters the reference directories
        {
            if(sFilenameString.count("-")==3){                           //Filters version directories
                QStringList liste = sFilenameString.split("-");          //Split the name with '-' as separator
                QString sVersionNumber = liste.last();                   //The last part is the index
                if(sVersionNumber.size()<4){                             //Checks the index is under 4 characters long
                    if(sVersionNumber.toInt()>m_iMax){                      //if the index is superior to the ones of all the previously tested file names
                        m_iMax=sVersionNumber.toInt();                      //it becomes the new maximum
                    }
                }else{
                    m_bWarningNameTooLong=true;    //elsewise, there is a problem, the name isn't supposed to be that long
                }

            }
        }
}

//Function with letters
//void WindowWebConfig::traitementNomFichierFTPServer(QUrlInfo filename)
//{
//    bool versionNumberIsUpper=true;                 //vérifie que tout les indices de version sont en majuscules
//    emptyfile=false;                                //si ceci est appelé, cela signifie que le répertoire de la référence a déjà été créé
//    QString filenameString=filename.name();         //on récupère le nom à traiter
//    qDebug() << filenameString;
//    if(filenameString.contains("057-") && filenameString.contains("-527")) //filtre les dossier de référence
//    {
//        if(filenameString.count("-")==3){                           //filtre les dossier de version
//            QStringList liste = filenameString.split("-");          //on sépare le nom selon les tirets
//            QString versionNumber = liste.last();                   //le dernier est le numéro de version
//            for (int i=0;i<versionNumber.length();i++){             //on parcourt la chaine pour vérifier que l'indice de version est composé uniquement de majuscules
//                if(!versionNumber.at(i).isUpper()){
//                    versionNumberIsUpper=false;
//                }
//            }
//            if(versionNumberIsUpper){       //Si tout est en majuscules


//                if(versionNumber.size()>max.size()) //si l'indice de version traité est plus grand que l'indice max, il remplace cet indice
//                {
//                    max=versionNumber;
//                    if(versionNumber.size()>4){     //on vérifie que l'indice de version traité est inférieure à 4 caractères
//                        warningNameTooLong=true;    //Sinon on avertit qu'il y a un problème puisque ce n'est pas normal

//                    }
//                }else if (versionNumber.size()==max.size()){ //si les deux indices sont de même taille, on les trie dans l'ordre alphabétique
//                    int comparator = QString::compare(versionNumber,max,Qt::CaseSensitive);
//                    if(comparator>0){
//                        max=versionNumber;      //si versionNumber est après, il remplace max
//                    }
//                }
//            }else{
//                warningForbiddenCharacters=true;    //Sinon on prévient que cet indice de version contient un caractère interdit
//            }
//        }
//    }
//}



//Function with letters
//QString WindowWebConfig::nameNewFolder(QString indice)
//{
//    //ici on trouve le nouvel indice pour le ftp
//    //le paramètre correspond à l'indice max actuel
//    QString resultat="";    //on créé une string vide qui contiendra notre résultat
//    int retenue=1;          //on met la retenue à 1 car on souhaite aller à l'indice suivant
//    for(int i=indice.size()-1;i>=0;i--){     //Nous allons parcourir la chaine à l'envers
//        QChar currentChar=indice.at(i); //On récupère le caractère à la position i
//        qDebug() << currentChar;
//        if(currentChar=='Z' && retenue==1){ //Si l'on est à Z et qu'il faut passer au suivant on repasse à A avec un indice à 1
//            currentChar='A';
//            retenue=1;

//        }else{
//            currentChar=(QChar) (currentChar.unicode()+retenue);  //Sinon on ajoute la retenue à l'unicode du caractère, et on récupère ce caractère
//            qDebug()<<currentChar;
//            retenue=0;                                      //la retenue passe à Zéro car la valeur a changé
//        }
//        resultat.prepend(currentChar);                      //On ajoute le caractère traité à l'avant
//    }
//    if(retenue==1){
//        resultat.prepend('A');                      //Si la retenue est toujours à 1, cela signifie que l'on a atteint le max a cette taille, il faut donc ajouter un A au début pour augmenter la taille
//        retenue=0;
//    }

//    if(emptyfile){                                  //Si aucun dossier n'a été trouvé sur le ftp, on créer le premier indice, A
//        resultat="A";
//    }
//    return resultat;
//}

//Fonction with numbers
QString WindowWebConfig::nameNewFolder(int indice)
{

    return QString::number(indice+1);
}




void WindowWebConfig::testNames(){      //fonction test

}
void WindowWebConfig::changeNetwork(bool local){
    if(local){
        m_sLocalOrInternet="ftp-vs";  //VS Network
    }else{
        m_sLocalOrInternet="ftp.vision-systems.fr"; //Other Networks(internet)
    }
}

#include "mainwindow.h"
#include "fenetretracability.h"
#include <QApplication>
#include "packetsender.h"
#include <QDir>
#include "basicdiagnostic.h"
#include "ftpmanager.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //Define the name of the company, of the software and version number. It will be used by QSettings.
    QApplication::setOrganizationName("Vision Systems");
    QApplication::setApplicationName("SmartVision Diag VS");
     QApplication::setApplicationVersion("1.0");
    //Creates a main window object
    MainWindow w;
    w.show();


    //if it doesn't exists, creates the storage directory which contains files created in WindowWebConfig and the update log.
    QString sSoftwareDirectory="C:/Users/Public/SmartVision_Diag_VS_Files";
    if(!QDir(sSoftwareDirectory).exists()){
        QDir().mkdir(sSoftwareDirectory);
    }


    //QSettings stores variables between session, and allow for these variables to be used in other classes.
    QSettings settings("Vision Systems", "SmartVision Diag VS");
    settings.setValue("cheminRepertoire",sSoftwareDirectory);                //Stores the path in QSettings to be able to use it anywhere




    return a.exec();
}

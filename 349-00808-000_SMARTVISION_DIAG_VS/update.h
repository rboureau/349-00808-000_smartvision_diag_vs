#ifndef UPDATE_H
#define UPDATE_H

#include <QObject>
#include <QDebug>
#include <QTableWidget>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <QUuid>
#include "QApplication"
#include <Windows.h>
#include <QUuid>
#include "cp_xcom.h"
#include "sp_chacom.h"
#include "dll_visionsystem_sv.h"
#include <QWidget>
#include <QtConcurrent/QtConcurrent>
#include <QObject>
using namespace std;
using namespace QtMetaTypePrivate;
using namespace mscorlib;

using namespace SP_Chacom;
using namespace DLL_VisionSystem_SV;
using namespace CP_Xcom;

class update: public QObject
{   Q_OBJECT
public:
    explicit update();
    PP_VisionSystem_SV* ppVs;
signals:
    void emitProgression(int);
public slots:
    //void timer();
    void threads(QString value);

private:
    QString chemin;
    void progressionChangement();
    void maj();
};

#endif // UPDATE_H

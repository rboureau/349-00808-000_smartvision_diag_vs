#ifndef PACKETSENDER_H
#define PACKETSENDER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>
#include <QDebug>
class PacketSender : public QObject
{
    Q_OBJECT
public:
    explicit PacketSender(QObject *parent = nullptr);

signals:
    void emitString(QString, bool);
    //void emitString(QString);
public slots:
    void newConnection();
    void readPort();

private:
        QTcpServer *server;
        QTcpSocket *socket;
        //QTcpSocket* socks[2];
        QVector<QTcpSocket*> socks;
        int nombreConnexion;
};
#endif // PACKETSENDER_H
